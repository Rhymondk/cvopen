<?php

class SiteController extends PublicController
{
    public function actions()
    {
        return array(
            'oauth' => array(
                'class'=>'ext.hoauth.HOAuthAction',
                'model' => 'User',
                'scenario' => 'social-insert',
                'attributes' => array(
                    'email' => 'email',
                    'name' => 'displayName',
                    'role_id' => 1
                ),
            )
        );
    }

    /**
     * Access Controller filter
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Only employers
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'expression' => array('PublicController', 'isGuest'),
                'actions' => array('register')
            ),
            array('deny',
                'users' => array('*'),
                'actions' => array('register'),
                'deniedCallback' => function () {
                    $this->redirect(Yii::app()->createUrl('site/index'));
                }
            )
        );
    }

    public function actionIndex()
    {
        $user = new User();
        $criteria = Selection::getSelectionValidCriteria();
        $criteria->limit = 4;
        $selections = Selection::model()->findAll($criteria);

        $this->render('index', array(
            'user' => $user,
            'selections' => $selections
        ));
    }

    public function actionMaintenance()
    {
        $this->layout = '//layouts/maintenance';
        $this->render('maintenance');
    }

    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            $logger = new Logger();
            $logger->format($error);
            $logger->save(false);
            $this->render('404');
        }
    }

    public function actionRegister($id)
    {
        $selection = Selection::model()->findValidByPk($id);

        $user = new User();

        $this->render('register', array(
            'user' => $user,
            'selection' => $id
        ));
    }

    public function actionDownload($file)
    {
        $file = base64_decode(urldecode($file));

        if (!Worker::model()->cvExist($file)) {
            throw new CHttpException(404, "Nėra CV");
        }

        if ((self::isEmployer() || self::isAdmin()) &&
            file_exists(Yii::getPathOfAlias('webroot') . '/' . $file)
        ) {
            Yii::app()->getRequest()->sendFile(basename($file), file_get_contents($file));
            $this->redirect(Yii::app()->createUrl('site/index'));
        }

        throw new CHttpException(404, "Klaida siunčiantis failą");
    }
}
