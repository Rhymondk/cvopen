<?php

class AjaxController extends PublicController
{
    public function beforeAction($action)
    {
        parent::beforeAction($action);

        if (!Yii::app()->request->isAjaxRequest) {
            throw new CHttpException(404, Yii::t('web', 'Puslapis nerastas'));
        }

        return true;
    }

    public function actionLogin()
    {
        $user = new LoginForm;

        if (isset($_POST['LoginForm'])) {
            $user->attributes = $_POST['LoginForm'];

            if (!$user->login()) {
                echo CActiveForm::validate($user);
            }
        }
    }

    public function actionChangeWorkerStatus()
    {
        $selectionCandidate = SelectionCandidate::model()->findByPk($_POST['candidateId']);

        if (isset($_POST['action'])) {
            $selectionCandidate->{$_POST['action']} = 1 - $selectionCandidate->{$_POST['action']};
            var_dump($selectionCandidate->save(false));
        }


    }

    public function actionGetPriceTable()
    {
        if (
            !empty($_POST['Selection']['date_start']) &&
            !empty($_POST['Selection']['date_end']) &&
            strtotime($_POST['Selection']['date_start']) < strtotime($_POST['Selection']['date_end'])
        ) {

            list($priceTable, $total) = SelectionHosting::model()->getPriceTableInfo(
                $_POST['Selection']['date_start'],
                $_POST['Selection']['date_end'],
                isset($_POST['HostPlan']) ? $_POST['HostPlan'] : '',
                0
            );

            $success = true;

        } else {
            $priceTable = array();
            $total = array();
            $success = false;
        }

        echo CJSON::encode(array(
            'success' => $success,
            'html' => $this->renderPartial('//employer/_priceTable', array(
                'priceTable' => $priceTable,
                'total' => $total,
            ), true)));

    }

}