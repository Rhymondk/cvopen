<?php

class UserController extends PublicController
{

    public function actionLogin()
    {
        $user = new LoginForm;

        if (isset($_POST['LoginForm'])) {
            $user->attributes = $_POST['LoginForm'];

            if (!$user->login() && !$user->validate()) {
                echo CActiveForm::validate($user);
                Yii::app()->end();
            }

            echo CJSON::encode(array('redirect' => Yii::app()->createUrl(strtolower(Yii::app()->user->role) . '/index')));
            Yii::app()->end();
        }
    }

    private function afterRegistration($user)
    {
        Mailer::app()->sendTemplate($user->email, 'register' . ucfirst($user->role->model));

        if (strtolower($user->role->model) == 'employer') {
            Mailer::app()->sendText('sigita@cvopen.lt', $user->role->name . ' Registracija. Email: ' . $user->email, 'Užsiregistravo ' . $user->role->name);
            Mailer::app()->sendText('rentcas@gmail.com', $user->role->name . ' Registracija. Email: ' . $user->email, 'Užsiregistravo ' . $user->role->name);
        }
    }

    public function actionChangePassword($token)
    {
        $recover = UserRecover::model()->findByAttributes(array(
            'token' => $token
        ));

        if (is_null($recover) || strtotime($recover->date_end) < strtotime(date('Y-m-d H:i:s'))) {
            throw new CHttpException(404, 'Nerastas tokenas arba pasibaiges galiojimas');
        }

        $user = $recover->user;
        $user->scenario = 'recover-password';

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if ($user->validate()) {
                $user->password = CPasswordHelper::hashPassword($user->password, 10);
                $user->save(false);
                $recover->delete();
                $this->render('recoverChangePasswordSuccess');
                die();
            } else {
                $user->getErrors();
            }
        }

        $this->render('recoverChangePassword', array(
            'user' => $user
        ));
    }

    public function actionRecoverPassword()
    {
        $user = new User();
        $user->scenario = 'recover';

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if ($user->validate()) {

                $userRecover = User::model()->findByAttributes(array(
                    'email' => $user->email
                ));

                if (!is_null($userRecover)) {
                    $recover = new UserRecover();
                    $recover->user_id = $userRecover->id;
                    $recover->token = $this->uid();
                    $recover->date_end = date('Y-m-d H:i:s', strtotime('+1 day', time()));

                    if ($recover->validate()) {
                        $recover->save(false);
                        Mailer::app()->sendTemplate($_POST['User']['email'], 'changePassword', array(
                            'token' => $recover->token
                        ));
                    }

                }

                $this->render('recoverPasswordSend');
                die();
            } else {
                $user->getErrors();
            }
        }

        $this->render('recoverPassword', array(
            'user' => $user
        ));
    }

    public function actionRegister($selection = null)
    {
        $user = new User('front-insert');

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];
            $user->date_created = $this->today();
            $user->ip = Yii::app()->request->getUserHostAddress();

            if ($user->{'rules' . $user->role_id}) {
                $user->{'rules' . ($user->role_id == 1 ? 2 : 1)} = true;
            }

            if ($user->validate()) {
                $loginPass = $user->password;
                $user->password = CPasswordHelper::hashPassword($user->password, 10);
                $user->save(false);

                $role = Role::model()->findByPk($user->role_id);
                $userRole = new $role->model;
                $userRole->user_id = $user->id;
                $userRole->save(false);

                $login = new LoginForm();
                $login->username = $user->email;
                $login->password = $loginPass;
                $login->login();

                $this->afterRegistration($user);
                if (is_null($selection)) {
                    echo CJSON::encode(array('redirect' => Yii::app()->createUrl($user->role->model . '/index')));
                } else {
                    echo CJSON::encode(array('redirect' => Yii::app()->createUrl($user->role->model . '/selection', array('id' => $selection))));
                }
            } else {
                echo CActiveForm::validate($user);
            }

            Yii::app()->end();
        }
    }

    public function actionLogout()
    {
        if (!Yii::app()->user->isGuest) {
            Yii::app()->user->logout();
            $this->redirect(Yii::app()->createUrl('site/index'));
        }
    }

}
