<?php
class SelectionController extends PublicController
{
    /**
     * Access Controller filter
     * @return array
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Only employers
     * @return array
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'expression' => array('PublicController', 'isEmployer'),
            ),
            array('deny',
                'users' => array('*'),
                'deniedCallback' => function () {
                    $this->redirect(Yii::app()->createUrl('site/index'));
                }
            )
        );
    }

    /**
     * Selection review, candidates list
     * @param $id - Selection id
     * @throws CHttpException selection not found, not selection owner
     */
    public function actionView($id, $type_id = 0, $starred = 0, $hidden = 0, $limit = null)
    {
        $selection = Selection::model()->findByPk($id);

        if (is_null($selection)) {
            throw new CHttpException(404, 'Atranka nerasta');
        }

        if ($selection->user_id != Yii::app()->user->getId()) {
            throw new CHttpException(404, 'Jūs neturite teisių');
        }

        $types = Type::model()->findAll();
        $typesSelectList = CHtml::listData(Type::model()->findAll(), 'id', 'name');
        array_unshift($typesSelectList, Yii::t('web', 'Bendras įvertinimas'));

        if (!in_array($type_id, array_keys($typesSelectList))) {
            $type_id = 0;
        }

        $inPageOptions = explode(',', Yii::app()->params['selections_in_page_option']);

        list($pages, $candidates) = $selection->getValidCandidates($type_id, $starred, $hidden);

        switch(true) {
            case $starred == 0 && $hidden == 0:
                $filter = 0;
                break;
            case $starred == 1:
                $filter = 1;
                break;
            case $hidden == 1:
                $filter = 2;
                break;
            default:
                $filter = 0;
        }

        $selectionShareForm = new SelectionShareForm();

        $this->render('view', array(
            'selection' => $selection,
            'pages' => $pages,
            'inPageOptions' => $inPageOptions,
            'types' => $types,
            'candidates' => $candidates,
            'typesSelectList' => $typesSelectList,
            'limit' => array_search($limit, $inPageOptions),
            'type_id' => $type_id,
            'filter' => $filter,
            'selectionShareForm' => $selectionShareForm
        ));
    }

    /**
     * Share selection to emails in selectionShareForm model
     * @param int $id
     */
    public function actionShare($id)
    {
        $model = new SelectionShareForm();
        $selection = Selection::model()->findByPk($id);
        $selectionShareEmails = SelectionShare::model()->findAllByAttributes(array(
            'selection_id' => $id
        ));

        if (is_null($selection)) {
            throw new CHttpException(404, 'Atranka nerasta');
        }

        if ($selection->user_id != Yii::app()->user->getId()) {
            throw new CHttpException(404, 'Jūs neturite teisių');
        }

        if (!$selection->isValidStatus()) {
            throw new CHttpException(404, 'Nepatvirtinta atranka');
        }

        if (isset($_POST['SelectionShareForm'])) {

            $model->attributes = $_POST['SelectionShareForm'];

            if ($model->validate()) {
                $emails = preg_split('/\r\n|[\r\n]/', $model->emails);

                foreach ($emails as $email) {
                    $selectionShare = new SelectionShare();
                    $selectionShare->email = $email;
                    $selectionShare->selection_id = $id;

                    if ($selectionShare->validate()) {
                        $selectionShare->save(false);
                    }
                }
            }

            if (!$model->hasErrors()) {
                echo CJSON::encode(array('success' => true));
            } else {
                echo CActiveForm::validate($model);
            }

            Yii::app()->end();
        }

        $this->render('share', array(
            'model' => $model,
            'selection' => $selection,
            'selectionShareEmails' => $selectionShareEmails
        ));
    }

    public function actionCandidate($id)
    {
        $selectionCandidate = SelectionCandidate::model()->findByPk($id);
        $worker = $selectionCandidate->workerUser;
        $workerCompetences = CHtml::listData($selectionCandidate->selectionCandidateCompetence, 'competence_id', 'competence_id');

        $competences = array();

        foreach($selectionCandidate->selection->competences as $competence) {
            $competences[$competence->id]['name'] = $competence->name;
            $competences[$competence->id]['active'] = in_array($competence->id, $workerCompetences);
        }

        $types = Type::model()->findAll();

        $typeInfo = array();

        foreach ($types as $type) {
            $typeInfo[$type->id] = $worker->getTypeAttribute($type->id);
            $typeInfo[$type->id]['type'] = $type->name;
        }

        //Add IQ type
        $typeDisplay = CHtml::listData($types, 'id', 'id');

        $this->render('candidate', array(
            'worker' => $worker,
            'competences' => $competences,
            'candidate' => $selectionCandidate,
            'typeInfo' => $typeInfo,
            'typeDisplay' => $typeDisplay,
            'selection' => $selectionCandidate->selection
        ));
    }
}