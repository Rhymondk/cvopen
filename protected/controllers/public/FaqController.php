<?php

class FaqController extends PublicController
{
    public function actionIndex()
    {
        $model = Faq::model()->findAll();

        if (is_null($model)) {
            throw new CHttpException(404, Yii::t('web', 'Puslapis nerastas'));
        }

        $this->render('view', array(
            'model' => $model
        ));
    }
}
?>