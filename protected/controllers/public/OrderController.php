<?php

class OrderController extends PublicController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {

        return array(
            array('allow',
                'users' => array('*'),
                'expression' => '!Yii::app()->user->isGuest',
            ),
            array('deny',
                'users' => array('*'),
                'deniedCallback' => function () {
                    $this->redirect(Yii::app()->createUrl('site/index'));
                }
            )
        );
    }

    public function actionAccept()
    {
        if (isset($_GET['data'])) {
            $this->render('accept');
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionCancel()
    {
        $this->render('cancel');
    }

    public function actionPaid()
    {
        $this->render('paid');
    }

    public function loadModel($uid)
    {
        $order = Order::model()->findByAttributes(array(
            'uid' => $uid
        ));

        if (is_null($order)) {
            throw new CHttpException(404, Yii::t('web', 'Tokio apmokėjimo nėra'));
        }

        return $order;
    }
}
?>
