<?php

class QuizController extends PublicController
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array(
                'allow',
                'users' => array('*'),
                'actions' => array('afterPay', 'print', 'afterPaySMS')
            ),
            array(
                'allow',
                'users' => array('*'),
                'expression' => array('PublicController', 'isWorker'),
                'actions' => array('start', 'solve')
            ),
            array(
                'allow',
                'users' => array('*'),
                'expression' => array('QuizController', 'isComplete'),
                'actions' => array('finish', 'pay')
            ),
            array(
                'deny',
                'users' => array('*'),
                'deniedCallback' => function () {
                    $this->redirect(Yii::app()->createUrl('site/index'));
                }
            )
        );
    }

    public static function isComplete()
    {
        
        if (!parent::isWorker()) {
            return false;
        }

        $account = Worker::model()->findByPk(Yii::app()->user->getId());
        return $account->complete;
    }

    private function sendQuizResults($user_id)
    {
        $user = User::model()->findByPk($user_id);
        Mailer::app()->sendTemplate($user->email, 'printQuiz', array(), WorkerPDF::app()->getOutput($user->id), Yii::t('web', 'cvopen_testo_rezultatai.pdf'));
    }

    private function completeOrder($id)
    {
        $order = Order::model()->findByPk($id);

        if (!is_null($order)) {
            $order->paid = 1;
            $order->date_paid = $this->today();
            $order->save(false);

            $user = User::model()->findByPk($order->quizOrder->worker_user_id);
            //Send mail
            $this->sendQuizResults($user->id);
        } else {
            throw new CHttpException(404);
        }
    }

    public function actionAfterPaySMS()
    {
        try {
            $response = WebToPay::validateAndParseData(
                $_GET,
                Yii::app()->params['paysera_project_id'],
                Yii::app()->params['paysera_password']
            );

            $sms = explode(' ', $response['sms']);
            $worker = Worker::model()->findByPk($sms[1]);
            $this->completeOrder($worker->quizOrder->order_id);

            echo "NOSMS";

        } catch (Exception $e) {
            throw new CHttpException(404, $e->getMessage());
        }
    }

    public function actionAfterPay($id)
    {
        try {
            WebToPay::validateAndParseData(
                $_GET,
                Yii::app()->params['paysera_project_id'],
                Yii::app()->params['paysera_password']
            );

            $this->completeOrder($id);

            echo 'OK';

        } catch (Exception $e) {
            throw new CHttpException(404, $e->getMessage());
        }
    }

    public function actionPay($via = null)
    {
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());
        $quizOrder = QuizOrder::model()->findByAttributes(array(
            'worker_user_id' => Yii::app()->user->getId()
        ));

        if (!is_null($quizOrder)) {
            $order = Order::model()->findByPk($quizOrder->order_id);

            if (!is_null($via)) {
                $order->via = $via;
                $order->save(false);
            }

        } else {
            throw new CHttpException(404, 'Tokio užsakymo nėra');
        }

        $order = $quizOrder->order;

        if ($order->paid) {
            $this->redirect(Yii::app()->createUrl('order/paid'));
        }

        $this->redirectToPay($order);
    }

    public function redirectToPay($order)
    {
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());

        Order::pay($order->uid, array(
            'callbackurl' => Yii::app()->createAbsoluteUrl('quiz/afterPay', array('id' => $order->id)),
            'p_email' => $worker->user->email,
            'p_firstname' => $worker->firstname,
            'p_lastname' => $worker->lastname
        ));
    }

    public function actionFinish()
    {
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());
        $quizOrder = QuizOrder::model()->findByAttributes(array(
            'worker_user_id' => Yii::app()->user->getId()
        ));

        if (is_null($quizOrder)) {
            $order = new Order();
            $order->uid = $this->uid();
            $order->amount = Yii::app()->params['quiz_price'];
            $order->date_created = $this->today();
            $order->type = 'quiz';
            $order->save(false);

            $quizOrder = new QuizOrder();
            $quizOrder->order_id = $order->id;
            $quizOrder->worker_user_id = Yii::app()->user->getId();
            $quizOrder->save(false);
        } else {
            $order = $quizOrder->order;
        }

        if ($order->paid) {
            $this->redirect(Yii::app()->createUrl('order/paid'));
        }

        $timeSpent = 0;
        foreach ($worker->quizTakens as $quizTaken) {
            $timeSpent += strtotime('1970-01-01 ' . $quizTaken->getDiff() . 'GMT');
        }

        $timeSpentString = '';

        $mins = (string)intval(gmdate('i', $timeSpent));
        $timeSpentString .= $mins . ' ';
        $timeSpentString .= $this->numberString($mins, Yii::t('web', 'minu'), array(
            'čių',
            'tę',
            'tes'
        ));
        $timeSpentString .= ' ';

        $sec = (string)intval(gmdate('s', $timeSpent));
        $timeSpentString .= $sec . ' ';
        $timeSpentString .= $this->numberString($sec, Yii::t('web', 'sekund'), array(
            'žių',
            'ė',
            'es'
        ));

        $paymentMethods = WebToPay::getPaymentMethodList(Yii::app()->params['paysera_project_id'])
            ->setDefaultLanguage('lt')
            ->getCountry('lt')
            ->getPaymentMethods();

        $notifyEmail = '';
        $notifyEmail .= $worker->firstname . ' ' . $worker->lastname . '.';
        $notifyEmail .= 'Bendras įvertinimas' . $worker->getScore(0) . '.';

        if (isset(Yii::app()->session['candidateSelectionId'])) {
            $selection = Selection::model()->findByPk(Yii::app()->session['candidateSelectionId']);
            $selection->notify($worker);
            unset(Yii::app()->session['candidateSelectionId']);

            $notifyEmail .= 'Kandidatavo į ' . $selection->position . '.' . $selection->employer->name . '.';
        }

//        Mailer::app()->sendText('sigita@cvopen.lt', 'Testą išsprendė: ' . $worker->firstname . ' ' . $worker->lastname, $notifyEmail);
//        Mailer::app()->sendText('rentcas@gmail.com', 'Testą išsprendė: ' . $worker->firstname . ' ' . $worker->lastname, $notifyEmail);

        $this->render('finish', array(
            'timeSpent' => $timeSpentString,
            'paymentMethods' => $paymentMethods
        ));
    }

    public function checkPaid()
    {
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());
        $quizOrder = QuizOrder::model()->findByAttributes(array(
            'worker_user_id' => Yii::app()->user->getId()
        ));

        if (is_null($quizOrder)) {
            throw new CHttpException(404, Yii::t('web', 'Užsakymas nerastas'));
        }

        //If not paid redirect to pay
        if (!$quizOrder->order->paid) {
            $this->redirectToPay($quizOrder->order);
            die();
        }

        return true;
    }

    public function checkQuizTaken($id = null)
    {
        $quizNotComplete = QuizTaken::model()->getNotComplete();

        if (!is_null($quizNotComplete)) {

            return $quizNotComplete->quiz;
        }

        $quizRunning = QuizTaken::model()->getNotCompleteRunning();

        if (!is_null($quizRunning)) {
            $this->redirect(Yii::app()->createUrl('quiz/solve'));
        }

        $criteria = new CDbCriteria();
        $criteria->order = 't.order DESC';
        $criteria->addCondition('t.required = 1', 'OR');

        if (!is_null($id)) {
            Yii::app()->session['candidateSelectionId'] = $id;
        }

        if (isset(Yii::app()->session['candidateSelectionId'])) {

            $criteria->join = 'LEFT JOIN ' . SelectionQuiz::model()->tableName() . ' sq ON sq.quiz_id = t.id';

            $criteria->addCondition('sq.selection_id = :selection_id', 'OR');
            $criteria->params[':selection_id'] = Yii::app()->session['candidateSelectionId'];
        }

        $quizzes = Quiz::model()->findAll($criteria);

        if (is_null($quizzes)) {
            throw new CHttpException(404, Yii::t('web', 'Nerasta testų'));
        }

        $solveQuiz = null;

        foreach ($quizzes as $quiz) {
            $quizTaken = QuizTaken::model()->findByAttributes(array(
                'quiz_id' => $quiz->id,
                'user_id' => Yii::app()->user->getId(),
            ));

            if (is_null($quizTaken)) {
                $quizTaken = new QuizTaken();
                $quizTaken->quiz_id = $quiz->id;
                $quizTaken->user_id = Yii::app()->user->getId();
                $quizTaken->complete = 0;
                $quizTaken->save(false);
                $solveQuiz = $quiz;
                break;
            }
        }

        if (is_null($solveQuiz)) {
            WorkerScore::model()->generateScore(Yii::app()->user->getId());
            $this->redirect(Yii::app()->createUrl('quiz/finish'));
        }

        return $solveQuiz;
    }

    public function actionStart($id = null)
    {
        $quiz = $this->checkQuizTaken($id);

        $this->render('start', array(
            'quiz' => $quiz
        ));
    }

    public function actionSolve($id = null)
    {
        //Get not completed test
        $quizTaken = QuizTaken::model()->getNotCompleteRunning();

        if (is_null($quizTaken)) {

            $this->redirect(Yii::app()->createUrl('quiz/finish'));
        }

        //Get Quiz model
        $quiz = Quiz::model()->findByPk($quizTaken->quiz_id);

        if (is_null($quiz)) {
            throw new CHttpException(404, Yii::t('web', 'Tokio testo nėra'));
        }

        //Check if timer started
        if (empty($quizTaken->date_start)) {
            $quizTaken->date_start = $this->today();
            $quizTaken->save(false);
        }

        //Check if answered
        if (isset($_POST['QuizTakenAnswer'])) {
            $quizTakenAnswer = new QuizTakenAnswer();
            $check = QuizTakenAnswer::model()->findByAttributes(array(
                'quiz_taken_id' => $quizTaken->id,
                'question_id' => $_POST['Question']['id']
            ));

            if (is_null($check)) {
                $quizTakenAnswer->question_id = $_POST['Question']['id'];
                $quizTakenAnswer->answer_id = $_POST['QuizTakenAnswer']['answer_id'];
                $quizTakenAnswer->quiz_taken_id = $quizTaken->id;
                $quizTakenAnswer->save(false);
            }
        }

        //Get all answered questions
        $quizAnsweredQuestions = CHtml::listData($quizTaken->quizTakenAnswers, 'question_id', 'question_id');
        $quizQuestions = CHtml::listData($quiz->quizQuestions, 'question_id', 'question_id');

        //Check for unnanswered questions
        $question = null;

        //Check id
        if (
            !is_null($id) &&
            in_array($id, $quizQuestions) &&
            !in_array($id, $quizAnsweredQuestions) &&
            $quiz->pagination
        ) {
            $question = Question::model()->findByPk($id);
        } else {
            //Next question
            if (!is_null($id) && in_array($id, $quizQuestions)) {
                $quizQuestions = array_values($quizQuestions);
                $start = array_slice($quizQuestions, array_search($id, $quizQuestions));
                $end = array_slice($quizQuestions, 0, array_search($id, $quizQuestions));
                $quizQuestions = $start + $end;
            }

            foreach ($quizQuestions as $quizQuestion) {
                if (!in_array($quizQuestion, $quizAnsweredQuestions)) {
                    $question = Question::model()->findByPk($quizQuestion);
                    break;
                }
            }
        }

        //Check if left unnanswered questions if not quiz completed
        if (is_null($question)) {
            $quizTaken->complete = 1;
            $quizTaken->date_end = date("Y-m-d H:i:s");
            $quizTaken->save(false);
            $this->redirect(Yii::app()->createUrl('quiz/start'));
        }

        //Get timer
        $interval = null;

        if ($quiz->time) {
            $dateStart = new DateTime($quizTaken->date_start);
            $dateStart->modify("+{$quiz->time} minutes");
            $dateEnd = new DateTime('now');
            $interval = $dateEnd->diff($dateStart);

            if ($interval->invert) {
                $quizTaken->complete = 1;
                $quizTaken->date_end = date("Y-m-d H:i:s");
                $quizTaken->save(false);
                $this->redirect(Yii::app()->createUrl('quiz/start'));
            }

            $interval->i += $interval->h * 60;
            $interval->h = 0;
        }

        //Reinit QuizTakenAnswer model
        $quizTakenAnswer = new QuizTakenAnswer();

        $this->breadcrumb = Yii::t('web', '{answered} iš {left}', array(
            '{answered}' => count($quizAnsweredQuestions) + 1,
            '{left}' => count($quiz->quizQuestions),
        ));

        $this->render('solve', array(
            'question' => $question,
            'quiz' => $quiz,
            'time_left' => $interval,
            'quizTakenAnswer' => $quizTakenAnswer,
            'quizAnsweredQuestions' => $quizAnsweredQuestions
        ));
    }

}
