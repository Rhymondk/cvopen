<?php

class CmsController extends PublicController
{
    public function actionIndex($hook)
    {
        $model = Cms::model()->findByAttributes(array(
            'hook' => $hook
        ));

        if (is_null($model)) {
            throw new CHttpException(404, Yii::t('web', 'Puslapis nerastas'));
        }

        $this->render('view', array(
            'model' => $model
        ));
    }
}
?>