<?php

class EmployerController extends PublicController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'expression' => array('PublicController', 'isEmployer'),
            ),
            array('deny',
                'users' => array('*'),
                'deniedCallback' => function () {
                    $this->redirect(Yii::app()->createUrl('site/index'));
                }
            )
        );
    }

    public function afterCreateSelection($selection)
    {
        $message = 'Sukurta atranka. ' . $selection->position;
        Mailer::app()->sendText('sigita@cvopen.lt', $message, $message);
        Mailer::app()->sendText('rentcas@gmail.com', $message, $message);
    }


    public function actionIndex()
    {
        $this->redirect(Yii::app()->createUrl('employer/selections'));
    }

    private function checkAuthority($selection)
    {

        if (is_null($selection) || $selection->user_id != Yii::app()->user->getId()) {
            throw new CHttpException(404, Yii::t('web', 'Projektas nerastas'));
        }
    }

    public function actionSelections()
    {
        if (!Selection::model()->countByAttributes(array('user_id' => Yii::app()->user->getId()))) {
            $this->redirect(Yii::app()->createUrl('employer/selection'));
        }

        $selections = Selection::model()->findAllByAttributes(array('user_id' => Yii::app()->user->getId()));

        $this->render('selectionsList', array(
            'selections' => $selections
        ));
    }

    public function actionEdit()
    {
        $employer = Employer::model()->findByPk(Yii::app()->user->getId());
        $user = User::model()->findByPk(Yii::app()->user->getId());
        $employer->scenario = 'reviewSelection';
        $user->scenario = 'change-password';
        $sucess = false;

        if (isset($_POST['User'])) {
            $user->attributes = $_POST['User'];

            if ($user->validate()) {
                $user->password = CPasswordHelper::hashPassword($user->password, 10);
                $sucess = $user->save(false);
                $user->currentPassword = '';
            } else {
                $user->getErrors();
            }
        }

        if (isset($_POST['Employer'])) {
            $employer->attributes = $_POST['Employer'];

            if ($employer->validate()) {
                $sucess = $employer->save(false);
            } else {
                $employer->getErrors();
            }
        }

        $this->render('updateInfo', array(
            'employer' => $employer,
            'user' => $user,
            'success' => $sucess
        ));
    }

    public function actionSelection($id = null)
    {
        $employer = Employer::model()->findByPk(Yii::app()->user->getId());
        $competences = array();

        if (is_null($id)) {
            $selection = new Selection();

            for ($i = 1; $i < 3; $i++) {
                $competences[] = new Competence();
            }
        } else {
            $selection = Selection::model()->findByPk($id);
            $this->checkAuthority($selection);

            foreach ($selection->competences as $competence) {
                $competences[$competence->id] = $competence;
            }
        }

        $valid = true;

        if (isset($_POST['Selection'])) {
            $selection->attributes = $_POST['Selection'];
            $selection->user_id = Yii::app()->user->getId();

            if (!$selection->validate()) {
                $selection->getErrors();
                $valid = false;
            }

            foreach ($_POST['Competence'] as $i => $competence) {
                $competences[$i] = new Competence();
                $competences[$i]->attributes = $competence;

                if (!$competences[$i]->validate()) {
                    $competences[$i]->getErrors();
                    $valid = false;
                }
            }

            $employer->attributes = $_POST['Employer'];

            if (!$employer->validate()) {
                $employer->getErrors();
                $valid = false;
            }

            if ($valid) {
                $selection->status_id = Selection::getDefaultStatus();
                $selection->save(false);
                Competence::model()->deleteAll('selection_id = ?', array($selection->id));

                foreach ($competences as $competence) {
                    $competence->selection_id = $selection->id;
                    $competence->save(false);
                }

                $employer->save(false);

                $this->afterCreateSelection($selection);
                if (is_null($id)) {
                    $this->redirect(Yii::app()->createUrl('employer/ReviewSelection', array('id' => $selection->id)));
                } else {
                    $this->redirect(Yii::app()->createUrl('employer/selections'));
                }
            }
        }

        $cities = City::model()->findAll();

        $this->render('createSelection', array(
            'selection' => $selection,
            'cities' => $cities,
            'competences' => $competences,
            'employer' => $employer
        ));
    }

    public function actionHostSelection($id)
    {
        $selection = Selection::model()->findByPk($id);
        $this->checkAuthority($selection);
        $selection->scenario = 'hosting';
        $hostingPlans = SelectionHostingPlan::model()->findAll();

        if (isset($_POST['Selection'])) {
            $selection->attributes = $_POST['Selection'];

            if ($selection->validate()) {
                if ($selection->status_id == 1) {
                    $selection->status_id = 2;
                }

                $selection->save(false);

                SelectionHosting::model()->deleteAll('selection_id = ?', array($selection->id));

                if ((int)$selection->host_type == 0) {
                    if (isset($_POST['HostPlan'])) {
                        foreach ($_POST['HostPlan'] as $hostPlan) {
                            $selectionHosting = new SelectionHosting();
                            $selectionHosting->selection_id = $selection->id;
                            $selectionHosting->selection_hosting_plan_id = $hostPlan;
                            $selectionHosting->save(false);
                        }
                    }
                }

                $this->redirect(Yii::app()->createUrl('employer/ReviewSelection', array('id' => $selection->id)));
            }
        }

        $this->render('hostSelection', array(
            'selection' => $selection,
            'hostingPlans' => $hostingPlans
        ));
    }

    public function actionSelectionSuccess($id)
    {
        $selection = Selection::model()->findByPk($id);

        $this->render('selectionSuccess', array(
            'billPdf' => $selection->bill
        ));
    }

    public function actionReviewSelection($id)
    {
        $selection = Selection::model()->findByPk($id);

        $this->checkAuthority($selection);

        $this->render('reviewSelection', array(
            'selection' => $selection,
        ));
    }
}
