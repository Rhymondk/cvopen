<?php
class WorkerController extends PublicController
{
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Check if user is worker static class
     * @return bool
     */
    public static function isWorker()
    {
        return isset(Yii::app()->user->isWorker) && Yii::app()->user->isWorker;
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'expression' => array('PublicController', 'isWorker')
            ),
            array(
                'allow',
                'users' => array('*'),
                'actions' => array('view', 'selections')
            ),
            array('deny',
                'users' => array('*'),
                'deniedCallback' => function () {
                    $this->redirect(Yii::app()->createUrl('site/index'));
                }
            )
        );
    }

    public function actionIndex()
    {
        $this->redirect(Yii::app()->createUrl('worker/selections'));
    }

    public function actionSelections()
    {
        $criteria = Selection::getSelectionValidCriteria();

        $count = Selection::model()->count($criteria);

        $pageSize = Yii::app()->request->getParam('limit', Yii::app()->params['selections_in_page']);

        $inPageOptions = explode(',', Yii::app()->params['selections_in_page_option']);

        $pageSize = in_array($pageSize, $inPageOptions) ? $pageSize : Yii::app()->params['selections_in_page'];

        $pages = new CPagination($count);
        $pages->pageSize = in_array($pageSize, $inPageOptions) ? $pageSize : Yii::app()->params['selections_in_page'];
        $pages->applyLimit($criteria);

        $selections = Selection::model()->findAll($criteria);

        $this->render('selectionsList', array(
            'selections' => $selections,
            'pages' => $pages,
            'count' => $count,
            'inPageOptions' => $inPageOptions,
            'pageSize' => $pageSize
        ));
    }

    private function checkHistory($model)
    {
        if (is_null($model) || $model->worker_user_id != Yii::app()->user->getId()) {
            throw new CHttpException(404);
        }
    }

    public function actionRemoveHistory($id, $selection_id = null)
    {
        $model = WorkerHistory::model()->findByPk($id);
        $this->checkHistory($model);

        $model->delete();

        if (is_null($selection_id)) {
            $this->redirect(Yii::app()->createUrl('worker/edit'));
        } else {
            $this->redirect(Yii::app()->createUrl('worker/selection', array('id' => $selection_id)));
        }
    }

    public function actionAddHistory($id = null)
    {
        if (isset($_POST['WorkerHistory'])) {
            if (is_null($id)) {
                $model = new WorkerHistory;
            } else {
                $model = WorkerHistory::model()->findByPk($id);
                $this->checkHistory($model);
            }

            if ($_POST['WorkerHistory']['working_now']) {
                $model->scenario = 'working-now';
            } else {
                $model->scenario = 'insert';
            }

            $model->attributes = $_POST['WorkerHistory'];
            $model->worker_user_id = Yii::app()->user->getId();

            if ($model->validate()) {
                $model->save(false);
                echo CJSON::encode(array('success' => true));
            } else {
                echo CActiveForm::validate($model);
            }

            Yii::app()->end();
        }

        throw new CHttpException(404);
    }

    public function actionEdit($history = null)
    {
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());
        $user = User::model()->findByPk(Yii::app()->user->getId());
        $educations = Education::model()->findAll();
        $genders = Gender::model()->findAll();

        if (!is_null($history)) {
            $workerHistory = WorkerHistory::model()->findByPk($history);
            $this->checkHistory($workerHistory);
        } else {
            $workerHistory = new WorkerHistory();
        }

        if (is_null($worker)) {
            $this->redirect(Yii::app()->createUrl('site/index'));
        }

        $worker->scenario = 'selection-create';
        $user->scenario = 'change-password';
        $sucess = false;

        if (isset($_POST['User']['currentPassword'])) {
            $user->attributes = $_POST['User'];

            if ($user->validate()) {
                $user->password = CPasswordHelper::hashPassword($user->password, 10);
                $sucess = $user->save(false);
                $user->currentPassword = '';
            } else {
                $user->getErrors();
            }
        }

        if (isset($_POST['User']['imageFile'])) {
            $user->imageFile = CUploadedFile::getInstance($user, 'imageFile');
            $worker->cvFile = CUploadedFile::getInstance($worker, 'cvFile');
            $this->selectionUpload($user, 'imageFile', 'image', $worker->user_id);
            $this->selectionUpload($worker, 'cvFile', 'cv', $worker->user_id);
        }

        if (isset($_POST['Worker']['firstname'])) {
            $worker->attributes = $_POST['Worker'];

            if ($worker->validate()) {
                $sucess = $worker->save(false);
            } else {
                $worker->getErrors();
            }
        }

        if (empty($worker->user->image)) {
            $worker->user->image = '/images/Web/no-image.png';
        } else {
            $worker->user->image = Image::open($worker->user->image)->zoomCrop(112, 112)->jpeg(100);
        }

        $this->render('updateInfo', array(
            'worker' => $worker,
            'user' => $user,
            'success' => $sucess,
            'education' => $educations,
            'genders' => $genders,
            'workerHistory' => $workerHistory
        ));
    }

    public function actionSubmitSelection($id)
    {
        $selection = Selection::model()->findValidByPk($id);
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());

        $selectionCandidate = SelectionCandidate::model()->findByAttributes(array(
            'selection_id' => $selection->id,
            'worker_user_id' => Yii::app()->user->getId()
        ));

        if (is_null($selectionCandidate)) {
            $selectionCandidate = new SelectionCandidate;
            $selectionCandidate->selection_id = $selection->id;
            $selectionCandidate->worker_user_id = Yii::app()->user->getId();
            $selectionCandidate->save(false);
        }

        SelectionCandidateCompetence::model()->deleteAllByAttributes(array('selection_candidate_id' => $selectionCandidate->id));

        if (isset($_POST['competence'])) {
            foreach ($_POST['competence'] as $competence) {
                $selectionCandidateCompetence = new SelectionCandidateCompetence;
                $selectionCandidateCompetence->selection_candidate_id = $selectionCandidate->id;
                $selectionCandidateCompetence->competence_id = $competence;
                $selectionCandidateCompetence->save(false);
            }
        }

        if (!$worker->getComplete($selection->id)) {
            $this->redirect(Yii::app()->createUrl('quiz/start', array(
                'id' => $selection->id
            )));
        } else {
            $selection->notify($worker);
            $this->redirect(Yii::app()->createUrl('worker/success'));
        }
    }

    public function actionSuccess()
    {
        $this->render('selectionSuccess');
    }

    private function selectionUpload($model, $modelFile, $attribute, $id)
    {
        if ($model->validate(array($modelFile))) {
            if (!is_null($model->{$modelFile})) {
                rename(
                    $model->{$modelFile}->getTempName(),
                    'images/' . get_class($model) . '/' . $id . '.' . $model->{$modelFile}->getExtensionName()
                );

                $model->{$attribute} = 'images/' . get_class($model) . '/' . $id . '.' . $model->{$modelFile}->getExtensionName();
                $model->save(false);
            }
        } else {
            if (Yii::app()->request->isAjaxRequest) {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            } else {
                $model->getErrors();
            }
        }
    }

    public function actionView($id)
    {
        $selection = Selection::model()->findValidByPk($id);

        $this->render('viewSelection', array(
            'selection' => $selection
        ));
    }

    public function actionSelection($id, $history = null)
    {
        $selection = Selection::model()->findValidByPk($id);
        $worker = Worker::model()->findByPk(Yii::app()->user->getId());
        $user = User::model()->findByPk(Yii::app()->user->getId());

        if (is_null($worker)) {
            $this->redirect(Yii::app()->createUrl('site/index'));
        }

        $worker->scenario = 'selection-create';

        if (!is_null($history)) {
            $workerHistory = WorkerHistory::model()->findByPk($history);
            $this->checkHistory($workerHistory);
        } else {
            $workerHistory = new WorkerHistory();
        }

        $educations = Education::model()->findAll();
        $genders = Gender::model()->findAll();

        if (isset($_POST['Worker'])) {

            if (!isset($_POST['Worker']['cvFile'])) {
                Yii::app()->session->add('workerForm', $_POST['Worker']);
            }

            $worker->attributes = $_POST['Worker'];
            
            if ($worker->validate()) {
                $worker->save(false);
                unset(Yii::app()->session['workerForm']);
            } else {
                if (Yii::app()->request->isAjaxRequest) {
                    echo CActiveForm::validate($worker);
                    Yii::app()->end();
                } else {
                    $worker->getErrors();
                }
            }

            $user->imageFile = CUploadedFile::getInstance($user, 'imageFile');
            $worker->cvFile = CUploadedFile::getInstance($worker, 'cvFile');
            $this->selectionUpload($user, 'imageFile', 'image', $worker->user_id);
            $this->selectionUpload($worker, 'cvFile', 'cv', $worker->user_id);

            if (Yii::app()->request->isAjaxRequest) {
                echo CJSON::encode(array('success' => true));
                Yii::app()->end();
            }
        }

        if (Yii::app()->session->contains('workerForm')) {
            $worker->attributes = Yii::app()->session->get('workerForm');
        }

        if (empty($worker->user->image)) {
            $worker->user->image = '/images/Web/no-image.png';
        } else {
            $worker->user->image = Image::open($worker->user->image)->zoomCrop(112, 112)->jpeg(100);
        }

        $selectionCandidate = SelectionCandidate::model()->findByAttributes(array(
            'selection_id' => $selection->id,
            'worker_user_id' => Yii::app()->user->getId()
        ));

        $activeCompetences = !is_null($selectionCandidate) ?
            CHtml::listData($selectionCandidate->selectionCandidateCompetence, 'competence_id', 'competence_id'):
            array();

        $this->render('selection', array(
            'selection' => $selection,
            'educations' => $educations,
            'genders' => $genders,
            'workerHistory' => $workerHistory,
            'worker' => $worker,
            'user' => $user,
            'activeCompetences' => $activeCompetences
        ));
    }
}
