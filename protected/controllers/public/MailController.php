<?php

class MailController extends PublicController
{
    // imap server connection
    public $conn;

    // inbox storage and inbox message count
    private $inbox;
    private $msg_cnt;

    // email login credentials
    private $server = 'mail.cvopen.lt';
    private $user = 'cv@cvopen.lt';
    private $pass = 'vebReStEs8';
    private $flag = 'UNSEEN';

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'expression' => 'isset($_GET["token"]) && $_GET["token"] == "ifkelwnkqFij4swj4df7s"',
            ),
            array('deny',
                'users' => array('*'),
                'deniedCallback' => function () {
                    throw new CHttpException(404, 'Bad token');
                }
            )
        );
    }

    public function actionInvitation()
    {
        $selectionShares = SelectionShare::model()->getMailList();

        if ($selectionShares) {
            foreach ($selectionShares as $selectionShare) {

                $selection = $selectionShare->selection;

                Mailer::app()->sendTemplate($selectionShare->email, 'selectionShare',
                    array('selection' => $selection), null, null,
                    $selection->employer->email, 'Dėl ' . $selection->employer->name . ' atrankos',
                    $selection->employer->name
                );

                $selectionShare->sent = date('Y-m-d H:i:s');
                $selectionShare->save(false);
            }
        }
    }


    public function send()
    {
        $validator = new CEmailValidator;
        $emails = preg_split('/\r\n|[\r\n]/', $this->emails);

        foreach ($this->emails as $email) {

            if ($validator->validateValue(trim($email))) {
                Mailer::app()->sendTemplate($email, 'selectionShare',
                    array('selection' => $this->selection), null, null,
                    $this->selection->employer->email, 'Dėl ' . $this->selection->employer->name . ' atrankos',
                    $this->selection->employer->name
                );
            }
        }
    }

    public function actionReminder()
    {
        $workers = Worker::model()->findAll();

        foreach ($workers as $worker) {
            if (!$worker->complete && strtotime($worker->user->date_created) < strtotime("-1 day")) {
                $mail = new Mail();
                $mail->email = $worker->user->email;
                $mail->force = 0;
                $mail->template = 'reminder';
                if ($mail->validate()) {
                    $mail->save(false);
                }
            }
        }
    }

    // connect to the server and get the inbox emails
    public function actionAutoReply()
    {
        echo php_sapi_name();
        $this->connect();
        if ($this->checkMail()) {
            $this->sendResponse();
        } else {
            echo 'Tuščia';
        }
    }

    private function sendResponse()
    {
        foreach (imap_search($this->conn, $this->flag) as $i) {
            $header = imap_headerinfo($this->conn, $i);
            $from = $header->from[0]->mailbox . "@" . $header->from[0]->host;

            $mail = new Mail();
            $mail->email = $from;
            $mail->template = 'autoreply';
            $mail->force = 0;

            if ($mail->validate()) {
                $mail->save(false);
            }

            $this->removeMail($i);
            echo 'Sent to: ' . $from . '<br />';
        }

        imap_expunge($this->conn);
    }

    private function removeMail($msgNo)
    {
        $status = imap_setflag_full($this->conn, $msgNo, "\\Seen");
    }

    private function checkMail()
    {

        var_dump(imap_search($this->conn, $this->flag));

        if (imap_search($this->conn, $this->flag)) {
            return true;
        }

        return false;
    }

    // close the server connection
    private function close()
    {
        $this->inbox = array();
        $this->msg_cnt = 0;

        imap_close($this->conn);
    }

    // open the server connection
    // the imap_open function parameters will need to be changed for the particular server
    // these are laid out to connect to a Dreamhost IMAP server
    private function connect()
    {
        $this->conn = imap_open('{' . $this->server . '/notls}', $this->user, $this->pass);
    }

    // move the message to a new folder
    private function move($msg_index, $folder = 'INBOX.Spam')
    {
        // move on server
        imap_mail_move($this->conn, $msg_index, $folder);
        imap_expunge($this->conn);

        // re-read the inbox
        $this->inbox();
    }

    // get a specific message (1 = first email, 2 = second email, etc.)
    private function get($msg_index = null)
    {
        if (count($this->inbox) <= 0) {
            return array();
        } elseif (!is_null($msg_index) && isset($this->inbox[$msg_index])) {
            return $this->inbox[$msg_index];
        }

        return $this->inbox[0];
    }

    // read the inbox
    private function inbox()
    {
        $this->msg_cnt = imap_num_msg($this->conn);

        $in = array();
        for ($i = 1; $i <= $this->msg_cnt; $i++) {
            $in[] = array(
                'index' => $i,
                'header' => imap_headerinfo($this->conn, $i),
                'body' => imap_body($this->conn, $i),
                'structure' => imap_fetchstructure($this->conn, $i)
            );
        }

        $this->inbox = $in;
    }
}
