<?php
class LoggerController extends AdminController
{
    public function actionView()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);
        $model->status = (string) $model->status;

        $this->render('view', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        $log = $this->loadModel($id);
        $log->status = !$log->status;
        $log->save(false);
        $this->redirect($this->createUrl('logger/admin'));
    }

    public function actionAdmin()
    {
        $model = new Logger('search');
        $model->unsetAttributes();

        if (isset($_GET['Logger'])) {
            $model->attributes = $_GET['Logger'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Logger::model()->findByPk($id);
        $model = is_null($model) ? new Logger : $model;

        return $model;
    }
}
