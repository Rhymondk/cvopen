<?php
class MailController extends AdminController
{
    public function actionCreate()
    {
        $model = new Mail();

        if (isset($_POST['Mail'])) {
            $model->attributes = $_POST['Mail'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('mail/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionAdmin()
    {
        $model = new Mail('search');
        $model->unsetAttributes();

        if (isset($_GET['Mail'])) {
            $model->attributes = $_GET['Mail'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }
}
