<?php
class SettingController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['Setting'])) {
            $model->attributes = $_POST['Setting'];

            if ($model->validate()) {
                $model->save(false);
                Setting::model()->generateParamsFile();

                $this->redirect(array('setting/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionRenew()
    {
        Setting::model()->deleteAll();

        foreach (Yii::app()->params as $key => $value) {
            $model = new Setting();
            $model->key = $key;
            $model->value = $value;
            $model->save(false);
        }
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }

        Setting::model()->generateParamsFile();
    }

    public function actionAdmin()
    {

        $model = new Setting('search');
        $model->unsetAttributes();

        if (isset($_GET['Setting'])) {
            $model->attributes = $_GET['Setting'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Setting::model()->findByPk($id);
        $model = is_null($model) ? new Setting : $model;

        return $model;
    }
}
