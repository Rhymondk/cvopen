<?php

class SelectionHostingPlanController extends AdminController
{
    public function actionUpload()
    {
        $model = new SelectionHostingPlan();
        parent::uploadFile($model, 'imageFile', 'image');
    }

    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['SelectionHostingPlan'])) {
            $model->attributes = $_POST['SelectionHostingPlan'];

            if ($model->validate()) {

                $model->price = number_format((float)$model->price, 2, '.', '');
                $model->save(false);

                $model->image = Yii::getPathOfAlias('webroot') . $_POST['SelectionHostingPlan']['image'];

                if (!empty($model->image) && file_exists($model->image)) {

                    rename(
                        $model->image,
                        'images/' . get_class($model) . '/' . $model->id . '.png'
                    );

                    $model->image = 'images/' . get_class($model) . '/' . $model->id . '.png';
                    $model->save(false);
                }

                $this->redirect(array('selectionHostingPlan/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new SelectionHostingPlan('search');
        $model->unsetAttributes();

        if (isset($_GET['SelectionHostingPlan'])) {
            $model->attributes = $_GET['SelectionHostingPlan'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = SelectionHostingPlan::model()->findByPk($id);
        $model = is_null($model) ? new SelectionHostingPlan : $model;

        return $model;
    }
}
