<?php
class FaqController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['Faq'])) {
            $model->attributes = $_POST['Faq'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('faq/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Faq('search');
        $model->unsetAttributes();

        if (isset($_GET['Faq'])) {
            $model->attributes = $_GET['Faq'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Faq::model()->findByPk($id);
        $model = is_null($model) ? new Faq : $model;

        return $model;
    }
}
