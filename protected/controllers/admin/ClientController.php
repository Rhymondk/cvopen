<?php
class ClientController extends AdminController
{
    public function actionUpload()
    {
        $model = new Client();
        parent::uploadFile($model, 'clientLogo', 'image');
    }

    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['Client'])) {
            $model->attributes = $_POST['Client'];

            if ($model->validate()) {

                $model->save(false);

                $model->image = Yii::getPathOfAlias('webroot') . $_POST['Client']['image'];

                if (!empty($model->image) && file_exists($model->image)) {

                    rename(
                        $model->image,
                        'images/' . get_class($model) . '/' . $model->id . '.png'
                    );

                    $model->image = 'images/' . get_class($model) . '/' . $model->id . '.png';
                    $model->save(false);
                }

                $this->redirect(array('client/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Client('search');
        $model->unsetAttributes();

        if (isset($_GET['Client'])) {
            $model->attributes = $_GET['Client'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Client::model()->findByPk($id);
        $model = is_null($model) ? new Client : $model;

        return $model;
    }
}
