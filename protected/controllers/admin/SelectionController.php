<?php
class SelectionController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionCandidates($id)
    {
        $selectionCandidate = new SelectionCandidate();
        $selectionCandidate->selection_id = $id;

        $this->render('candidates', array(
            'selectionCandidate' => $selectionCandidate
        ));
    }

    public function actionCandidate($id)
    {
        $selectionCandidate = SelectionCandidate::model()->findByPk($id);

        $types = Type::model()->findAll();
        $types = CHtml::listData($types, 'id', 'name');

        $this->render('candidate', array(
            'selectionCandidate' => $selectionCandidate,
            'types' => $types
        ));
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);
        $selectionCandidate = new SelectionCandidate();
        $selectionCandidate->selection_id = $model->id;

        if (isset($_POST['Selection'])) {
            $model->attributes = $_POST['Selection'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('selection/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
            'selectionCandidate' => $selectionCandidate
        ));
    }

    public function actionAdditional($id)
    {
        $selection = $this->loadModel($id);


        $criteria = new CDbCriteria;
        $criteria->addCondition('required = 0');

        $quizes = Quiz::model()->findAll($criteria);
//        $types = Type::model()->findAll();

        $selectionQuiz = new SelectionQuiz();
//        $selectionType = new SelectionType();

        if (isset($_POST['SelectionQuiz'])) {
//        if (isset($_POST['SelectionType']) && isset($_POST['SelectionQuiz'])) {

            $selectionQuiz->deleteAllByAttributes(array(
                'selection_id' => $id,
            ));

//            $selectionType->deleteAllByAttributes(array(
//                'selection_id' => $id,
//            ));

            if (!empty($_POST['SelectionQuiz']['quiz_id'])) {
                foreach ($_POST['SelectionQuiz']['quiz_id'] as $quiz_id) {
                    $selectionQuiz = new SelectionQuiz();
                    $selectionQuiz->quiz_id = $quiz_id;
                    $selectionQuiz->selection_id = $id;
                    $selectionQuiz->save(false);
                }
            }

//            if (!empty($_POST['SelectionType']['type_id'])) {
//                foreach ($_POST['SelectionType']['type_id'] as $type_id) {
//                    $selectionQuiz = new SelectionType();
//                    $selectionQuiz->type_id = $type_id;
//                    $selectionQuiz->selection_id = $id;
//                    $selectionQuiz->save(false);
//                }
//            }

            $this->redirect(array('selection/admin'));
        }

        $selectionQuizes = array();

        if (!empty($selection->selectionQuizes)) {
            foreach ($selection->selectionQuizes as $quiz) {
                $selectionQuizes[] = $quiz['quiz_id'];
            }
        }

//        $selectionTypes = array();
//
//        if (!empty($selection->selectionTypes)) {
//            foreach ($selection->selectionTypes as $type) {
//                $selectionTypes[] = $type['type_id'];
//            }
//        }

        $selectionQuiz->quiz_id = $selectionQuizes;
//        $selectionType->type_id = $selectionTypes;

        $this->render('additional', array(
            'selectionQuiz' => $selectionQuiz,
//            'selectionType' => $selectionType,
            'quizes' => $quizes,
//            'types' => $types
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Selection('search');
        $model->unsetAttributes();

        if (isset($_GET['Selection'])) {
            $model->attributes = $_GET['Selection'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Selection::model()->findByPk($id);
        $model = is_null($model) ? new Selection : $model;

        return $model;
    }
}
