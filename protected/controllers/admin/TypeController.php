<?php

class TypeController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['Type'])) {
            $model->attributes = $_POST['Type'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('type/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Type('search');
        $model->unsetAttributes();

        if (isset($_GET['Type'])) {
            $model->attributes = $_GET['Type'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Type::model()->findByPk($id);
        $model = is_null($model) ? new Type : $model;

        return $model;
    }
}
