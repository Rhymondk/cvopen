<?php
class SiteController extends AdminController
{
    public function actionError()
    {
        if ($error = Yii::app()->errorHandler->error) {
            $logger = new Logger();
            $logger->format($error);
            $logger->save(false);
            $this->render('404');
        }
    }

    public function actionLogin()
    {
        $this->layout = '//layouts/login';

        $model = new LoginForm('Admin');

        if (isset($_POST['LoginForm'])) {
            $model->attributes = $_POST['LoginForm'];

            if ($model->validate() && $model->login()) {
                $this->redirect(Yii::app()->homeUrl);
            }
        }

        $this->render('login', array(
            'model' => $model,
        ));
    }

    public function actionLogout()
    {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

    public function actionIndex()
    {
        $charts = array();

        //Registered Chart
        $charts['registered']['title'] = 'Užsiregistravo/Išsprendė testus';

        $workers = User::model()->findAll('date_created >= :date_created && role_id = 1', array(
            ':date_created' => date('Y-m-d 00:00:00', strtotime('-30 day'))
        ));

        //Fill zero
        for ($i = 30; $i >= 0; $i--) {
            $day = date('Y-m-d', strtotime('-' . $i . ' day'));
            $register[$day] = 0;
            $solved[$day] = 0;
            $buyed[$day] = 0;
            $keys[$day] = $day;
        }

        if (!is_null($workers)) {
            foreach($workers as $worker) {

                $register_day = date('Y-m-d', strtotime($worker->date_created));

                $register[$register_day]++;

                if (!is_null($worker->worker) && $worker->worker->complete) {
                    $solved[$register_day]++;
                    if ($worker->worker->paid) {
                        $buyed[$register_day]++;
                    }
                }
            }
        } else {
            throw new CHttpException(404, 'Nėra duomenų');
        }

        ksort($register);
        ksort($solved);
        ksort($buyed);
        ksort($keys);

        $charts['registered']['data']['registered'] = '["Užsiregistravo",' . implode(',', $register) . ']';
        $charts['registered']['data']['solved'] = '["Išsprendė",' . implode(',', $solved) . ']';
        $charts['registered']['data']['buyed'] = '["Nusipirko",' . implode(',', $buyed) . ']';
        $charts['registered']['data']['keys'] = '["' . implode('","', $keys) . '"]';

        $this->render('index', array(
            'chart' => $charts
        ));
    }

    public function actionDownload($file)
    {
        $file = base64_decode(urldecode($file));

        if (!Worker::model()->cvExist($file)) {
            throw new CHttpException(404, "Nėra CV");
        }

        if (file_exists(Yii::getPathOfAlias('webroot') . '/' . $file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($file));
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            echo "<script>window.close();</script>";
            $this->redirect(Yii::app()->createUrl('site/index'));
        }

        throw new CHttpException(404, "Klaida siunčiantis failą");
    }
}
