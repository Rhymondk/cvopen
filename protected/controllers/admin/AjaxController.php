<?php
Class AjaxController extends CController
{
    public $response;
    public $data;
    public $hasError = false;
    public $errorMsg = 'OK';

    public function actionLoad()
    {
        $this->data = (object) $_POST;

        if (isset($this->data->template) && method_exists(__class__, $this->data->template)) {
            $this->{$this->data->template}();
        } else {
            $this->errorMsg = 'Method don\'t exist';
        }

        if ($this->errorMsg != 'OK') {
            $this->hasError = true;
        }

        echo CJSON::encode(array(
            'response' => $this->response,
            'hasError' => $this->hasError,
            'errorMsg' => $this->errorMsg,
        ));

        Yii::app()->end();
    }

    private function userRole()
    {
        //1 - Worker
        //2 - Employer

        $role = '';
        switch ($this->data->role) {
            case 1:
                $role = 'Worker';
                break;
            case 2:
                $role = 'Employer';
                break;
        }

        if (!empty($role)) {
            $model = new $role;

            $this->response = $this->renderPartial('//' . strtolower($role) . '/form', array(
                'model' => $model
            ), true);

        } else {
            $this->errorMsg = 'Role not set';
        }
    }

}