<?php

class UserController extends AdminController
{

    public function actionUpload()
    {
        $model = new User();
        parent::uploadFile($model, 'imageFile', 'image');
    }

    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['User'])) {

            $model->attributes = $_POST['User'];

            if (empty($_POST['User']['password'])) {
                unset($model->password);
            }

            if ($model->validate()) {
                $model->password = CPasswordHelper::hashPassword($model->password);
                $model->save(false);

                if (!empty($_POST['User']['image'])) {
                    $model->image = Yii::getPathOfAlias('webroot') . $_POST['User']['image'];
                }

                if (!empty($model->image) && file_exists($model->image)) {

                    rename(
                        $model->image,
                        'images/' . get_class($model) . '/' . $model->id . '.png'
                    );

                    $model->image = 'images/' . get_class($model) . '/' . $model->id . '.png';
                    $model->save(false);
                }

                $this->formRedirect(array(
                    'save' => $this->createUrl('user/admin'),
                    'create-additional' => $this->createUrl('user/additional', array('id' => $model->id))
                ));

            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionAdditional($id)
    {
        $user = $this->loadModel($id);

        $model = new $user->role->model;

        if ($model->exists('user_id = :user_id', array(":user_id" => $id))) {
            $model = $model->findByPk($id);
        }

        if (isset($_POST[$user->role->model])) {
            $model->attributes = $_POST[$user->role->model];
            $model->user_id = $id;

            if ($model->validate()) {
                $model->save(false);
                $this->redirect(array('user/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('//' . $model->tableName() . '/form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionReQuiz($id)
    {
        $worker = Worker::model()->findByPk($id);

        if (is_null($worker)) {
            throw new CHttpException(404, 'Vartotojas nerastas');
        }

        $reset = array(
            'quizTakens',
            'workerScores',
            'selectionCandidates',
            'workerQuizScores'
        );

        foreach ($reset as $item) {

            if ($worker->{$item}) {

                foreach ($worker->{$item} as $reset_item) {
                    $reset_item->delete();
                }
            }
        }

        $this->redirect(
            Yii::app()->createUrl('user/admin')
        );
    }

    public function actionQuiz($id)
    {
        $worker = Worker::model()->findByPk($id);

        if (is_null($worker)) {
            throw new CHttpException(404);
        }

        $iq = IqTable::model()->findByAttributes(array(
            'score' => $worker->getScore(Yii::app()->params['iq_type_id'])
        ));

        $types = Type::model()->findAll();
        $types = CHtml::listData($types, 'id', 'name');

        if (isset($_POST['WorkerScore'])) {

            $sum = array();
            $valid = true;

            foreach ($_POST['WorkerScore'] as $type_id => $score) {
                $workerScore = new WorkerScore();
                $workerScore->score = $score['score'];
                $workerScore->type_id = $type_id;

                if (!$workerScore->validate(array('score'))) {
                    $valid = false;
                }

                $workerScores[$type_id] = $workerScore;
                $sum[$type_id] = $score['score'];
            }

            if ($valid) {
                $response = WorkerScore::model()->generateScore($id, $sum);
                echo CJSON::encode(array('generalScore' => $worker->getGeneralScore($sum)));
            } else {
                echo CActiveForm::validateTabular($workerScores, array('score'));
            }

            Yii::app()->end();
        }

        $this->render('quiz', array(
            'worker' => $worker,
            'types' => $types,
            'iq' => $iq,
        ));
    }

    public function actionAdmin()
    {
        $model = new User('search');
        $model->unsetAttributes();

        if (isset($_GET['User'])) {
            $model->attributes = $_GET['User'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = User::model()->findByPk($id);
        $model = is_null($model) ? new User : $model;

        return $model;
    }
}
