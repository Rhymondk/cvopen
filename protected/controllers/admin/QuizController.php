<?php
class QuizController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['Quiz'])) {

            $model->attributes = $_POST['Quiz'];

            if ($model->validate()) {

                $model->save(false);

                $this->formRedirect(array(
                    'save' => $this->createUrl('quiz/admin'),
                    'add-questions' => $this->createUrl('quiz/question', array('id' => $model->id))
                ));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model
        ));
    }

    public function actionQuestion($id)
    {
        $quizModel = Quiz::model()->findByPk($id);

        if (is_null($quizModel)) {
            throw new CHttpException(404, 'Testas nerastas');
        }

        $model = new QuizQuestion;

        if (isset($_POST['QuizQuestion'])) {

            $model->deleteAllByAttributes(array(
                'quiz_id' => $id,
            ));

            if (!empty($_POST['QuizQuestion']['question_id'])) {
                foreach ($_POST['QuizQuestion']['question_id'] as $question_id) {
                    $model = new QuizQuestion;
                    $model->question_id = $question_id;
                    $model->quiz_id = $id;
                    $model->save(false);
                }
            }

            $this->redirect($this->createUrl('quiz/admin'));
        }

        $questions = array();

        if (!empty($quizModel->quizQuestions)) {
            foreach ($quizModel->quizQuestions as $question) {
                $questions[] = $question['question_id'];
            }
        } else {
            $questions = array();
        }

        $model->question_id = $questions;

        $this->render('question', array(
            'model' => $model
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Quiz('search');
        $model->unsetAttributes();

        if (isset($_GET['quiz'])) {
            $model->attributes = $_GET['Quiz'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Quiz::model()->findByPk($id);
        $model = is_null($model) ? new Quiz : $model;

        return $model;
    }
}
