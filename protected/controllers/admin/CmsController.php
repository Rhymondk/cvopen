<?php
class CmsController extends AdminController
{
    public function actionUpdate($id = 0)
    {
        $model = $this->loadModel($id);

        if (isset($_POST['Cms'])) {
            $model->attributes = $_POST['Cms'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('cms/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Cms('search');
        $model->unsetAttributes();

        if (isset($_GET['Cms'])) {
            $model->attributes = $_GET['Cms'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Cms::model()->findByPk($id);

        if (is_null($model)) {
            throw new CHttpException(404, Yii::t('web', 'CMS puslapis nerastas'));
        }

        return $model;
    }
}
