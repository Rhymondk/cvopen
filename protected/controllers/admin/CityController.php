<?php
class CityController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['City'])) {
            $model->attributes = $_POST['City'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('city/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new City('search');
        $model->unsetAttributes();

        if (isset($_GET['City'])) {
            $model->attributes = $_GET['City'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = City::model()->findByPk($id);
        $model = is_null($model) ? new City : $model;

        return $model;
    }
}
