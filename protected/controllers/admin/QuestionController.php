<?php

class QuestionController extends AdminController
{
    public function actionUpload()
    {
        $model = new Question();
        parent::uploadFile($model, 'imageFile', 'image');
    }

    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['Question'])) {
            $model->attributes = $_POST['Question'];

            if ($model->validate()) {
                $model->save(false);

                if (!empty($_POST['Question']['image'])) {
                    $model->image = Yii::getPathOfAlias('webroot') . $_POST['Question']['image'];
                }

                if (!empty($model->image) && file_exists($model->image)) {
                    rename(
                        $model->image,
                        'images/' . get_class($model) . '/' . $model->id . '.png'
                    );

                    $model->image = 'images/' . get_class($model) . '/' . $model->id . '.png';
                    $model->save(false);
                }

                $this->jsonRedirect(array(
                    'save' => $this->createUrl('question/admin'),
                    'create-answer' => $this->createUrl('answer/create', array('qid' => $model->id))
                ));
            } else {
                echo CActiveForm::validate($model);
            }

            Yii::app()->end();
        }

        if ($id) {
            $answerModel = new Answer;
            $answerModel->question_id = $id;
        }

        $this->render('form', array(
            'model' => $model,
            'answerModel' => $id ? $answerModel : null,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new Question('search');
        $model->unsetAttributes();

        if (isset($_GET['Question'])) {
            $model->attributes = $_GET['Question'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = Question::model()->findByPk($id);
        $model = is_null($model) ? new Question : $model;

        return $model;
    }
}
