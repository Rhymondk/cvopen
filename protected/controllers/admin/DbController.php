<?php

class DbController extends AdminController
{
    public function actionMigrate()
    {
        try {
            $migrateFile = Yii::getPathOfAlias('webroot') . '/migration/migration.txt';

            if (!file_exists($migrateFile)) {
                echo 'Failo nėra';
                die();
            }

            $sql = file_get_contents($migrateFile);

            if (empty(trim($sql))) {
                echo 'Nėra SQL';
                die();
            }

            Yii::app()->db->createCommand($sql)->execute();
            file_put_contents($migrateFile, "");
            echo 'OK';
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }


    public function actionGenerateScore()
    {
        $workers = Worker::model()->findAll();
        $types = CHtml::listData(Type::model()->findAll(), 'id', 'id');

        while($worker = array_pop($workers)){

            echo $worker->user_id . '<br>';

            if (!$worker->complete) {
                continue;
            }

            $sum = array();
            foreach ($types as $type) {
                $sum[$type] = 0;
            }

            if (!empty($worker->quizTakens)) {
                foreach ($worker->quizTakens as $quizTaken) {
                    $scores = $quizTaken->getScore();
                    foreach ($scores as $id => $score) {
                        $sum[$id] += $score;
                    }
                }
            }

            $sum[0] = $worker->getGeneralScore($sum);
            $types[0] = 0;

            foreach ($types as $type) {
                $workerScore = new WorkerScore();
                $workerScore->worker_user_id = $worker->user_id;
                $workerScore->score = $sum[$type];
                $workerScore->type_id = $type;
                $workerScore->save(false);
                $workerScore = null;
            }
        }
    }
}
