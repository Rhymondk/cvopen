<?php

class TypeDescriptionController extends AdminController
{
    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $id = Yii::app()->request->getParam('id', 0);
        $model = $this->loadModel($id);

        if (isset($_POST['TypeDescription'])) {
            $model->attributes = $_POST['TypeDescription'];

            if ($model->validate()) {
                $model->save(false);

                $this->redirect(array('typeDescription/admin'));
            } else {
                $model->getErrors();
            }
        }

        $this->render('form', array(
            'model' => $model,
        ));
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $this->loadModel($id)->delete();
        }
    }

    public function actionAdmin()
    {
        $model = new TypeDescription('search');
        $model->unsetAttributes();

        if (isset($_GET['TypeDescription'])) {
            $model->attributes = $_GET['TypeDescription'];
        }

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function loadModel($id)
    {
        $model = TypeDescription::model()->findByPk($id);
        $model = is_null($model) ? new TypeDescription : $model;

        return $model;
    }
}
