<?php

class AnswerController extends AdminController
{
    public function actionUpload()
    {
        $model = new Answer();
        parent::uploadFile($model, 'imageFile', 'image');
    }

    public function actionCreate()
    {
        $this->actionUpdate();
    }

    public function actionUpdate()
    {
        $model = $this->loadModel(Yii::app()->request->getParam('id', 0));

        if (isset($_POST['Answer'])) {
            $model->attributes = $_POST['Answer'];

            $model->question_id = Yii::app()->request->getParam('qid', $model->question_id);

            if ($model->validate()) {
                $model->save(false);

                if (!empty($_POST['Answer']['image'])) {
                    $model->image = Yii::getPathOfAlias('webroot') . $_POST['Answer']['image'];
                }

                if (!empty($model->image) && file_exists($model->image)) {
                    rename(
                        $model->image,
                        'images/' . get_class($model) . '/' . $model->id . '.png'
                    );

                    $model->image = 'images/' . get_class($model) . '/' . $model->id . '.png';
                    $model->save(false);
                }

                $this->updateScore($model);
                $this->jsonRedirect(array(
                    'save' => $this->createUrl('question/update', array('id' => $model->question_id)),
                    'create-answer' => $this->createUrl('answer/create', array('qid' => $model->question_id))
                ));
            } else {
                echo CActiveForm::validate($model);
            }

            Yii::app()->end();
        }

        $this->render('form', array(
            'model' => $model,
            'types' => Type::model()->findAll(),
        ));
    }

    public function updateScore($model)
    {
        if (isset($_POST['Score'])) {
            foreach ($_POST['Score'] as $type_id => $score) {
                $scoreModel = $this->getScoreModel($type_id, $model->id);
                $scoreModel->type_id = $type_id;
                $scoreModel->value = $score['value'];
                $scoreModel->answer_id = $model->id;
                $scoreModel->save();
            }
        }
    }

    public function getScoreModel($type_id, $answer_id)
    {
        $scoreModel = Score::model()->findByAttributes(array(
            'type_id' => $type_id,
            'answer_id' => $answer_id
        ));

        if ($scoreModel === null) {
            $scoreModel = new Score;
        }

        return $scoreModel;
    }

    public function actionDelete($id)
    {
        if (Yii::app()->request->isPostRequest) {
            $model = $this->loadModel($id);
            $model->delete();
        }
    }

    public function loadModel($id)
    {
        $model = Answer::model()->findByPk($id);
        $model = is_null($model) ? new Answer : $model;

        return $model;
    }
}
