<?php

class LoginFormWidget extends CWidget
{
    public function run()
    {
        $user = new LoginForm;

        $this->render('loginForm', array(
            'user' => $user,
        ));
    }
}

?>

