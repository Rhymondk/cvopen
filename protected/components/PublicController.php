<?php

class PublicController extends CController
{
    public $layout = '//layouts/main';
    public $breadcrumb;
    public $timeleft;
    public $imageBlock; //For emails

    public function beforeAction($action)
    {
        parent::beforeAction($action);

        if ((int) Yii::app()->params['maintenance'] == 1 && $this->route != 'site/maintenance' && !$this->isRole('admin')) {
            $this->redirect(Yii::app()->createUrl('site/maintenance'));
        } else if ($this->route == 'site/maintenance' && $this->isRole('admin')) {
            $this->redirect(Yii::app()->createUrl('site/index'));
        }

        return true;
    }

    public function numberString($number, $root, $endings)
    {
        $number = (string) $number;

        if ($number[strlen($number) - 1] == 0 || ($number > 10 && $number <= 19)) {
            $end = $endings[0];
        } else if ($number[strlen($number) - 1] == 1) {
            $end = $endings[1];
        } else {
            $end = $endings[2];
        }

        return $root . $end;
    }

    public function uid()
    {
        return md5(uniqid('', true));
    }

    public function isRole($role)
    {
        $role = ucfirst($role);
        return isset(Yii::app()->user->{'is' . $role}) && Yii::app()->user->{'is' . $role};
    }

    public static function isWorker()
    {
        return isset(Yii::app()->user->isWorker) && Yii::app()->user->isWorker;
    }

    public static function isAdmin()
    {
        return isset(Yii::app()->user->isAdmin) && Yii::app()->user->isAdmin;
    }

    public static function isEmployer()
    {
        return isset(Yii::app()->user->isEmployer) && Yii::app()->user->isEmployer;
    }

    public static function isGuest()
    {
        return isset(Yii::app()->user->isGuest) && Yii::app()->user->isGuest;
    }

    public function today()
    {
        return date('Y-m-d H:i:s');
    }
}
?>
