<?php
class Billing
{
    protected $config;
    protected $employer;
    private $curlHeaders;

    public function __construct($employer)
    {
        $this->config = array(
            'apiKey' => Yii::app()->params['saskaitos_api_key'],
            'companyId' => Yii::app()->params['saskaitos_id']
        );

        $this->employer = $employer;
        $this->getCompanyDetails();
        $this->getCustomer();
    }

    public function getCustomer()
    {
        $customerParams = array(
            'type' => 'customer',
            'action' => 'create', // search (by any param in array) | get | create | update | delete
            'Customer' => array(
                'Name' => $this->employer->contact_person,
                'Address' => $this->employer->address,
                'Phone' => $this->employer->number,
                'Email' => $this->employer->user->email,
                'Bank' => array(
                    'Account' => 'LT1234567890',
                ),
                'Company' => array(
                    'Name' => $this->employer->user->name,
                    'Code' => $this->employer->code,
                    'Tax' => $this->employer->vat_code,
                )
            ),
        );

        $results = json_decode($this->UseCURLSocket('http://www.saskaitos.lt/api', array_merge($this->config, $customerParams)));
        $this->customer = $results->Customer;
    }

    public function getCompanyDetails()
    {
        $companyParams = array(
            'type'      => 'company',
            'action'    => 'get',
            'Company' => array(
                'ID' => $this->config['companyId']
            ),
        );

        $results = (object) json_decode($this->UseCURLSocket('http://www.saskaitos.lt/api', array_merge($this->config, $companyParams)));
        $this->company = $results->Company;
    }

    public function createBill($selection)
    {
        $newSeriaParams = array(
            'type'      => 'bill',
            'action'    => 'seria',
            'Bill' => array(
                'Type' => 'pre',
            ),
        );

        $respondNewSeria = json_decode($this->UseCURLSocket('http://www.saskaitos.lt/api', array_merge($this->config, $newSeriaParams)));

        list($priceTable, $total) = SelectionHosting::model()->getPriceTableInfo(
            $selection->date_start,
            $selection->date_end,
            CHtml::listData($selection->hostings, 'id', 'selection_hosting_plan_id'),
            $selection->host_type
        );

        $items = array();
        foreach ($priceTable as $service) {
            $items['Count'][] = 1;
            $items['Name'][] = $service['name'];
            $items['Price'][] = $service['number'];
            $items['Type'][] = 'Paslauga';
        }


        $billParams = array(
            'type'      => 'bill',
            'action'    => 'create',
            'Bill' => array(
                'Customer' => array(
                    'ID' => $this->customer->ID,
                ),
                'Prefix'        => $respondNewSeria->Seria->Prefix,
                'Number'        => $respondNewSeria->Seria->Number + 54,
                'Date'          => date("Y-m-d"),
                'PayDate'       => date("Y-m-d", strtotime("+14 days")),
                'Currency'      => 'EUR',
                'PaymentAmount' => '',
                'PaymentDate'   => '',
//                'Recipient'     => $this->employer->user->email,
                'TaxPercentage' => $this->company->Tax->Percentage,
                'Type'          => 'pre',
                'Wrote'         => 'Renatas parojus',
                'Comment'       => 'Mokėjimo paskirtyje įrašykite: ' . $selection->id,
                'Item'          => $items,
            ),
        );

        $respondBill = json_decode($this->UseCURLSocket('http://www.saskaitos.lt/api', array_merge($this->config, $billParams)));

        if ($respondBill->Type == 'success') {
            return $respondBill->Bill->PDF;
        } else {
            throw new CHttpException(404);
        }
    }



    public function UseCURLSocket( $url, $post_fields = '', $timeout=10, $cookie = null, $UA = "XML Sender", $proxy = null, $extended = false )
    {
        $this->curlHeaders = array();
        $post_fields = http_build_query($post_fields);

        $cu = curl_init();
        curl_setopt($cu, CURLOPT_URL, $url);

        if ($UA) {
            curl_setopt($cu, CURLOPT_USERAGENT, $UA);
        }

        curl_setopt($cu, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($cu, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($cu, CURLOPT_SSL_VERIFYPEER, false);
        @curl_setopt($cu, CURLOPT_FOLLOWLOCATION, true);

//            @curl_setopt($cu, CURLOPT_INTERFACE, $_SERVER['SERVER_ADDR']);

        $proxy = explode(":", $proxy);
        if ($proxy[0] && $proxy[1]) {
            if ($proxy[2] == 'SOCK5') {
                curl_setopt($cu, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);
            }

            curl_setopt($cu, CURLOPT_PROXY, trim($proxy[0]));
            curl_setopt($cu, CURLOPT_PROXYPORT, trim($proxy[1]));
        }

        if (strlen($post_fields) > 0) {
            curl_setopt($cu, CURLOPT_POST, 1);
            curl_setopt($cu, CURLOPT_POSTFIELDS, $post_fields);
        }

        curl_setopt($cu, CURLOPT_HEADERFUNCTION, array($this, 'curlHeaderCallback'));
        curl_setopt($cu, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($cu, CURLOPT_CONNECTTIMEOUT, $timeout);

        if (strstr($cookie, '=')) {
            curl_setopt($cu, CURLOPT_COOKIE, $cookie);
        }

        $err_no = curl_errno($cu);
        $err_msg = null;
        $succeeded = $err_no == 0 ? true : false;

        if (!$succeeded) {
            $err_msg = curl_error($cu);
            trigger_error("SOCKET", "\ncURL unable to connect to " . $url . ": $err_no $err_msg<br>", '', true);
        } elseif ($cu) {
            $output = curl_exec($cu);

            if (false === $output) {
                $err_no = curl_errno($cu);
                $err_msg = curl_error($cu);
            }
        }

        if ($extended) {
            return array(
                'data' => $output,
                'error' => $err_msg,
                'errno' => $err_no
            );
        } else {
            return $output;
        }


    }

    public function curlHeaderCallback($resURL, $strHeader)
    {
        $this->curlHeaders[] = trim($strHeader);

        return strlen($strHeader);

    }
}