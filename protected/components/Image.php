<?php
Class Image extends Gregwar\Image\Image {

    protected $cacheDir = 'images/Cache';

    public function cacheFile($type = 'jpg', $quality = 80, $actual = false) {
        $filename = parent::cacheFile($type, $quality, $actual);
        return '/' . $filename;
    }
}