<?php

class Mailer extends YiiMailer
{

    public static function app()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new Mailer();
            $instance->createSmtp();
        }

        return $instance;
    }

    public function createSmtp()
    {
        $this->setSmtp('mail.cvopen.lt', 25, '', true, 'info@cvopen.lt', 'opencv123');
    }

    public function sendText($email, $text, $subject, $from = false)
    {
        if (!$from) {
            $from = 'CV open';
        }

        try {
            $this->clearLayout();
            $this->setFrom(Yii::app()->params['mail_email'], $from);
            $this->setTo($email);
            $this->setSubject($subject);
            $this->setBody($text);
            $this->send();
        } catch (Exception $e) {
            throw new CHttpException(404, $e->getMessage());
        }
    }

    public function sendTemplate($email, $view, $data = array(), $attachment = null, $attachmentName = null, $from = false, $subject = false, $fromName = 'CV open')
    {
        parent::__construct($view, $data, 'basic');

        if (!$subject) {
            $subject = Yii::t('mailSubject', $view);
        }

        try {

            if (!is_null($attachment)) {
                $this->AddStringAttachment($attachment, $attachmentName);
            }

            $this->IsHTML(true);
            $this->setFrom(Yii::app()->params['mail_email'], $fromName);
            $this->setTo($email);
            $this->setSubject($subject);
            $this->send();
        } catch (Exception $e) {
            throw new CHttpException(404, $e->getMessage());
        }
    }

    public function setSmtp($host='localhost',$port=25, $secure='', $auth=false, $username='', $password='')
    {
        $this->isSMTP();
        $this->Host = $host;
        $this->Port = $port;
        $this->SMTPSecure = $secure;
        $this->SMTPAuth = $auth;
        $this->Username = $username;
        $this->Password = $password;
    }

}

?>
