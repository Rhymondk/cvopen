<?php

Yii::import('booster.widgets.TbFileUpload');

class FileUpload extends TbFileUpload
{
    public $form;

    public $attributeUrl;

    public $formView = 'application.components.views.fileUpload.form';

    public $uploadView = 'application.components.views.fileUpload.upload';

    public $downloadView = 'application.components.views.fileUpload.download';

    public $previewImagesView = 'application.components.views.fileUpload.preview';

    public $options = array(
        'autoUpload' => true,
    );

    public function run()
    {

        list($name, $id) = $this->resolveNameID();

        $this->htmlOptions['id'] = $this->id;

        $this->options['url'] = $this->url;
        $this->options['autoUpload'] = true;
        $this->options['added'] = 'js:function() {$(\'.fileupload-table .preview\').remove();}';
        $this->options['completed'] = 'js:function(e, data) {
            $(\'#fileupload-image-url\').val($(\'#original-image\').data(\'url\'))
        }';

        // if acceptFileTypes is not set as option, try getting it from models rules
        if (!isset($this->options['acceptFileTypes'])) {
            $fileTypes = $this->getFileValidatorProperty($this->model, $this->attribute, 'types');
            if (isset($fileTypes)) {
                $fileTypes = (preg_match(':jpg:', $fileTypes) && !preg_match(':jpe:', $fileTypes) ? preg_replace(
                    ':jpg:',
                    'jpe?g',
                    $fileTypes
                ) : $fileTypes);
                $this->options['acceptFileTypes'] = 'js:/(\.)(' . preg_replace(':,:', '|', $fileTypes) . ')$/i';
            }
        }

        // if maxFileSize is not set as option, try getting it from models rules
        if (!isset($this->options['maxFileSize'])) {
            $fileSize = $this->getFileValidatorProperty($this->model, $this->attribute, 'maxSize');
            if (isset($fileSize)) {
                $this->options['maxFileSize'] = $fileSize;
            }
        }

        if ($this->multiple) {
            $this->htmlOptions["multiple"] = true;
        }

        $this->render($this->uploadView);
        $this->render($this->downloadView);
        $this->render($this->formView, array('name' => $name, 'htmlOptions' => $this->htmlOptions));

        if ($this->previewImages || $this->imageProcessing) {
            $this->render($this->previewImagesView);
        }

        $this->registerClientScript($this->htmlOptions['id']);
    }

    private function getFileValidatorProperty($model = null, $attribute = null, $property = null)
    {
        if (!isset($model, $attribute, $property)) {
            return null;
        }

        foreach ($model->getValidators($attribute) as $validator) {
            if ($validator instanceof CFileValidator) {
                $ret = $validator->$property;
            }
        }
        return isset($ret) ? $ret : null;
    }
}