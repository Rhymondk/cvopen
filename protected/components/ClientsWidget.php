<?php

class ClientsWidget extends CWidget
{
    public function run()
    {
        $clients = Client::model()->findAll('status = 1');

        $this->render('clients', array(
            'clients' => $clients,
        ));
    }
}

?>

