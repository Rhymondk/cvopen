<?php

class WorkerPDF extends CWidget
{

    public static function app()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new WorkerPDF();
        }

        return $instance;
    }

    public function getOutput($worker_id)
    {
        $mpdf = new mPDF('', 'A4', 14, 'opensans', 0, 0, 0, 0, 0, 0);
        $worker = Worker::model()->findByPk($worker_id);

        if (is_null($worker)) {
            throw new CHttpException(404, 'Darbuotojas nerastas');
        }

        $iqTable = IqTable::model()->findByAttributes(array(
            'score' => $worker->getScore(Yii::app()->params['iq_type_id'])
        ));

        $types = Type::model()->findAll();
        $typeInfo = array();

        foreach ($types as $type) {
            $typeInfo[$type->id] = $worker->getTypeAttribute($type->id);
            $typeInfo[$type->id]['type'] = $type->name;
        }

        $typeDisplay = explode(',', Yii::app()->params['type_print_display']);

        $html = $this->render('print', array(
            'worker' => $worker,
            'iqTable' => $iqTable,
            'iqQuizResults' => $worker->getIqQuizResults(),
            'typeInfo' => $typeInfo,
            'typeDisplay' => $typeDisplay
        ), true);

        $mpdf->WriteHTML($html);

        return $mpdf->output('', 'S');
    }
}

?>
