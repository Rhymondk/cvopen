<?php
class AdminController extends CController
{
    public $layout = '//layouts/main';
    public $breadcrumbs;
    public $imageBlock;

    public function filters()
    {
        return array(
            'accessControl',
        );
    }
 
    public function accessRules()
    {
        return array(
            array('allow',
                'users' => array('*'),
                'actions' => array('login'),
            ),
            array('allow',
                'expression' => array('AdminController', 'isAdmin')
            ),
            array('deny',
                'users' => array('*'),
                'deniedCallback' => function() {
                    $this->redirect(Yii::app()->createUrl('site/login'));
                }
            ),
        );
    }

    private function uid()
    {
        return md5(uniqid('', true));
    }

    public function uploadFile($model, $attribute, $attributeUrl)
    {
        header('Vary: Accept');

        if (isset($_SERVER['HTTP_ACCEPT']) && 
            (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            header('Content-type: application/json');
        } else {
            header('Content-type: text/plain');
        }
        
        $model->{$attribute} = CUploadedFile::getInstance($model, $attribute);

        if ($model->{$attribute} !== null  && $model->validate(array($attribute))) {
            $tmpFileName = 'tmp/' . $this->uid() . '.png';
            $model->{$attribute}->saveAs($tmpFileName);
            $model->{$attributeUrl} = $tmpFileName;

            $data[] = array(
                'name' => $model->{$attribute}->name,
                'type' => $model->{$attribute}->type,
                'size' => $model->{$attribute}->size,
                'hiddenAttribute' => '#' . get_class($model) . '_' . $attributeUrl,
                'url'  => $tmpFileName,
                'thumbnail_url' => Image::open($tmpFileName)->resize(150, 150)->png(),
            );
        } else {

            if ($model->hasErrors($attribute)) {
                $data[] = array('error', $model->getErrors($attribute));
            } else {
                throw new CHttpException(500, "Could not upload file ".     CHtml::errorSummary($model));
            }
        }

        echo CJSON::encode($data);
        Yii::app()->end();
    }

    public static function isAdmin()
    {
        return isset(Yii::app()->user->isAdmin) && Yii::app()->user->isAdmin ? true : false;
    }

    public function formRedirect($redirects)
    {
        foreach($redirects as $postValue => $redirect) {
            
            if (isset($_POST[$postValue])) {
                $this->redirect($redirect);
            }
        }
    }

    public function jsonRedirect($redirects)
    {
        foreach($redirects as $postValue => $redirect) {
            
            if (isset($_POST[$postValue])) {
                echo CJSON::encode(array(
                    'redirect' => $redirect,
                ));
            }
        }
    }
}
?>
