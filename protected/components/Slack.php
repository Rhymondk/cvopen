<?php

Class Slack
{
    private $emoji = 'scream_cat';
    private $channel = 'cvopen';
    private $username = 'Klaida';

    public static function app()
    {
        static $instance = null;
        if (null === $instance) {
            $instance = new Slack();
        }

        return $instance;
    }

    public function message($message)
    {
        $data = "payload=" . json_encode(array(
                "channel" => '#' . $this->channel,
                "text" => $message,
                "icon_emoji" => ':' . $this->emoji . ':',
                "username" => $this->username
            ));

        $this->send($data);
    }

    public function error($error)
    {
        $data = "payload=" . json_encode(array(
                "channel" => '#' . $this->channel,
                "mrkdwn" => true,
                "attachments" => array(
                    array(
                        "fallback" => $error['message'],
                        "title" => $error['message'],
                        "text" => $error['message'],
                        "color" => "#F35A00",
                        "fields" => array(
                            array(
                                "title" => "Line",
                                "value" => $error['line'],
                                "short" => true
                            ),
                            array(
                                "title" => "Type",
                                "value" => $error['type'],
                                "short" => true
                            ),
                            array(
                                "title" => 'In',
                                "value" => $error['file'],
                                "short" => false
                            )
                        )
                    )
                ),
                "icon_emoji" => ':' . $this->emoji . ':',
                "username" => $this->username
            ));

        //var_dump($data); die();
        $this->send($data);
    }

    public function send($data)
    {
        $ch = curl_init("https://hooks.slack.com/services/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");
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
    }
}

?>