<?php

class UserIdentity extends CUserIdentity
{
    public $userType = 'Public';
    public $id;

    public function authenticate()
    {
        require_once Yii::app()->basePath . '/extensions/hoauth/models/UserOAuth.php';

        if ($this->userType == 'Social') {

            $record = User::model()->findByAttributes(array('email' => $this->username));

            if (is_null($record)) {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            } elseif (!UserOAuth::model()->findUser($record->id)) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } else {
                $role = Role::model()->findByPk($record->role_id);
                $this->setState('is' . $role->model, 1);
                $this->setState('role', $role->model);
                $this->setState('name', $record->name);
                $this->id = $record->id;
                $this->errorCode = self::ERROR_NONE;
            }

        } elseif ($this->userType == 'Public') {

            $record = User::model()->findByAttributes(array('email' => $this->username));

            if (is_null($record)) {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            } elseif (!CPasswordHelper::verifyPassword($this->password, $record->password)) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } else {
                $role = Role::model()->findByPk($record->role_id);
                $this->setState('is' . $role->model, 1);
                $this->setState('role', $role->model);
                $this->setState('name', $record->name);
                $this->id = $record->id;
                $this->errorCode = self::ERROR_NONE;
            }

        } elseif ($this->userType == 'Admin') {

            if ($this->username != Yii::app()->params['admin_username']) {
                $this->errorCode = self::ERROR_USERNAME_INVALID;
            } elseif ($this->password != Yii::app()->params['admin_password']) {
                $this->errorCode = self::ERROR_PASSWORD_INVALID;
            } else {
                $this->setState('isAdmin', 1);
                $this->setState('role', 3);
                $this->setState('name', 'admin');
                $this->errorCode = self::ERROR_NONE;
            }

        }

        return !$this->errorCode;
    }

    public function getId()
    {
        return $this->id;
    }
}

?>
