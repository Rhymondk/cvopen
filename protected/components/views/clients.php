<div class="our-clients">
    <div class="row">
        <div class="col-md-12 heading">
            <div class="heading-arrow-icon">
                <?= Yii::t('web', 'Mūsų'); ?> <span class="blue-color"><?= Yii::t('web', 'klientai'); ?></span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="client-logos">
            <?php foreach ($clients as $client) { ?>
                <div class="item">
                    <?=
                    CHtml::link(
                        CHtml::image(
                            Image::open($client->image)->resize(235, 100)->png(),
                            $client->name),
                        $client->link);
                    ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>