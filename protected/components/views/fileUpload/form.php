<?php
/**
 * The file upload form used as target for the file upload widget
 *
 * @var TbFileUpload $this
 * @var array $htmlOptions
 */
?>
<div class="row-fluid">
    <table class="fileupload-table">
        <tbody class="files" data-toggle="modal-gallery" data-target="#modal-gallery">
            <?php if ($this->model->image) { ?>
            <tr class="preview">
                <td><img src="<?= Image::open($this->model->image)->resize(150, 150)->png(); ?>"></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>
<div class="fileupload-buttonbar">
    <div class="row">
        <div class="col-md-12 form-group">
            <!-- The fileinput-button span is used to style the file input field as button -->
            <span class="btn btn-success fileinput-button"> <i class="icon-plus icon-white"></i>
                <span><?= Yii::t('web', 'Pridėti failą'); ?></span>
                    <?= $this->form->fileField($this->model, $this->attribute, $htmlOptions); ?>
                    <?= $this->form->hiddenField($this->model, $this->attributeUrl, array('id' => 'fileupload-image-url')); ?>
            </span>
            <?= $this->form->error($this->model, $this->attributeUrl); ?>
            <input type="checkbox" class="toggle">
        </div>
    </div>
</div>
<!-- The loading indicator is shown during image processing -->
<div class="fileupload-loading"></div>
<!-- The table listing the files available for upload/download -->

