<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    {% if (file.error) { %}
    <tr class="error">
        <td class="error" colspan="2"><span class="label label-danger">{%=locale.fileupload.error%}</span> {%=locale.fileupload.errors[file.error] || file.error%}</td>
    </tr>
    {% } %}
    {% if (file.thumbnail_url) { %}
    <tr class="preview">
        <td>
            <img src="{%=file.thumbnail_url%}">
            <div id="original-image" class="hidden" data-url="/{%=file.url%}"></div>
        </td>
    </tr>
    {% } %}
{% } %}
</script>
