<div>
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'login-form',
        'action' => Yii::app()->createUrl('user/login'),
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
                if (!hasError) {
                    app.redirect(data.redirect);
                }
            }'
        ),
    ));
    ?>

    <div class="heading blue-color"><?= Yii::t('web', 'Prisijunkite') ?></div>

    <?php

    echo $form->textField($user, 'username', array(
        'class' => 'input-icon input-icon-mail',
        'placeholder' => Yii::t('web', 'El. paštas')
    ));

    echo $form->error($user, 'username');

    echo $form->passwordField($user, 'password', array(
        'class' => 'input-icon input-icon-password',
        'placeholder' => Yii::t('web', 'Slaptažodis')
    ));

    echo $form->error($user, 'password');

    echo CHtml::submitButton(Yii::t('web', 'Prisijungti'), array(
        'class' => 'btn btn-blue btn-fluid'
    ));
    ?>

    <div class="text-center forgot-password">
        <?php
        echo CHtml::link(Yii::t('web', 'Pamiršau slaptažodį'), Yii::app()->createUrl('user/recoverPassword'), array(
            'class' => 'green-color'
        ));
        ?>
    </div>

    <?php
    $this->endWidget();
    ?>
</div>