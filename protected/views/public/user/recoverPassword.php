<?php $this->breadcrumb = Yii::t('web', 'Priminti slaptažodį'); ?>
<div class="container">
    <div class="row">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'recover-password-form',
        ));
        ?>
        <div class="legend blue-color col-md-12">
            <?= Yii::t('web', 'Įveskite savo el. paštą'); ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->textField($user, 'email', array(
                'placeholder' => $user->getAttributeLabel('email'),
                'value' => '',
            ));
            ?>
            <?= $form->error($user, 'email'); ?>
        </div>
        <div class="col-md-2">
            <?=
            CHtml::submitButton(Yii::t('web', 'Siųsti'), array(
                'class' => 'btn btn-blue'
            ));
            ?>
        </div>
        <div class="col-md-12 helper">
            <?= Yii::t('web', 'Į nurodytą el. paštą išsiųstime nuorodą su slaptažodžio keitimo forma.') ?>
        </div>
        <?php
        $this->endWidget();
        ?>
    </div>
</div>