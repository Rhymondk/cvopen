<?php $this->breadcrumb = 'Keisti slaptažodį'; ?>
<div class="container">
    <div class="row">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'password-form',
        ));
        ?>
        <div class="col-md-4">
            <?=
            $form->passwordField($user, 'password', array(
                'placeholder' => Yii::t('web', 'Naujas slaptažodis'),
                'value' => '',
            ));
            ?>
            <?= $form->error($user, 'password'); ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->passwordField($user, 'repeatPassword', array(
                'placeholder' => $user->getAttributeLabel('repeatPassword'),
                'value' => '',
            ));
            ?>
            <?= $form->error($user, 'repeatPassword'); ?>
        </div>
        <div class="col-md-4">
            <?=
            CHtml::submitButton(Yii::t('web', 'Keisti'), array(
                'class' => 'btn btn-blue'
            ))
            ?>
        </div>
        <?php
        $this->endWidget();
        ?>
    </div>
</div>