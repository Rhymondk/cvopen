<?php $this->breadcrumb = Yii::t('web', 'Nuoroda išsiųsta'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="legend"><?= Yii::t('web', 'Nuoroda išsiųsta') ?></div>
            <div class="description">
                <?= Yii::t('web', 'Pasitikrinkite savo el. pašto dėžutę') ?>
            </div>
            <div class="back">
                <?=
                CHtml::link(
                    Yii::t('web', 'Grįžti į pagrindinį puslapį'),
                    Yii::app()->createUrl('site/index'),
                    array(
                        'class' => 'btn btn-blue'
                    )
                );
                ?>
            </div>
        </div>
    </div>
</div>
