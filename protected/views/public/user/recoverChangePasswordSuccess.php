<?php $this->breadcrumb = Yii::t('web', 'Slaptažodis pakeistas sėkmingai!'); ?>
<div class="container success-page">
    <div class="row">
        <div class="col-md-12">
            <div class="legend"><?= Yii::t('web', 'Slaptažodis pakeistas') ?></div>
            <div class="description">
                <?= Yii::t('web', 'Dabar galite prisijunkti naudodami savo naują slaptažodį') ?>
            </div>
            <div class="back">
                <?=
                CHtml::link(
                    Yii::t('web', 'Grįžti į pagrindinį puslapį'),
                    Yii::app()->createUrl('site/index'),
                    array(
                        'class' => 'btn btn-blue'
                    )
                );
                ?>
            </div>
        </div>
    </div>
</div>
