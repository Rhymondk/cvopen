<li><?= CHtml::link(Yii::t('web', 'Atrankos'), Yii::app()->createUrl('worker/selections')) ?></li>
<li><?= CHtml::link(Yii::t('web', 'Apie mus'), '#video') ?></li>
<li><?= CHtml::link(Yii::t('web', 'DUK'), Yii::app()->createUrl('faq/index')) ?></li>
<li>
    <?=
    CHtml::button(Yii::t('web', 'Prisijungti'), array(
        'class' => 'btn btn-blue popover-trigger',
        'data-popover' => 'login-form-popover',
        'id' => 'login-button',
    ));
    ?>
    <div class="popover bottom col-md-1" id="login-form-popover">
        <div class="arrow"></div>
        <div class="popover-content">
            <?php $this->widget('LoginFormWidget') ?>
        </div>
    </div>
</li>
<li>
    <?=
    CHtml::link(Yii::t('web', 'Registracija'), Yii::app()->getHomeUrl() . '#registration', array(
        'class' => 'btn btn-green scroll-to',
        'data-scroll' => '.registration'
    ));
    ?>
</li>