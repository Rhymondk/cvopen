<li><i class="icon icon-selection"></i><?= CHtml::link(Yii::t('web', 'Mano atrankos'), Yii::app()->createUrl('employer/selections')) ?></li>
<li><i class="icon icon-question"></i><?= CHtml::link(Yii::t('web', 'DUK'), Yii::app()->createUrl('faq/index')) ?></li>
<li><i class="icon icon-settings"></i><?= CHtml::link(Yii::app()->user->name, Yii::app()->createUrl('employer/edit')); ?></li>
<li><a href="<?= Yii::app()->createUrl('user/logout'); ?>" class="icon icon-logout"></a></li>
