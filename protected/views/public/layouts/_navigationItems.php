<ul>
    <?php
    if (Yii::app()->controller->id == 'quiz') {
        $this->renderPartial('//layouts/_navItemsQuiz');
    } else if (Yii::app()->user->isGuest) {
        $this->renderPartial('//layouts/_navItemsGuest');
    } else if (isset(Yii::app()->user->role)) {
        if (Yii::app()->user->role == 'Employer') {
            $this->renderPartial('//layouts/_navItemsEmployer');
        } else if (Yii::app()->user->role == 'Worker') {
            $this->renderPartial('//layouts/_navItemsWorker');
        }
    } else {
        $this->renderPartial('//layouts/_navItemsGuest');
    }
    ?>
</ul>