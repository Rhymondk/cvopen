<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <?php
    //Core
    Yii::app()->getClientScript()->registerCoreScript('jquery');

    //CSS
    Yii::app()->clientScript->registerCssFile('//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext');
    Yii::app()->clientScript->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/public/style.min.css');
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/public/main.css');

    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/public/script.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/public/global.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/public/libs.js', CClientScript::POS_END);
    ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body
    onload="app.load()"
    data-controller="<?= ucfirst(strtolower(Yii::app()->controller->id)) ?>"
    data-action="<?= ucfirst(strtolower(Yii::app()->controller->action->id)) ?>">
<div class="wrapper">
    <div class="relative" id="<?= $this->getUniqueId(); ?>-<?= $this->action->id; ?>">

        <div class="navigation">

            <div class="container">
                <div class="col-md-12">
                    <div class="logo push-left">
                        <?= CHtml::link(CHtml::image(Image::open('images/Web/logo-b.png')->zoomCrop(205, 70)->png(), 'cvopen'), '/'); ?>
                    </div>
                    <div class="nav-items push-right">
                        <?php $this->renderPartial('//layouts/_navigationItems'); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php if ($this->breadcrumb || $this->timeleft) : ?>
            <div class="breadcrumb">
                <div class="container">
                    <div class="row">
                        <div class="col-md-<?= $this->timeleft ? '10' : '12'; ?>">
                            <?= $this->breadcrumb ?>
                        </div>
                        <?php if ($this->timeleft) : ?>
                        <div class="col-md-2 text-right">
                            <i class="clock-icon"></i>
                            <span class="time-left">
                                <span class="minutes"><?= $this->timeleft->format('%I') ?></span>:<span class="seconds"><?= $this->timeleft->format('%S') ?></span>
                            </span>
                        </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?= $content; ?>
    </div>
</div>
<div class="footer text-center">
    <div class="footer-links">
        <ul>
            <li><?= CHtml::link(Yii::t('web', 'Kontaktai'), Yii::app()->createUrl('cms/index', array('hook' => 'contacts'))); ?></li>
            <li><?= CHtml::link(Yii::t('web', 'D.U.K.'), Yii::app()->createUrl('faq/index')); ?></li>
        </ul>
    </div>
    <div class="copyrights">
        © <?= date('Y'); ?> <?= CHtml::link(Yii::t('web', 'CV OPEN'), '/'); ?>
    </div>
</div>
</body>
</html>
