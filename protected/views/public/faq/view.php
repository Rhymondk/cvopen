<?php $this->breadcrumb = Yii::t('web', 'Dažniausiai užduodami klausimai'); ?>
<div class="container">
    <div class="row faq">
        <div class="col-md-12">
            <?php foreach ($model as $faq): ?>
                <div class="faq-block">
                    <div class="row">
                        <div class="title col-md-11"><?= $faq->name; ?></div>
                        <div class="col-md-1 text-right"><i class="more"></i></div>
                        <div class="col-md-11">
                            <div class="description">
                                <?= $faq->description ?>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>