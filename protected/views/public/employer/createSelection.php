<?php if ($selection->isNewRecord): ?>
    <?php $this->breadcrumb = Yii::t('web', 'Sukurkite atranką keliais paprastais žingsniais'); ?>
<?php else: ?>
    <?php $this->breadcrumb = Yii::t('web', 'Redaguoti atranką'); ?>
<?php endif; ?>

<div class="container">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'selection-form'
    ));
    ?>
    <div class="row">
        <div class="col-md-6">
            <div class="legend blue-color"><?= Yii::t('web', '1. Pagrindinė informacija'); ?></div>
            <?=
            $form->textField($selection, 'position', array(
                'placeholder' => $selection->getAttributeLabel('position')
            ));
            ?>
            <?= $form->error($selection, 'position'); ?>

            <?=
            $form->dropDownList($selection, 'city_id', CHtml::listData($cities, 'id', 'name'), array(
                'placeholder' => $selection->getAttributeLabel('city_id')
            ));
            ?>
            <?= $form->error($selection, 'city_id'); ?>

            <?=
            $form->textField($selection, 'salary', array(
                'placeholder' => $selection->getAttributeLabel('salary')
            ));
            ?>
            <?= $form->error($selection, 'salary'); ?>

            <div class="legend blue-color"><?= Yii::t('web', '3. Darbo pobūdis'); ?></div>
            <?=
            $form->textarea($selection, 'description', array(
                'placeholder' => $selection->getAttributeLabel('description'),
                'rows' => 6,
            ));
            ?>
            <?= $form->error($selection, 'description'); ?>
        </div>
        <div class="col-md-6">
            <div class="legend blue-color"><?= Yii::t('web', '2. Reikalavimai'); ?></div>
            <div class="competence-fields">
                <?php foreach ($competences as $i => $compentece): ?>
                    <div class="competence-field-group relative">
                        <?=
                        $form->textfield($compentece, '[' . $i . ']name', array(
                            'placeholder' => $compentece->getAttributeLabel('name'),
                            'data-index' => $i
                        ));
                        ?>
                        <?= $form->error($compentece, '[' . $i . ']name'); ?>
                        <span></span>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="add-field"><span></span><?= Yii::t('web', 'Pridėti laukelį'); ?></div>
            <div class="legend blue-color"><?= Yii::t('web', '4. Ką mes siūlome'); ?></div>
            <?=
            $form->textarea($selection, 'offer', array(
                'placeholder' => $selection->getAttributeLabel('offer'),
                'rows' => 6,
            ));
            ?>
            <?= $form->error($selection, 'offer'); ?>
        </div>
    </div>
    <div class="row">
        <div class="choose-date col-md-12">
            <div class="legend blue-color"><?= Yii::t('web', '5. Laikotarpis'); ?></div>
            <div class="row">
                <div class="field-group col-md-6 relative">
                    <div class="pickadate-container relative">
                        <?= $form->textField($selection, 'date_start', array(
                            'class' => 'pickadate',
                            'placeholder' => $selection->getAttributeLabel('date_start')
                        )); ?>
                        <?= $form->error($selection, 'date_start'); ?>
                    </div>
                </div>
                <div class="field-group col-md-6">
                    <div class="pickadate-container relative">
                        <?= $form->textField($selection, 'date_end', array(
                            'class' => 'pickadate',
                            'placeholder' => $selection->getAttributeLabel('date_end')
                        )); ?>
                        <?= $form->error($selection, 'date_end'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="personal-info col-md-12">
            <div class="legend blue-color"><?= Yii::t('web', '6. Kontaktinė informacija') ?></div>
            <div class="row">
                <div class="col-md-6">
                    <?=
                    $form->textField($employer, 'contact_person', array(
                        'placeholder' => $employer->getAttributeLabel('contact_person')
                    ));
                    ?>
                    <?= $form->error($employer, 'contact_person'); ?>
                </div>
                <div class="col-md-6">
                    <?=
                    $form->textField($employer, 'number', array(
                        'placeholder' => $employer->getAttributeLabel('number')
                    ));
                    ?>
                    <?= $form->error($employer, 'number'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?= $form->checkbox($selection, 'notification', array(
                'placeholder' => $selection->getAttributeLabel('notification'),
                'class' => 'checkbox-small',
                'value' => 1,
                'id' => 'notification'
            ));
            ?>
            <?= $form->error($selection, 'notification'); ?>
            &nbsp;
        </div>
        <div class="col-md-6">
            <?=
            CHtml::submitButton(Yii::t('web', 'Peržiūrėti'), array(
                'class' => 'btn btn-blue col-md-6 push-right'
            ));
            ?>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>
