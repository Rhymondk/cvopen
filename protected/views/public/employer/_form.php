<div class="row">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'selection-form'
    ));
    ?>
    <div class="col-md-6">
        <?=
        $form->textField($employer, 'contact_person', array(
            'placeholder' => $employer->getAttributeLabel('contact_person')
        ));
        ?>
        <?= $form->error($employer, 'contact_person'); ?>
    </div>
    <div class="col-md-6">
        <?=
        $form->textField($employer, 'number', array(
            'placeholder' => $employer->getAttributeLabel('number')
        ));
        ?>
        <?= $form->error($employer, 'number'); ?>
    </div>
    <?php if (isset($onlyform) && !$onlyform): ?>
        <div class="col-md-12">
            <?=
            CHtml::submitButton(Yii::t('web', 'Išsaugoti'), array(
                'class' => 'btn btn-blue'
            ))
            ?>
        </div>
    <?php endif; ?>
    <?php
    $this->endWidget();
    ?>
</div>