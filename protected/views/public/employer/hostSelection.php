<?php $this->breadcrumb = Yii::t('web', 'Sukurkite atranką keliais paprastais žingsniais'); ?>
<?php $this->renderPartial('_steps', array('active' => 2)); ?>
<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'host-selection-form'
));
?>
    <div class="container">
        <div class="row">
            <div class="selection-position col-md-12"><?= $selection->position; ?></div>
        </div>
        <!--
        <div class="legend"><?= Yii::t('web', 'Pasirinkite kur talpinsite darbo skelbimą') ?></div>
        <div id="cvopen-host">
            <div class="row"
                <?= $selection->host_type == 1 ? 'style="display:none"' : '' ?>>
                <div class="choose-hosting-plan col-md-12">
                    <?php $selectionHostingPlans = CHtml::listData($selection->hostings, 'id', 'selection_hosting_plan_id'); ?>
                    <?php if (!empty($hostingPlans)): ?>
                        <?php foreach ($hostingPlans as $hostingPlan): ?>
                            <div class="col-md-3">
                                <?=
                                CHtml::checkBox('HostPlan[]', in_array($hostingPlan->id, $selectionHostingPlans), array(
                                    'placeholder' => CHtml::image(Image::open($hostingPlan->image)->resize(120, 100)->png(), $hostingPlan->name),
                                    'value' => $hostingPlan->id,
                                    'class' => 'checkbox-small',
                                    'id' => 'HostingPlan_' . $hostingPlan->id,
                                ));
                                ?>
                            </div>
                        <?php endforeach; ?>
                    <?php endIf; ?>
                </div>
            </div>
        </div>
        -->
        <div class="row">
            <div class="choose-date col-md-12">
                <div class="field-group col-md-offset-2 col-md-4 relative">
                    <div class="pickadate-container relative">
                        <?= $form->label($selection, 'date_start'); ?>
                        <?= $form->textField($selection, 'date_start', array('class' => 'pickadate')); ?>
                        <?= $form->error($selection, 'date_start'); ?>
                    </div>
                </div>
                <div class="field-group col-md-4">
                    <div class="pickadate-container relative">
                        <?= $form->label($selection, 'date_end'); ?>
                        <?= $form->textField($selection, 'date_end', array('class' => 'pickadate')); ?>
                        <?= $form->error($selection, 'date_end'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gradient-container">
        <div class="container">
<!--            <div id="price-table" class="row"></div>-->
            <div class="actions row">
                <div class="col-md-6 no-padding">
                    <?=
                    CHtml::link(Yii::t('web', 'Grįžti'), Yii::app()->createUrl('employer/selection', array(
                        'id' => $selection->id
                    )), array(
                        'class' => 'btn btn-big btn-white'
                    ))
                    ?>
                </div>
                <div class="col-md-6 text-right">
                    <?=
                    CHtml::submitButton(Yii::t('web', 'Tęsti'), array(
                        'class' => 'btn btn-big btn-blue'
                    ))
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php $this->endWidget(); ?>