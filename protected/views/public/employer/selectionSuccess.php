<?php $this->breadcrumb = Yii::t('web', 'Atranka sukurta'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="legend"><?= Yii::t('web', 'Gerbiamas kliente,') ?></div>
            <div class="description">
                <?= Yii::t('web', 'Dėkojame kad užsakėte CV OPEN teikiamas personalo atrankos paslaugas.') ?>
            </div>
            <div class="back">
                <?=
                CHtml::link(
                    Yii::t('web', 'Grįžti į atrankų sąrašą'),
                    Yii::app()->createUrl('employer/selections'),
                    array(
                        'class' => 'btn btn-blue'
                    )
                );
                ?>
            </div>
        </div>
    </div>
</div>
