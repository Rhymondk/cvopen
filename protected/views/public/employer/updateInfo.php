<?php $this->breadcrumb = $user->name ?>
<div class="container">
    <?php if ($success): ?>
    <div class="green-color legend">
        <?= Yii::t('web', 'Paskyra atnaujinta sėkmingai'); ?>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-md-12 legend blue-color"><?= Yii::t('web', 'Keisti slaptažodį'); ?></div>
        <div class="col-md-12">
            <?= $this->renderPartial('//user/changePassword', array(
                'user' => $user,
            ));
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 legend blue-color"><?= Yii::t('web', 'Pagrindinė informacija'); ?></div>
        <div class="col-md-12">
            <?= $this->renderPartial('_form', array(
                'employer' => $employer,
                'onlyform' => false
            ));
            ?>
        </div>
    </div>
</div>