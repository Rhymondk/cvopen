<?php
    $steps = array(
        1 => Yii::t('web', 'Nauja atranka'),
        2 => Yii::t('web', 'Talpinimas'),
        3 => Yii::t('web', 'Mokėjimas')
    );
    $stepsCount = count($steps);
?>
<div id="steps">
    <div class="container">
        <ul>
            <?php foreach ($steps as $key => $step) : ?>
                <li>
                    <div class="step-number <?= $active >= $key ? 'active' : '' ?>"><?= $key ?></div>
                    <div class="step-description"><?= $step ?></div>
                </li>
                <?php if ($step != end($steps)): ?>
                    <li class="seperator <?= $active >= ($key + 1) ? 'active' : '' ?>"></li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </div>
</div>