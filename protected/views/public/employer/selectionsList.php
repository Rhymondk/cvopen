<?php $this->breadcrumb = Yii::t('web', 'Kliento profilis'); ?>
<div class="container">
    <div class="row">
        <div class="legend blue-color"><?= Yii::t('web', 'Jūsų atrankos'); ?></div>
        <table class="col-md-12">
            <thead>
            <tr>
                <td><?= Yii::t('web', 'Pozicija') ?></td>
                <td><?= Yii::t('web', 'Dalyvių skaičius') ?></td>
                <td><?= Yii::t('web', 'Atrankos data') ?></td>
                <td><?= Yii::t('web', 'Statusas') ?></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <?php if (isset($selections)): ?>
                <?php foreach ($selections as $selection): ?>
                    <tr>
                        <td><?= $selection->position ?></td>
                        <td><?= $selection->countValidCandidates ?></td>
                        <td class="nowrap"><?= $selection->date_start ?> - <?= $selection->date_end ?></td>
                        <td style="color: <?= $selection->status->text_color ?>" class="nowrap">
                            <?php if (!empty($selection->status->icon)): ?>
                                <i class="status-icon <?= $selection->status->icon ?>"></i>
                            <?php endif; ?>
                            <?= $selection->status->name; ?>
                        </td>
                        <td class="actions">
                            <a class="hint--top tr-link" data-hint="<?= Yii::t('web', 'Kandidatai') ?>" href="<?= Yii::app()->createUrl('selection/view', array('id' => $selection->id)) ?>">
                                <img src="/images/Web/candidates.png" alt="<?= Yii::t('web', 'Kandidatai') ?>" />
                            </a>
                            <a  class="hint--top" data-hint="<?= Yii::t('web', 'Redaguoti atranką') ?>" href="<?= Yii::app()->createUrl('employer/selection', array('id' => $selection->id)) ?>">
                                <img src="/images/Web/update-selection.png" alt="<?= Yii::t('web', 'Redaguoti atranką') ?>" />
                            </a>
                            <?php if ($selection->isValidStatus()): ?>
                            <a href="<?= Yii::app()->createUrl('selection/share', array('id' => $selection->id)) ?>" class="hint--top" data-hint="<?= Yii::t('web', 'Pakviesti kandidatus') ?>">
                                <img src="/images/Web/selection-email.png" alt="<?= Yii::t('web', 'Pakviesti kandidatus') ?>" />
                            </a>
                            <?php endif; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
    </div>
    <div class="row create-selection">
        <div class="col-md-12 text-center">
            <?=
            CHtml::link(Yii::t('web', 'Kurti naują atranką'), Yii::app()->createUrl('employer/selection'), array(
                'class' => 'btn btn-blue'
            ));
            ?>
        </div>
    </div>
</div>