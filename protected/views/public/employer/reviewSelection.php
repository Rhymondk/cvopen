<?php $this->breadcrumb = Yii::t('web', 'Sukurkite atranką keliais paprastais žingsniais'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-offset-1 col-md-5">
            <div class="legend blue-color"><?= Yii::t('web', 'Pagrindinė informacija') ?></div>
            <table class="selection-basic-info col-md-12">
                <?php if (!empty($selection->position)): ?>
                    <tr>
                        <td><?= Yii::t('web', 'Pareigos:') ?></td>
                        <td><?= $selection->position; ?></td>
                    </tr>
                <?php endif; ?>
                <?php if (!empty($selection->city->name)): ?>
                    <tr>
                        <td><?= Yii::t('web', 'Miestas:') ?></td>
                        <td><?= $selection->city->name; ?></td>
                    </tr>
                <?php endif; ?>
                <?php if (!empty($selection->salary)): ?>
                    <tr>
                        <td><?= Yii::t('web', 'Atlyginimas:') ?></td>
                        <td><?= $selection->salary; ?></td>
                    </tr>
                <?php endif; ?>
            </table>
        </div>
        <div class="col-md-6 selection-competence">
            <div class="legend blue-color"><?= Yii::t('web', 'Reikalavimai') ?></div>
            <?php if (!empty($selection->competences)): ?>
                <?php foreach ($selection->competences as $competence): ?>
                    <p class="col-md-12"><?= $competence->name ?></p>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="legend blue-color"><?= Yii::t('web', 'Darbo pobūdis') ?></div>
            <div class="description">
                <?= nl2br($selection->description) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="legend blue-color"><?= Yii::t('web', 'Ką mes siūlome') ?></div>
            <div class="offer">
                <?= nl2br($selection->offer) ?>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 selection-hosting">
            <!--
            <div class="col-md-10 col-md-offset-1">
                <span class="legend blue-color"><?= Yii::t('web', 'Talpinimas') ?></span>
                <?php if ($selection->host_type == 1): ?>
                    <span><?= Yii::t('web', 'Talpinsiu savarankiškai') ?></span>
                <?php else: ?>
                    <span>
                        <?= CHtml::image(Image::open('images/Web/logo-b.png')->resize(120, 100)->png()); ?>
                    </span>
                    <?php if (!empty($selection->hostings)): ?>
                        <?php foreach ($selection->hostings as $hosting): ?>
                            <span>
                                <?= CHtml::image(Image::open($hosting->selectionHostingPlan->image)->resize(120, 100)->png()); ?>
                            </span>
                        <?php endforeach; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
                -->
        </div>
    </div>
    <div class="row">
        <div class="col-md-offset-2 col-md-4">
            <div class="label"><?= Yii::t('web', 'Pradžios data') ?></div>
            <div class="fake-input noselect"><?= $selection->date_start; ?></div>
        </div>
        <div class="col-md-4">
            <div class="label"><?= Yii::t('web', 'Pabaigos data') ?></div>
            <div class="fake-input noselect"><?= $selection->date_end; ?></div>
        </div>
    </div>
</div>
<div class="gradient-container">
    <div class="container">
        <div class="actions row">
            <div class="col-md-6 no-padding">
                <?=
                CHtml::link(Yii::t('web', 'Grįžti'), Yii::app()->createUrl('employer/hostSelection', array(
                    'id' => $selection->id
                )), array(
                    'class' => 'btn btn-big btn-white'
                ))
                ?>
            </div>
            <div class="col-md-6 text-right">
                <?=
                CHtml::link(Yii::t('web', 'Patvirtinti'), Yii::app()->createUrl('employer/selectionSuccess', array(
                    'id' => $selection->id
                )), array(
                    'class' => 'btn btn-big btn-green'
                ))
                ?>
            </div>
        </div>
    </div>
</div>