<?php if (!empty($priceTable)): ?>
    <table class="col-md-12">
        <?php foreach ($priceTable as $hostPlan): ?>
            <tr>
                <td><?= $hostPlan['name'] ?></td>
                <td><?= $hostPlan['total'] ?></td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td class="text-right"><?= Yii::t('web', 'PVM'); ?></td>
            <td><?= $total['vac'] ?></td>
        </tr>
        <tr>
            <td class="text-right"><?= Yii::t('web', 'VISO'); ?></td>
            <td><?= $total['total'] ?></td>
        </tr>
    </table>
<?php else: ?>
    <div class="col-md-12 text-center choose-params">
        <?= Yii::t('web', 'Pasirinkite datą'); ?>
    </div>
<?php endif; ?>
