<?php $this->breadcrumb = Yii::t('web', 'Apmokėjimas atšauktas'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 text-center">
            <div class="red-block">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="title"><?= Yii::t('web', 'Apmokėjimas atšauktas') ?></div>
                        <div class="description">
                            <?= Yii::t('web', 'Apmokėjimas atšauktas ir neapmokėtas') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
