<?php $this->breadcrumb = Yii::t('web', 'Užsakymas apmokėtas'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="legend"><?= Yii::t('web', 'Užsakymas apmokėtas') ?></div>
            <div class="description">
                <?= Yii::t('web', 'Užsakymas apmokėtas. Testo rezultatus jau turėjote gauti el. paštu. Jei rezultatų negavote prašome susisiekti su administracija') ?>
            </div>
            <div class="back">
                <?=
                CHtml::link(
                    Yii::t('web', 'Peržiūrėti darbo pasiūlymus'),
                    Yii::app()->createUrl('worker/selections'),
                    array(
                        'class' => 'btn btn-blue'
                    )
                );
                ?>
            </div>
        </div>
    </div>
</div>
