<?php $this->breadcrumb = Yii::t('web', 'Mokėjimas išsiųstas'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="legend"><?= Yii::t('web', 'Laukiame patvirtinimo') ?></div>
            <div class="description">
                <?= Yii::t('web', 'Jūsų mokėjimas išsiųstas ir laukia patvirtinimo. Gavę mokėjimą, Jūsų registracijoje nurodytu el. paštu, išsiųsime testo rezultatus.') ?>
            </div>
        </div>
    </div>
</div>
