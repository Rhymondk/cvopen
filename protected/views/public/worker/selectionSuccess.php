<?php $this->breadcrumb = Yii::t('web', 'CV pateiktas'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="legend"><?= Yii::t('web', 'CV pateiktas') ?></div>
            <div class="description">
                <?= Yii::t('web', 'Jūsų CV nusiųstas darbdaviui. Jeigu atlikto testo rezultatai ir pateikta informacija atitiks kliento reikalavimus, susisieksime su Jumis dviejų savaičių laikotarpyje.') ?>
            </div>
            <div class="back">
                <?=
                CHtml::link(
                    Yii::t('web', 'Peržiūrėti kitus darbo pasiūlymus'),
                    Yii::app()->createUrl('worker/selections'),
                    array(
                        'class' => 'btn btn-blue'
                    )
                );
                ?>
            </div>
        </div>
    </div>
</div>
