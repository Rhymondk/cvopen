<?php $this->breadcrumb = Yii::t('web', 'Atrankos peržiūra'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="title blue-color legend"><?= $selection->employer->name ?></div>
            <div class="position"><?= $selection->position ?></div>
        </div>
        <div class="info-container col-md-10 col-md-offset-1">
            <div class="row">
                <div class="col-md-6">
                    <div class="info-block main-info">
                        <div class="legend blue-color"><?= Yii::t('web', 'Pagrindinė informacija') ?></div>
                        <div class="description">
                            <table>
                                <tr>
                                    <td><?= Yii::t('web', 'Miestas:') ?></td>
                                    <td><?= $selection->city->name ?></td>
                                </tr>
                                <tr>
                                    <td><?= Yii::t('web', 'Atlyginimas:') ?></td>
                                    <td><?= $selection->salary ?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="info-block">
                        <div class="legend blue-color"><?= Yii::t('web', 'Darbo pobūdis') ?></div>
                        <div class="description">
                            <?= nl2br($selection->description) ?>
                        </div>
                    </div>
                    <div class="info-block">
                        <div class="legend blue-color"><?= Yii::t('web', 'Ką mes siūlome') ?></div>
                        <div class="description">
                            <?= nl2br($selection->offer) ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="legend blue-color"><?= Yii::t('web', 'Reikalavimai') ?></div>
                    <div class="description">
                        <?php foreach ($selection->competences as $competence): ?>
                            - <?= $competence->name; ?><br />
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center">
                    <div class="to-selection">
                        <?php if (Yii::app()->user->isGuest): ?>
                            <?=
                            CHtml::link(
                                Yii::t('web', 'Noriu kandidatuoti'),
                                Yii::app()->createUrl('site/register', array('id' => $selection->id)),
                                array(
                                    'class' => 'btn btn-blue'
                                )
                            )
                            ?>
                        <?php else: ?>
                            <?=
                            CHtml::link(
                                Yii::t('web', 'Noriu kandidatuoti'),
                                Yii::app()->createUrl('worker/selection', array('id' => $selection->id)),
                                array(
                                    'class' => 'btn btn-blue'
                                )
                            )
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>