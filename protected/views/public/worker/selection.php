<?php $this->breadcrumb = $selection->position . ' | ' . $selection->employer->name ?>
<div class="container">
    <div class="row info-block">
        <div class="col-md-12">
            <?= Yii::t('web', 'Užpildykite žemiau esančiuose laukeliuose reikalingą informaciją ir išspręskite testą.') ?>
            <br/>
            <?= Yii::t('web', 'Tik teisingai užpildę reikiamus laukelius ir išsprendę testą galėsite kandidatuoti į pasirinktą pareigybę') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="legend blue-color">
                <?= Yii::t('web', '1. Užpildykite žemiau esančią formą') ?>
            </div>
        </div>
    </div>
    <div class="row worker-info">
        <div class="col-md-2 text-center">
            <?php
            $fileForm = $this->beginWidget('CActiveForm', array(
                'id' => 'worker-file-form',
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data'
                ),
            ));
            ?>
            <div class="image">
                <div class="img-preview img-circle"
                     style="background-image: url(<?= $worker->user->image ?>)">
                </div>
            </div>
            <div class="upload-image button">
                <?=
                CHtml::button(Yii::t('web', 'Prisegti nuotrauką'), array(
                        'type' => 'button',
                        'class' => 'btn btn-blue'
                    )
                );
                ?>
                <?=
                $fileForm->filefield($user, 'imageFile', array(
                    'class' => 'hidden'
                ));
                ?>
                <?= $fileForm->error($user, 'imageFile'); ?>
            </div>
            <div class="upload-selection button">
                <?php if (!empty($worker->cv)): ?>
                    <?=
                    CHtml::button(Yii::t('web', 'CV prisegtas'), array(
                            'type' => 'button',
                            'class' => 'btn btn-green'
                        )
                    );
                    ?>
                <?php else: ?>
                    <?=
                    CHtml::button(Yii::t('web', 'Prisegti CV'), array(
                            'type' => 'button',
                            'class' => 'btn btn-darkblue'
                        )
                    );
                    ?>
                <?php endif; ?>
                <?=
                $fileForm->filefield($worker, 'cvFile', array(
                    'class' => 'hidden'
                ));
                ?>
                <?= $fileForm->error($worker, 'cvFile'); ?>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'worker-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'validateOnType' => false,
                'afterValidate' => 'js:function(form, data, hasError) {
                    if (!hasError) {
                        $("#submit-form").submit();
                    } else {
                        $("html, body").animate({
                            scrollTop: $("#worker-form").offset().top - 200
                        }, 500);
                    }
                }'
            )
        ));
        ?>
        <div class="col-md-5">
            <?= $form->textField($worker, 'firstname', array(
                'placeholder' => $worker->getAttributeLabel('firstname')
            )); ?>
            <?= $form->error($worker, 'firstname'); ?>

            <?= $form->textField($worker, 'lastname', array(
                'placeholder' => $worker->getAttributeLabel('lastname')
            )); ?>
            <?= $form->error($worker, 'lastname'); ?>

            <div class="inline-elements">
                <label><?= Yii::t('web', 'Lytis') ?></label>
                <?php foreach ($genders as $count => $gender): ?>
                    <?=
                    $form->radioButton($worker, 'gender_id', array(
                        'placeholder' => $gender->name,
                        'value' => $gender->id,
                        'uncheckValue' => null,
                        'id' => 'gender_' . $gender->id
                    ));
                    ?>
                <?php endforeach; ?>
                <?= $form->error($worker, 'gender_id'); ?>
            </div>
            <div class="pickadate-container relative">
                <?=
                $form->textField($worker, 'birthday', array(
                    'class' => 'pickadate',
                    'placeholder' => $worker->getAttributeLabel('birthday')
                ));
                ?>
                <?= $form->error($worker, 'birthday'); ?>
            </div>
            <?=
            $form->textField($worker, 'number', array(
                'placeholder' => $worker->getAttributeLabel('number')
            ));
            ?>
            <?= $form->error($worker, 'number'); ?>
            <?=
            $form->textField($worker, 'address', array(
                'placeholder' => $worker->getAttributeLabel('address')
            ));
            ?>
            <?= $form->error($worker, 'address'); ?>
        </div>
        <div class="col-md-5">
            <?=
            $form->dropDownList($worker, 'education_id', CHtml::listData($educations, 'id', 'name'), array(
                'data-withoutsearch' => false,
                'data-prompt' => $worker->getAttributeLabel('education_id'),
                'prompt' => $worker->getAttributeLabel('education_id')
            ));
            ?>
            <?= $form->error($worker, 'education_id'); ?>

            <?=
            $form->textField($worker, 'salary', array(
                'placeholder' => $worker->getAttributeLabel('salary')
            ));
            ?>
            <?= $form->error($worker, 'salary'); ?>


            <?=
            $form->textarea($worker, 'comment', array(
                'placeholder' => $worker->getAttributeLabel('comment')
            ));
            ?>
            <?= $form->error($worker, 'comment'); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
</div>
<div class="gradient-container">
    <div class="worker-history container">
        <div class="row">
            <div class="col-md-12">
                <div class="legend white-color"><?= Yii::t('web', '2. Darbo patirtis') ?></div>
            </div>
            <?php if (!empty($worker->workerHistories)): ?>
            <div class="col-md-12">
                <table class="table">
                    <thead>
                        <tr>
                            <td><?= Yii::t('web', 'Įmonės pavadinimas') ?></td>
                            <td><?= Yii::t('web', 'Pareigos') ?></td>
                            <td><?= Yii::t('web', 'Dirbta nuo') ?></td>
                            <td><?= Yii::t('web', 'Dirbta iki') ?></td>
                            <td></td>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($worker->workerHistories as $history): ?>
                        <tr>
                            <td><?= $history->company; ?></td>
                            <td><?= $history->position; ?></td>
                            <td><?= $history->date_start; ?></td>
                            <td><?= $history->working_now ? Yii::t('web', 'Dabar') : $history->date_end ?></td>
                            <td>
                                <?=
                                CHtml::link(CHtml::image('/images/Web/update-selection-white.png', Yii::t('web', 'Redaguoti')),
                                    Yii::app()->createUrl('worker/selection', array(
                                        'id' => $selection->id,
                                        'history' => $history->id
                                    )) . '#history-form')
                                ?>
                                <?=
                                CHtml::link(CHtml::image('/images/Web/update-selection-remove.png', Yii::t('web', 'Ištrinti')),
                                    Yii::app()->createUrl('worker/removeHistory', array(
                                        'id' => $history->id,
                                        'selection_id' => $selection->id
                                    )))
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
            <?php else: ?>
            <div class="col-md-12">
                <div class="text-center history-empty">
                    <?= Yii::t('web', 'Nėra įvesta darbo patirties') ?>
                </div>
            </div>
            <?php endif; ?>
        </div>
        <div class="remodal history-form" data-remodal-id="history-form">
            <?php
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'worker-history-form',
                'action' => Yii::app()->createUrl('worker/addHistory',
                    $workerHistory->isNewRecord ? array() : array(
                        'id' => $workerHistory->id
                    )
                ),
                'enableAjaxValidation' => true,
                //'enableClientValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                    'validateOnType' => false,
                    'afterValidate' => 'js:function(form, data, hasError) {
                        if (!hasError) {
                            window.location.href = "' . Yii::app()->createUrl('worker/selection', array('id' => $selection->id)) . '";
                        }
                    }'
                )
            ));
            ?>
            <div class="row">
                <div class="col-md-12">
                    <?php if ($workerHistory->isNewRecord): ?>
                        <div class="legend blue-color"><?= Yii::t('web', 'Pridėti darbovietę') ?></div>
                    <?php else: ?>
                        <div class="legend blue-color"><?= Yii::t('web', 'Redaguoti darbovietę') ?></div>
                    <?php endif; ?>

                    <?=
                    $form->textField($workerHistory, 'company', array(
                        'placeholder' => $workerHistory->getAttributeLabel('company')
                    ));
                    ?>
                    <?= $form->error($workerHistory, 'company'); ?>

                    <?=
                    $form->textField($workerHistory, 'position', array(
                        'placeholder' => $workerHistory->getAttributeLabel('position')
                    ));
                    ?>
                    <?= $form->error($workerHistory, 'position'); ?>

                    <div class="pickadate-container relative">
                    <?=
                        $form->textField($workerHistory, 'date_start', array(
                            'placeholder' => $workerHistory->getAttributeLabel('date_start'),
                            'class' => 'pickadate'
                        ));
                        ?>
                        <?= $form->error($workerHistory, 'date_start'); ?>
                    </div>

                    <div class="pickadate-container relative">
                        <?=
                        $form->textField($workerHistory, 'date_end', array(
                            'placeholder' => $workerHistory->getAttributeLabel('date_end'),
                            'class' => 'pickadate',
                            'style' => !$workerHistory->working_now ?: 'display:none'
                        ));
                        ?>
                        <?= $form->error($workerHistory, 'date_end'); ?>
                    </div>

                    <?=
                    $form->checkBox($workerHistory, 'working_now', array(
                        'placeholder' => $workerHistory->getAttributeLabel('working_now')
                    ));
                    ?>

                    <?= $form->error($workerHistory, 'working_now'); ?>
                </div>
            </div>
            <div class="row actions">
                <div class="col-md-6">
                    <div class="text-right">
                        <?=
                        CHtml::link(Yii::t('web', 'Uždaryti'), '#', array(
                            'class' => 'remodal-cancel'
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="text-left">
                        <?=
                        CHtml::submitButton(Yii::t('web', 'Patvirtinti'), array(
                            'class' => 'btn btn-blue'
                        ));
                        ?>
                    </div>
                </div>
            </div>
            <?php $this->endWidget(); ?>
        </div>
        <div class="col-md-12 text-center add-history">
        <?= CHtml::link(Yii::t('web', 'Pridėti darbovietę'), '#history-form', array(
            'class' => 'btn btn-blue'
        )) ?>
        </div>
    </div>
</div>
<div class="container form-end">
    <div class="row">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'submit-form',
            'action' => Yii::app()->createUrl('worker/submitSelection', array(
                    'id' => $selection->id
                )
            )
        ));
        ?>
        <div class="col-md-6 choose-competence">
            <div class="row border">
                <div class="col-md-offset-4 col-md-7 col-md-pull-1">
                    <div class="legend blue-color text-right"><?= Yii::t('web', '3. Pažymėk reikalavimus, kuriuos atitinki') ?></div>
                </div>
                <div class="col-md-11 col-md-pull-1 competences">
                    <?php foreach ($selection->competences as $competence): ?>
                        <span class="competence <?= in_array($competence->id, $activeCompetences) ? 'active' : '' ?>"><?= $competence->name; ?></span>
                        <?=
                        CHtml::checkBox('competence[]', in_array($competence->id, $activeCompetences), array(
                            'value' => $competence->id,
                            'id' => 'competence_' . $competence->id,
                            'class' => 'default hidden'
                        )); ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <?php if ($worker->getComplete($selection->id)) : ?>
        <div class="col-md-6 text-left solve-test">
            <div class="row">
                <div class="col-md-offset-1 col-md-11">
                    <div class="legend blue-color"><?= Yii::t('web', '4. Pateikti CV') ?></div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <?= CHtml::button(Yii::t('web', 'Pateikti CV peržiūrai'), array(
                        'class' => 'btn btn-blue btn-big btn-fluid',
                        'id' => 'submit-worker-form',
                    )) ?>
                </div>
            </div>
        </div>
        <?php else: ?>
        <div class="col-md-6 text-left solve-test">
            <div class="row">
                <div class="col-md-offset-1 col-md-11">
                    <div class="legend blue-color"><?= Yii::t('web', '4. Spręsk testą') ?></div>
                </div>
                <div class="col-md-offset-1 col-md-11">
                    <div class="description">
                        <?= Yii::t('web', 'Dėmesio, testo, sprendimas užtruks apie 20 minučių, viso bus pateikti 59 klausimai. Kiekvienas kandidatas aplikuojantis į šią poziciją privalo išspręsti testą.'); ?>
                    </div>
                </div>
                <div class="col-md-offset-1 col-md-10">
                    <?= CHtml::button(Yii::t('web', 'Spręsti testą'), array(
                        'class' => 'btn btn-blue btn-big btn-fluid',
                        'id' => 'submit-worker-form',
                    )) ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
    </div>
    <?php $this->endWidget(); ?>
</div>