<div class="row worker-info">
    <div class="col-md-2 text-center">
        <?php
        $fileForm = $this->beginWidget('CActiveForm', array(
            'id' => 'worker-file-form',
            'htmlOptions' => array(
                'enctype' => 'multipart/form-data'
            ),
        ));
        ?>
        <div class="image">
            <div class="img-preview img-circle"
                 style="background-image: url(<?= $worker->user->image ?>)">
            </div>
        </div>
        <div class="upload-image button">
            <?=
            CHtml::button(Yii::t('web', 'Prisegti nuotrauką'), array(
                    'type' => 'button',
                    'class' => 'btn btn-blue'
                )
            );
            ?>
            <?=
            $fileForm->filefield($user, 'imageFile', array(
                'class' => 'hidden'
            ));
            ?>
            <?= $fileForm->error($user, 'imageFile'); ?>
        </div>
        <div class="upload-selection button">
            <?php if (!empty($worker->cv)): ?>
                <?=
                CHtml::button(Yii::t('web', 'CV prisegtas'), array(
                        'type' => 'button',
                        'class' => 'btn btn-green'
                    )
                );
                ?>
            <?php else: ?>
                <?=
                CHtml::button(Yii::t('web', 'Prisegti CV'), array(
                        'type' => 'button',
                        'class' => 'btn btn-darkblue'
                    )
                );
                ?>
            <?php endif; ?>
            <?=
            $fileForm->filefield($worker, 'cvFile', array(
                'class' => 'hidden'
            ));
            ?>
            <?= $fileForm->error($worker, 'cvFile'); ?>
        </div>
        <?php $this->endWidget(); ?>
    </div>
    <div class="col-md-5">
        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'worker-form'
        ));
        ?>
        <?= $form->textField($worker, 'firstname', array(
            'placeholder' => $worker->getAttributeLabel('firstname')
        )); ?>
        <?= $form->error($worker, 'firstname'); ?>

        <?= $form->textField($worker, 'lastname', array(
            'placeholder' => $worker->getAttributeLabel('lastname')
        )); ?>
        <?= $form->error($worker, 'lastname'); ?>

        <div class="inline-elements">
            <label><?= Yii::t('web', 'Lytis') ?></label>
            <?php foreach ($genders as $count => $gender): ?>
                <?=
                $form->radioButton($worker, 'gender_id', array(
                    'placeholder' => $gender->name,
                    'value' => $gender->id,
                    'uncheckValue' => null,
                    'id' => 'gender_' . $gender->id
                ));
                ?>
            <?php endforeach; ?>
            <?= $form->error($worker, 'gender_id'); ?>
        </div>
        <div class="pickadate-container relative">
            <?=
            $form->textField($worker, 'birthday', array(
                'class' => 'pickadate',
                'placeholder' => $worker->getAttributeLabel('birthday')
            ));
            ?>
            <?= $form->error($worker, 'birthday'); ?>
        </div>
        <?=
        $form->textField($worker, 'number', array(
            'placeholder' => $worker->getAttributeLabel('number')
        ));
        ?>
        <?= $form->error($worker, 'number'); ?>
        <?=
        $form->textField($worker, 'address', array(
            'placeholder' => $worker->getAttributeLabel('address')
        ));
        ?>
        <?= $form->error($worker, 'address'); ?>
    </div>
    <div class="col-md-5">
        <?=
        $form->dropDownList($worker, 'education_id', CHtml::listData($educations, 'id', 'name'), array(
            'data-withoutsearch' => false,
            'data-prompt' => $worker->getAttributeLabel('education_id'),
            'prompt' => $worker->getAttributeLabel('education_id')
        ));
        ?>
        <?= $form->error($worker, 'education_id'); ?>

        <?=
        $form->textField($worker, 'salary', array(
            'placeholder' => $worker->getAttributeLabel('salary')
        ));
        ?>
        <?= $form->error($worker, 'salary'); ?>


        <?=
        $form->textarea($worker, 'comment', array(
            'placeholder' => $worker->getAttributeLabel('comment')
        ));
        ?>
        <?= $form->error($worker, 'comment'); ?>
    </div>
</div>
<div class="row">
    <?php if (isset($onlyform) && !$onlyform): ?>
        <div class="col-md-10 col-md-offset-2">
            <?=
            CHtml::submitButton(Yii::t('web', 'Išsaugoti'), array(
                'class' => 'btn btn-blue'
            ))
            ?>
        </div>
    <?php endif; ?>
    <?php
    $this->endWidget();
    ?>
</div>