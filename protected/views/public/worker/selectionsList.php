<?php $this->breadcrumb = Yii::t('web', 'Pasirinkite dominančią atranką ir kandidatuokite'); ?>
<div class="container">
    <div class="row before-table">
        <div class="col-md-6">
            <?= Yii::t('web', 'Atrankų skačius: {n}', $count); ?>
        </div>
        <div class="col-md-offset-4 col-md-2">
            <?=
            CHtml::dropDownList('limit', Yii::app()->params['selections_in_page'], $inPageOptions, array(
                'data-withoutsearch' => false,
                'data-prompt' => Yii::t('web', 'Rodyti'),
                'prompt' => Yii::t('web', 'Rodyti')
            ));
            ?>
        </div>
    </div>
    <div class="row">
        <div class="selection-table col-md-12">
            <div class="selection-tr thead">
                <span><?= Yii::t('web', 'Paskelbta') ?></span>
                <span><?= Yii::t('web', 'Įmonė') ?></span>
                <span><?= Yii::t('web', 'Pareigos') ?></span>
                <span><?= Yii::t('web', 'Miestas') ?></span>
                <span><?= Yii::t('web', 'Atlyginimas (€)') ?></span>
                <span><?= Yii::t('web', 'Galioja iki') ?></span>
                <span></span>
            </div>
            <?php if (isset($selections)): ?>
                <?php foreach ($selections as $selection): ?>
                    <a href="<?= Yii::app()->createUrl('worker/view', array('id' => $selection->id)) ?>"
                       class="selection-tr">
                        <span class="nowrap"><?= $selection->date_start ?></span>
                        <span><?= $selection->employer->name; ?></span>
                        <span><?= $selection->position ?></span>
                        <span><?= $selection->city->name ?></span>
                        <span><?= $selection->salary ?></span>
                        <span class="nowrap"><?= $selection->date_end ?></span>
                        <span><?= !empty($selection->employer->image) ? CHtml::image(Image::open($selection->employer->image)->resize(70, 40)) : ''; ?></span>
                    </a>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row pagination">
        <div class="col-md-2">
            <?= Yii::t('web', 'Puslapis: {page}/{count}', array(
                '{page}' => $pages->getCurrentPage() + 1,
                '{count}' => $pages->getPageCount()
            )); ?>
        </div>
        <div class="col-md-10 text-right">
            <?php
            $this->widget('CLinkPager', array(
                'pages' => $pages,
                'header' => '',
                'nextPageLabel' => '',
                'prevPageLabel' => '',
                'lastPageLabel' => '',
                'firstPageLabel' => '',
                'cssFile' => '/css/public/style.min.css'
            ))
            ?>
        </div>
    </div>
</div>