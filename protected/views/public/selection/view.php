<?php $this->breadcrumb = $selection->position . ' / ' . Yii::t('web', 'kandidatai');  ?>
<?php Yii::app()->clientScript->registerScript("selection-id", 'selectionId = "' . $selection->id . '";'); ?>
<div class="container" data-id="<?= $selection->id ?>">
    <div class="row">
        <div class="col-md-12">
            <div class="back">
                <?=
                    CHtml::link(
                        Yii::t('web', '< Grįžti į atrankų sąrašą'),
                        Yii::app()->createUrl('employer/selections'),
                        array(
                            'class' => 'green-color'
                        )
                    );
                ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="blue-heading-block">
                <div class="col-md-6">
                    <div class="title"><?= Yii::t('web', 'Kandidatai') ?></div>
                </div>
                <div class="col-md-6">
                    <div class="description">
                        <span>
                            <?= Yii::t('web', 'Dalyvių skaičius: {n}', $selection->CountValidCandidates) ?>
                        </span>
                        <span>
                            <?= Yii::t('web', 'Laikotarpis: {n}', $selection->period) ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="filter">
                <div class="row">
                    <div class="col-md-8">
                        <?=
                        CHtml::dropDownList('filter', $filter, array(
                            0 => 'Visi', //Starred = 1, Hidden = 1
                            1 => 'Pažymėti', //Starred = 1, Hidden = 0
                            2 => 'Paslėpti' //Starred = 0, Hidden = 1
                        ), array(
                            'data-withoutsearch' => false,
                            'data-prompt' => Yii::t('web', 'Rodyti'),
                            'prompt' => Yii::t('web', 'Rodyti')
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="filter">
                <div class="row">
                    <div class="col-md-4">
                    <?=
                    CHtml::dropDownList('limit', $limit, $inPageOptions, array(
                        'data-withoutsearch' => false,
                        'data-prompt' => Yii::t('web', 'Rodyti'),
                        'prompt' => Yii::t('web', 'Rodyti')
                    ));
                    ?>
                    </div>
                    <div class="col-md-8">
                        <?=
                        CHtml::dropDownList('type_id', $type_id, $typesSelectList, array(
                            'data-withoutsearch' => false,
                            'data-prompt' => Yii::t('web', 'Rodyti'),
                            'prompt' => Yii::t('web', 'Rodyti')
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php if ($candidates): ?>
            <div class="candidate-list">
                <div class="row">
                    <?php foreach ($candidates as $candidate): ?>
                    <div class="col-md-6">
                        <div class="candidate pull-left">
                            <div class="image col-md-4">
                                <?= CHtml::image(
                                    Image::open(
                                        is_null($candidate->workerUser->user->image) || $selection->isDemo() ?
                                            'images/Web/no-image.png' :
                                            $candidate->workerUser->user->image
                                    )->zoomCrop(100, 100)->png(), '', array(
                                    'class' => 'img-circle',
                                    'height' => 100,
                                    'width' => 100
                                ));
                                ?>
                            </div>
                            <div class="main-info col-md-8">
                                <div class="fullname">
                                    <?= $selection->isDemo() ? Yii::t('web', $candidate->workerUser->gender_id == 1 ? 'Kandidatas' : 'Kandidatė') : $candidate->workerUser->fullname ?>
                                </div>
                                <div class="age"><?= $candidate->workerUser->age . ' ' . $this->numberString($candidate->workerUser->age, 'met', array('ų', 'ai', 'ai')) ?></div>
                                <div class="email">
                                    <?php if ($selection->isDemo()): ?>
                                        <?= CHtml::mailto('*******@gmail.com', '*******@gmail.com', array(
                                            'class' => 'green-color'
                                        )) ?>
                                    <?php else: ?>
                                        <?= CHtml::mailto($candidate->workerUser->user->email, $candidate->workerUser->user->email, array(
                                            'class' => 'green-color'
                                        )) ?>
                                    <?php endif; ?>
                                </div>
                                <div class="number">
                                    <?php if ($selection->isDemo()): ?>
                                        +3706******
                                    <?php else: ?>
                                        <?= $candidate->workerUser->number ?>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="full-width">
                                <div class="col-md-12 general-score">
                                    <div class="green-block">
                                        <div class="col-md-4">
                                            <?= Yii::t('web', 'Bendras kandidato įvertinimas:') ?>
                                        </div>
                                        <div class="col-md-8 text-center percentage">
                                            <?= $selection->getCandidateScore($candidate->workerUser) ?>%
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="types-score">
                                <?php foreach ($candidate->workerUser->typeResultsShow as $result): ?>
                                <div class="col-md-6">
                                    <div class="type">
                                        <div class="col-md-12 no-padding">
                                            <div class="name"><?= $result['name'] ?></div>
                                        </div>
                                        <div class="col-md-10 no-padding">
                                            <div class="progress">
                                                <span style="width:<?= $result['percent'] ?>%"></span>
                                            </div>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="progress-percent">
                                                <?= $result['percent'] ?>%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                <?php if ($selection->hasQuiz): ?>
                                    <?php foreach ($selection->selectionQuizes as $selectionQuiz): ?>
                                        <div class="col-md-6">
                                            <div class="type">
                                                <div class="col-md-12 no-padding">
                                                    <div class="name"><?= $selectionQuiz->quiz->name ?></div>
                                                </div>
                                                <div class="col-md-10 no-padding">
                                                    <div class="progress">
                                                        <span style="width:<?= $candidate->workerUser->getQuizScore($selectionQuiz->quiz) ?>%"></span>
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="progress-percent">
                                                        <?= $candidate->workerUser->getQuizScore($selectionQuiz->quiz) ?>%
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                            <div class="row">
                                <div class="actions col-md-12">
                                    <div class="col-md-9 attributes">
                                        <div class="starred-attr">
                                            <?=
                                            CHtml::checkBox('starred', $candidate->starred, array(
                                                'placeholder' => Yii::t('web', 'Pažymėti kandidatą'),
                                                'class' => 'checkbox-small',
                                                'value' => $candidate->id,
                                                'id' => 'starred_' . $candidate->id
                                            ));
                                            ?>
                                        </div>
                                        <div class="hidden-attr">
                                            <?=
                                            CHtml::checkBox('hidden', $candidate->hidden, array(
                                                'placeholder' => Yii::t('web', 'Paslėpti kandidatą'),
                                                'class' => 'checkbox-small',
                                                'value' => $candidate->id,
                                                'id' => 'hidden_' . $candidate->id
                                            ));
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-md-3 no-padding text-center">
                                        <?= CHtml::link(
                                            Yii::t('web', 'Daugiau +'),
                                            Yii::app()->createUrl('selection/candidate', array('id' => $candidate->id)),
                                            array(
                                                'class' => 'more green-color'
                                            )
                                            );
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="row pagination">
                <div class="col-md-2">
                    <?= Yii::t('web', 'Puslapis: {page}/{count}', array(
                        '{page}' => $pages->getCurrentPage() + 1,
                        '{count}' => $pages->getPageCount()
                    )); ?>
                </div>
                <div class="col-md-10 text-right">
                    <?php
                    $this->widget('CLinkPager', array(
                        'pages' => $pages,
                        'header' => '',
                        'nextPageLabel' => '',
                        'prevPageLabel' => '',
                        'lastPageLabel' => '',
                        'firstPageLabel' => '',
                        'cssFile' => '/css/public/style.min.css'
                    ))
                    ?>
                </div>
            </div>
            <?php else: ?>
                <div class="col-md-12 text-center empty">
                    <?= Yii::t('web', 'Nėra kandidatų'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>