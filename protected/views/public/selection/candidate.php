<?php $this->breadcrumb = $selection->isDemo() ? Yii::t('web', $worker->gender_id == 1 ? 'Kandidatas' : 'Kandidatė') : $worker->fullname; ?>
<div class="container">
    <div class="row back">
        <div class="col-md-12">
            <?=
            CHtml::link(
                Yii::t('web', '< Grįžti į kandidatų sąrašą'),
                Yii::app()->createUrl('selection/view', array('id' => $selection->id)),
                array(
                    'class' => 'green-color'
                )
            );
            ?>
        </div>
    </div>
    <div class="row general-info">
        <div class="col-md-6">
            <div class="row">
                <div class="col-md-4">
                    <div class="image text-center">
                        <?= CHtml::image(
                            Image::open(
                                is_null($worker->user->image) || $selection->isDemo() ?
                                    'images/Web/no-image.png' :
                                    $worker->user->image
                            )->zoomCrop(100, 100)->png(), '', array(
                            'class' => 'img-circle',
                            'height' => 100,
                            'width' => 100
                        ));
                        ?>
                    </div>
                </div>
                <div class="col-md-8 main-info">
                    <div class="fullname">
                        <?= $selection->isDemo() ? Yii::t('web', $worker->gender_id == 1 ? 'Kandidatas' : 'Kandidatė') : $worker->fullname ?>
                    </div>
                    <div class="age"><?= $worker->age . ' ' . $this->numberString($worker->age, 'met', array('ų', 'as', 'ai')) ?></div>
                    <div class="email">
                        <?php if ($selection->isDemo()): ?>
                            <?= CHtml::mailto('*******@gmail.com', '*******@gmail.com', array(
                                'class' => 'green-color'
                            )) ?>
                        <?php else: ?>
                            <?= CHtml::mailto($worker->user->email, $worker->user->email, array(
                                'class' => 'green-color'
                            )) ?>
                        <?php endif; ?>
                    </div>
                    <div class="number">
                        <?php if ($selection->isDemo()): ?>
                            +3706******
                        <?php else: ?>
                            <?= $worker->number ?>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($worker->admin_comment)): ?>
                        <div class="admin-comment">
                            <div class="col-md-1"><?= CHtml::image('/images/Web/admin_comment.png'); ?></div>
                            <div class="col-md-11 comment">
                                <?= $worker->admin_comment ?>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-6 types-score">
            <?php foreach ($worker->typeResultsShow as $result): ?>
                <div class="col-md-6">
                    <div class="type">
                        <div class="col-md-12 no-padding">
                            <div class="name"><?= $result['name'] ?></div>
                        </div>
                        <div class="col-md-10 no-padding">
                            <div class="progress">
                                <span style="width:<?= $result['percent'] ?>%"></span>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="progress-percent">
                                <?= $result['percent'] ?>%
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
            <?php if ($selection->hasQuiz): ?>
                <?php foreach ($selection->selectionQuizes as $selectionQuiz): ?>
                    <div class="col-md-6">
                        <div class="type">
                            <div class="col-md-12 no-padding">
                                <div class="name"><?= $selectionQuiz->quiz->name ?></div>
                            </div>
                            <div class="col-md-10 no-padding">
                                <div class="progress">
                                    <span style="width:<?= $candidate->workerUser->getQuizScore($selectionQuiz->quiz) ?>%"></span>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="progress-percent">
                                    <?= $candidate->workerUser->getQuizScore($selectionQuiz->quiz) ?>%
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="row green-block">
        <div class="col-md-6 download">
            <?php if (!$selection->isDemo()): ?>
                <?php if (!empty($worker->cv)): ?>
                    <a href="<?= $worker->cvLink; ?>" target="_blank">
                        <i class="download-icon"></i><?= Yii::t('web', 'Atsisiųsti kandidato CV') ?>
                    </a>
                <?php endif; ?>
            <?php else: ?>
                <a href="#" data-sweetalert="true" data-title="DEMO" data-description="Demo versija" data-type="error">
                    <i class="download-icon"></i><?= Yii::t('web', 'Atsisiųsti kandidato CV') ?>
                </a>
            <?php endif; ?>
        </div>
        <div class="col-md-6 <?= empty($worker->cv) ? 'col-md-offset-6' : ''; ?> general-score">
            <?= Yii::t('web', 'Bendras kandidato įvertinimas <span>{n}<sub>%</sub></span>', $selection->getCandidateScore($worker) ) ?>
        </div>
    </div>
    <div class="row additional-info">
        <?php if (!empty($worker->salary)): ?>
        <div class="col-md-12 salary">
            <?= Yii::t('web', 'Pageidaujamas atlyginimas (Eur): <span class="green-color">{n}</span>', $worker->salary); ?>
        </div>
        <?php endif; ?>
        <div class="col-md-12 competences">
            <?php foreach ($competences as $competence): ?>
                <span class="competence <?= $competence['active'] ? 'active' : '' ?>"><?= $competence['name'] ?></span>
            <?php endforeach; ?>
        </div>
        <div class="col-md-12 work-history">
            <div class="legend"><?= Yii::t('web', 'Darbo patirtis') ?></div>
            <?php if (!empty($worker->workerHistories)): ?>
                <table class="table col-md-12">
                    <tr>
                        <th class="text-left"><?= Yii::t('web', 'Įmonė') ?></th>
                        <th class="text-left"><?= Yii::t('web', 'Pareigos') ?></th>
                        <th class="text-left"><?= Yii::t('web', 'Data') ?></th>
                    </tr>
                    <?php foreach($worker->workerHistories as $history): ?>
                        <tr>
                            <td><?= $history->company ?></td>
                            <td><?= $history->position ?></td>
                            <td><?= $history->dateRange ?></td>
                        </tr>
                    <?php endforeach; ?>
                </table>
            <?php else: ?>
                <div class="text-center">
                    <?= Yii::t('web', 'Kandidatas neturi darbo patirties'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
    <div class="row actions">
        <div class="col-md-9 attributes">
            <div class="starred-attr">
                <?=
                CHtml::checkBox('starred', $candidate->starred, array(
                    'placeholder' => Yii::t('web', 'Pažymėti kandidatą'),
                    'class' => 'checkbox-small',
                    'value' => $candidate->id,
                    'id' => 'starred_' . $candidate->id

                ));
                ?>
            </div>
            <div class="hidden-attr">
                <?=
                CHtml::checkBox('hidden', $candidate->hidden, array(
                    'placeholder' => Yii::t('web', 'Paslėpti kandidatą'),
                    'class' => 'checkbox-small',
                    'value' => $candidate->id,
                    'id' => 'hidden_' . $candidate->id
                ));
                ?>
            </div>
        </div>
    </div>
    <div class="row about-types">
        <?php foreach($typeDisplay as $type_id): ?>
            <?php if (!isset($typeInfo[$type_id]['description'])): ?>
                <?php continue; ?>
            <?php endif; ?>
            <div class="types-block">
                <div class="row">
                    <div class="title col-md-11">
                        <span class="green-color"><?= $worker->typeResultsShow[$type_id]['percent'] ?>%</span>
                        <?= $typeInfo[$type_id]['type']; ?>
                    </div>
                    <div class="col-md-1 text-right"><i class="more"></i></div>
                    <div class="col-md-11">
                        <div class="description">
                            <?= $typeInfo[$type_id]['description'] ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>