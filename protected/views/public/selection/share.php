<?php $this->breadcrumb = $selection->position . ' / ' . Yii::t('web', 'Pakvieskite kandidatus'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="back">
                <a href="<?= Yii::app()->createUrl('employer/selections') ?>" class="green-color">
                    <?= Yii::t('web', '< Grįžti į atrankų sąrašą') ?>
                </a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="blue-heading-block">
                <div class="col-md-12">
                    <div class="title"><?= Yii::t('web', 'Pakvieskite kandidatus atlikti testus') ?></div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?php
            /** @var CActiveForm $form */
            $form = $this->beginWidget('CActiveForm', array(
                'id' => 'invitation-form',
                'action' => Yii::app()->createUrl('selection/share', array(
                        'id' => $selection->id
                    )
                ),
                'enableAjaxValidation' => true,
                'clientOptions' => array(
                    'validateOnSubmit' => true,
                    'validateOnChange' => false,
                    'validateOnType' => false,
                    'afterValidate' => 'js:function(form, data, hasError) {
                        if (!hasError) {
                            swal({
                                title: "' . Yii::t('web', 'Pranešimai šiunčiami!') . '",
                                text: "' . Yii::t('web', 'Atranka sėkmingai pasidalinta') . '",
                                type: "success",
                                showCancelButton: true,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "' . Yii::t('web', 'Grįžti į atrankų sąrašą') . '",
                                cancelButtonText: "' . Yii::t('web', 'Pasilikti') . '",
                                closeOnConfirm: false,
                                closeOnCancel: false,
                            }, function(isConfirm){
                                if (isConfirm) {
                                    window.location.href = "' . Yii::app()->createUrl('employer/selections') . '";
                                } else {
                                    window.location.href = window.location.href;
                                }
                            });
                        }
                    }'
                )
            ));
            ?>
            <div class="row">
                <div class="col-md-12">
                    <div
                        class="legend blue-color"><?= Yii::t('web', 'El. pašto adresą atskirkite nauja eilute') ?></div>

                    <?=
                    $form->textArea($model, 'emails', array(
                        'placeholder' => "info@cvopen.lt\r\njonas@jonaitis.lt\r\npetras@petraitis.lt",
                        'rows' => 10
                    ));
                    ?>

                    <?= $form->error($model, 'emails'); ?>

                </div>
                <div class="col-md-12 text-right mt30">
                    <?=
                    CHtml::submitButton(Yii::t('web', 'Siųsti'), array(
                        'class' => 'btn btn-blue'
                    ));
                    ?>
                </div>
            </div>

            <?php $this->endWidget(); ?>
        </div>
        <?php if (!empty($selectionShareEmails)): ?>
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <input placeholder="El. paštas" id="table-search" type="text">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <table class="default-table selection-share-table">
                    <thead>
                    <tr>
                        <th class="text-left"><?= Yii::t('web', 'Nr.') ?></th>
                        <th class="text-left"><?= Yii::t('web', 'El. paštas') ?></th>
                        <th class="sort"><?= Yii::t('web', 'Išsiųsta') ?></th>
                        <th class="sort"><?= Yii::t('web', 'Užsiregistravo') ?></th>
                        <th class="sort"><?= Yii::t('web', 'Atliko testą') ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($selectionShareEmails as $i => $selectionShare): ?>
                        <tr>
                            <td><?= $i + 1 ?></td>
                            <td class="filter"><?= $selectionShare->email ?></td>
                            <td class="text-center sent" data-val="<?= $selectionShare->sent ? strtotime($selectionShare->sent) : 0 ?>">
                                <?php if ($selectionShare->sent): ?>
                                    <?= $selectionShare->sent ?>
                                <?php else: ?>
                                    <?= Yii::t('app', 'Siunčiama') ?>
                                <?php endif; ?>
                            </td>
                            <td class="text-center info <?= $selectionShare->register ? 'green' : 'red' ?>" data-val="<?= (int) $selectionShare->register ?>">
                                <i class="fa fa-lg fa-<?= $selectionShare->register ? 'check' : 'times' ?>"></i>
                            </td>
                            <td class="text-center info <?= $selectionShare->quizCompelete ? 'green' : 'red' ?>" data-val="<?= (int) $selectionShare->quizCompelete ?>">
                                <i class="fa fa-lg fa-<?= $selectionShare->quizCompelete ? 'check' : 'times' ?>"></i>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php endif; ?>
    </div>
</div>