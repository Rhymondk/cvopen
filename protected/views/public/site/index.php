<?php $this->renderPartial('_slider', array(
    'selections' => $selections
)); ?>
<div class="grey-background">
    <div class="container">
        <div class="row heading-description">
            <div class="col-md-12 text-center">
                <?= Yii::t('web', 'Mes mąstome kitaip.
             Siekiame mesti iššūkį nusistovėjusiam darbuotojų atrankos <a href="http://lt.wikipedia.org/wiki/Status_quo" target="_blank">status quo</a>.
             Sukūrėme personalo vertinimo įrankį prieinamą kiekvienam.
             Objektyvų, teisingą ir paprastą.'); ?>
            </div>
        </div>
        <div class="row features">
            <div class="col-md-12 text-center">
                <div class="feature">
                    <div
                        class="icon"><?= CHtml::image(Image::open('images/Web/tests.png')->resize(146, 160)->png(), Yii::t('web', 'Testai')); ?></div>
                    <div class="heading"><?= Yii::t('web', 'Testai'); ?></div>
                    <div
                        class="description"><?= Yii::t('web', 'Asmenybės savybių ir mąstymo gebėjimų testai skirti specialiai Lietuvos rinkai.'); ?></div>
                </div>
                <div class="feature">
                    <div
                        class="icon"><?= CHtml::image(Image::open('images/Web/quality.png')->resize(146, 160)->png(), Yii::t('web', 'Kokybė')); ?></div>
                    <div class="heading"><?= Yii::t('web', 'Kokybė'); ?></div>
                    <div
                        class="description"><?= Yii::t('web', 'Mūsų sukurti testai yra patikimi, validūs, ir atitinka visus šiems testams keliamus reikalavimus.'); ?></div>
                </div>
                <div class="feature">
                    <div
                        class="icon"><?= CHtml::image(Image::open('images/Web/lie-scale.png')->resize(146, 160)->png(), Yii::t('web', 'Melo skalė')); ?></div>
                    <div class="heading"><?= Yii::t('web', 'Melo skalė'); ?></div>
                    <div
                        class="description"><?= Yii::t('web', 'Unikali "Melo skalė" leidžia 99 proc. tikslumu nustatyti kada sakoma tik dalis tiesos.'); ?></div>
                </div>
                <div class="feature">
                    <div
                        class="icon"><?= CHtml::image(Image::open('images/Web/speed.png')->resize(146, 160)->png(), Yii::t('web', 'Greitis')); ?></div>
                    <div class="heading"><?= Yii::t('web', 'Greitis'); ?></div>
                    <div
                        class="description"><?= Yii::t('web', 'Jau pirmosiomis atrankos dienomis pateiksime rezultatus ir rekomendacijas apie kiekvieną reikalavimus atitinkantį kandidatą.'); ?></div>
                </div>
                <div class="feature">
                    <div
                        class="icon"><?= CHtml::image(Image::open('images/Web/guarantee.png')->resize(146, 160)->png(), Yii::t('web', 'Garantija')); ?></div>
                    <div class="heading"><?= Yii::t('web', 'Garantija'); ?></div>
                    <div
                        class="description"><?= Yii::t('web', 'Per 3 mėn. kandidatui nepatvirtinus lūkesčių įsipareigojame grąžinti sumokėtus pinigus.'); ?></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="remodal" data-remodal-id="rules">
    <?php $this->renderPartial('_rules'); ?>
    <a class="remodal-cancel" href="#"><?= Yii::t('web', 'Uždaryti'); ?></a>
</div>
<div class="remodal video" data-remodal-id="video">
    <?php $this->renderPartial('_video'); ?>
</div>
<?php if (Yii::app()->user->isGuest): ?>
<div class="container">
    <div class="row registration">
        <div class="col-md-6 register-employer">
            <div class="heading blue-color">
                <?= Yii::t('web', 'Darbdaviui'); ?>
            </div>
            <div class="form">
                <?php
                $this->renderPartial('_registerForm', array(
                    'user' => $user,
                    'role' => 'Employer',
                    'role_id' => 2
                ));
                ?>
            </div>
        </div>
        <div class="col-md-6 register-worker">
            <div class="heading blue-color">
                <?= Yii::t('web', 'Kandidatui'); ?>
            </div>
            <div class="form">
                <?php
                $this->renderPartial('_registerForm', array(
                    'user' => $user,
                    'role' => 'Worker',
                    'role_id' => 1,
                    'social' => true
                ));
                ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<div class="container">
    <?php $this->widget('ClientsWidget'); ?>
</div>
<div class="quote-bg">
    <div class="container">
        <div class="quates">
            <div class="item">
                <div class="text-center col-md-12 quote">
                    <?= Yii::t('web', '„Dirbant su CVOPEN ypač patiko tai, kad jie lankstūs susitarimuose, kokybiškas bendradarbiavimas nuo iki, profesionali komanda, kuri mums padėjo surasti darbuotoją tokio kokio ir tikėjomės! Drąsiai rekomenduojame savo draugams bei kitiems darbdaviams.”'); ?>
                </div>
                <div class="col-md-4 col-md-offset-4 author text-center">
                    <?= Yii::t('web', 'Mindaugas. UAB "Skaitmeninė reklama"'); ?>
                </div>
            </div>
            <div class="item">
                <div class="text-center col-md-12 quote">
                    <?= Yii::t('web', '„Matėme atranką iš vidaus ir žinojome, kad kiekvienas kandidatas yra ištestuotas, matėme jų atliktų testų rezultatus įvairiais pjūviais ir buvome tikri, kad pasirinkome tinkamiausią kandidatą iš visų dalyvavusių”'); ?>
                </div>
                <div class="col-md-4 col-md-offset-4 author text-center">
                    <?= Yii::t('web', 'Aurimas, UAB "Korala"'); ?>
                </div>
            </div>
            <div class="item">
                <div class="text-center col-md-12 quote">
                    <?= Yii::t('web', '„Rekomenduojame CVOPEN. Pabandėm – patiko. Sakyčiau, tai puiki alternatyva kitoms atrankos kompanijoms.“'); ?>
                </div>
                <div class="col-md-4 col-md-offset-4 author text-center">
                    <?= Yii::t('web', 'Mindaugas, UAB "KG Contruction"'); ?>
                </div>
            </div>
            <div class="item">
                <div class="text-center col-md-12 quote">
                    <?= Yii::t('web', '„Patys išbandėme CVOPEN testus, kadangi įrankis pasirodė patikimas, nusprendėme keliauti toliau ir perdavėme savo vykstančią atranką CVOPEN komandai.“'); ?>
                </div>
                <div class="col-md-4 col-md-offset-4 author text-center">
                    <?= Yii::t('web', 'Gintaras, UAB "Doverta"'); ?>
                </div>
            </div>
            <div class="item">
                <div class="text-center col-md-12 quote">
                    <?= Yii::t('web', '„Su CV OPEN tinkamą kandidatą pamatėme beveik iš karto, pakalbinome 3 geriausiai testus atlikusius kandidatus ir jau antrą atrankos savaitę sukirtome rankomis.“'); ?>
                </div>
                <div class="col-md-4 col-md-offset-4 author text-center">
                    <?= Yii::t('web', 'Marius, UAB "Ireko"'); ?>
                </div>
            </div>
        </div>
    </div>
</div>