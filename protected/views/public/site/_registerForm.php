<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => $role . '-form',
    'action' => Yii::app()->createUrl('user/register', isset($selection) ? array('selection' => $selection) : array()),
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => false,
        'validateOnType' => false,
        'afterValidate' => 'js:function(form, data, hasError) {
            if (!hasError) {
                app.redirect(data.redirect);
            }
        }'
    )
));

echo $form->textField($user, 'name', array(
    'class' => 'input-icon input-icon-profile',
    'placeholder' => $role_id === 1 ? Yii::t('web', 'Vardas Pavardė') : Yii::t('web', 'Įmonės pavadinimas')
));

echo $form->error($user, 'name');

echo $form->textField($user, 'email', array(
    'class' => 'input-icon input-icon-mail',
    'placeholder' => Yii::t('web', 'El. paštas')
));

echo $form->error($user, 'email');

echo $form->passwordField($user, 'password', array(
    'class' => 'input-icon input-icon-password',
    'placeholder' => Yii::t('web', 'Slaptažodis')
));

echo $form->error($user, 'password');

echo $form->passwordField($user, 'repeatPassword', array(
    'class' => 'input-icon input-icon-password',
    'placeholder' => Yii::t('web', 'Pakartoti slaptažodį')
));

echo $form->error($user, 'repeatPassword');

echo $form->checkBox($user, 'rules' . $role_id, array(
    'placeholder' =>
        Yii::t('web', 'Sutinku su ') .
        CHtml::link(Yii::t('web', 'taisyklėmis'), '#rules', array(
            'class' => 'green-color semi-bold'
        ))
));

echo $form->error($user, 'rules' . $role_id);

echo CHtml::hiddenField('User[role_id]', $role_id);

echo CHtml::submitButton(Yii::t('web', 'Registruotis'), array(
    'class' => 'btn btn-green margin btn-fluid'
));

if (isset($social) && $social) {
    $this->widget('ext.hoauth.widgets.HOAuth');
}

echo CHtml::hiddenField('User[ip]', Yii::app()->request->getUserHostAddress());

echo $form->error($user, 'ip');

$this->endWidget();
?>