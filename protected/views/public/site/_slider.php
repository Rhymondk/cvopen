<?php Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/public/slides.min.css'); ?>
<div class="slider">
    <div class="slider__wrapper">
        <div class="slider__item slide__1">
            <div class="container relative text-center">
                <div class="row">
                    <div class="col-md-12">
                        <div class="heading">
                            <?= Yii::t('web', 'Profesionali ir greita darbuotojų atranka'); ?>
                        </div>
                    </div>
                </div>
                <div class="buttons row">
                    <div class="col-md-6 text-right">
                        <div data-scroll=".registration" class="hex-registration-btn hex-button text-center scroll-to">
                            <?= Yii::t('web', 'Registracija'); ?>
                        </div>
                    </div>
                    <div class="col-md-6 text-left">
                        <?=
                        CHtml::link(Yii::t('web', 'Sužinok daugiau'), Yii::app()->createUrl('faq/index'), array(
                            'class' => 'hex-moreinfo-btn hex-button text-center'
                        ));
                        ?>
                    </div>
                </div>
                <div class="container">
                    <div class="browser-image absolute col-md-10 col-md-offset-1">
                        <div id="worker-selections">
                        <?php if ($selections): ?>
                            <div class="row">
                                <div class="selection-table col-md-12">
                                    <div class="selection-tr thead">
                                        <span><?= Yii::t('web', 'Paskelbta') ?></span>
                                        <span><?= Yii::t('web', 'Įmonė') ?></span>
                                        <span><?= Yii::t('web', 'Pareigos') ?></span>
                                        <span><?= Yii::t('web', 'Miestas') ?></span>
                                        <span><?= Yii::t('web', 'Atlyginimas (€)') ?></span>
                                        <span><?= Yii::t('web', 'Galioja iki') ?></span>
                                        <span></span>
                                    </div>
                                    <?php foreach ($selections as $selection): ?>
                                        <a href="<?= Yii::app()->createUrl('worker/view', array('id' => $selection->id)) ?>"
                                           class="selection-tr">
                                            <span class="nowrap"><?= $selection->date_start ?></span>
                                            <span><?= $selection->employer->name; ?></span>
                                            <span><?= $selection->position ?></span>
                                            <span><?= $selection->city->name ?></span>
                                            <span><?= $selection->salary ?></span>
                                            <span class="nowrap"><?= $selection->date_end ?></span>
                                            <span><?= !empty($selection->employer->image) ? CHtml::image(Image::open($selection->employer->image)->resize(70, 40)) : ''; ?></span>
                                        </a>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>