<?php $this->breadcrumb = 'Kandidato registracija'; ?>
<div class="container">
    <div class="row registration">
        <div class="col-md-offset-3 col-md-6 register-worker">
            <div class="heading blue-color">
                <?= Yii::t('web', 'Kandidatui'); ?>
            </div>
            <div class="form">
                <?php
                $this->renderPartial('_registerForm', array(
                    'user' => $user,
                    'role' => 'Worker',
                    'role_id' => 1,
                    'selection' => $selection,
                    'social' => true
                ));
                ?>
            </div>
        </div>
    </div>
    <div class="remodal" data-remodal-id="rules">
        <?php $this->renderPartial('_rules'); ?>
        <a class="remodal-cancel" href="#"><?= Yii::t('web', 'Uždaryti'); ?></a>
    </div>
</div>