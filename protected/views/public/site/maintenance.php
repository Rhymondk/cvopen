<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="flex-center">
                <div class="logo animated flipInY">
                    <?= CHtml::image(Image::open('images/Web/logo.png')->resize(301, 150)->png(), 'cvopen'); ?>
                </div>
            </div>
        </div>
    </div>
</div>