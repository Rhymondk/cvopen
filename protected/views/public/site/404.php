<?php $this->breadcrumb = Yii::t('web', 'Puslapis nerastas'); ?>
<div class="container">
    <div class="row text-center">
        <div class="col-md-12">
            <h1>404</h1>
            <h3><?= Yii::t('web', 'Puslapis nerastas'); ?></h3>
            <?=
            CHtml::link(Yii::t('web', 'Grįžti į pagrindinį'), Yii::app()->createUrl('site/index'), array(
                'class' => 'btn btn-blue back'
            ));
            ?>
        </div>
    </div>
</div>