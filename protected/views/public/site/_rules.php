<!--googleoff: all-->
<div class="text-justify remodal-rules">
    <p class="heading1">CV OPEN sistemos Privatumo politika</p>

    <p>
        CV OPEN sistema imasi visų priemonių bei galimų veiksmų, siekdama apsaugoti sistemos Vartotojų asmeninę informaciją.
        Asmuo apsilankydamas Svetainėje ir naudodamas Paslaugas, pripažįsta ir sutinka su <strong>(“Privatumo politika”)</strong>. Dalis šios
        Privatumo politikos sąlygų yra perkeltos ir į <strong>Naudojimosi CV OPEN sistemos paslaugomis sutartį</strong> pateiktą šioje
        Svetainėje: www.cvopen.lt. Privatumo politika apibūdina CV OPEN sistemos praktiką dėl naudojimosi CV OPEN
        sistemos Paslaugomis pateiktos vartotojų asmeninės informacijos naudojimo.
    </p>

    <p>
        <strong>“Asmeninė informacija”</strong> tai visa (bet kokia) informacija, kuri tiesiogiai ar netiesiogiai
        identifikuoja asmenį (įskaitant, bet neapsiribojant, asmens vardu
        ir pavarde, adresu, elektroninio pašto adresu, telefono numeriu, taip pat kita dažniausiai viešai neskelbiama
        informacija). CV OPEN sistema renka ir
        naudoja Asmeninę informaciją išimtinai Paslaugų tinkamo suteikimo tikslais ir tik šioje Privatumo politikoje bei
        Naudojimosi CV OPEN sistemos paslaugomis
        sutartyje numatyta tvarka ir apimtimi.
    </p>

    <p>
        <strong>ASMUO, NAUDODAMAS PASLAUGAS IR (ARBA) TEIKIANTIS ASMENINĘ INFORMACIJĄ, AIŠKIAI IŠREIŠKIA SAVO SUTIKIMĄ
            DĖL ASMENINĖS INFROMACIJOS TVARKYMO, KIEK TAI
            NUMATYTA ŠIOJE PRIVATUMO POLITIKOJE BEI NAUDOJIMOSI CV OPEN SISTEMOS PASLAUGOMIS SUTARTYJE.</strong>
    </p>

    <p class="heading2">
        CV OPEN sistemos renkama ir Paslaugų teikimui naudojama informacija.
    </p>

    <p>
        CV OPEN sistema renka toliau nurodytą Asmeninę informaciją, kurią asmuo savanoriškai pateikia registruojantis
        Svetainėje arba pildydamas testus.
    </p>
    <ul class="disc">
        <li>
            <p>
                Asmuo, norėdamas tapti Registruotu vartotoju, pateikia Svetainėje savo elektroninio pašto adresą ir
                susikuria slaptažodį. Vartotojui
                registruojantis taip pat gali būti prašoma pateikti įmonės pavadinimą, kurios CV OPEN sistemoje Paskyros
                nariu šis Vartotojas pageidauja tapti bei
                nurodyti jo šioje įmonėje užimamas pareigas.
            </p>
        </li>
        <li>
            <p>
                CV OPEN sistema surenka visą informaciją, kurią asmuo pateikia registruojantis, pasirinkdamas tokį
                turinį prisidėti prie savo Asmeninės Paskyros,
                pažymėdamas ar sutinka dalyvauti CV OPEN sistemos rengiamose apklausose (tiesiogiai ar per trečiųjų
                šalių paslaugų teikėjus) ir kokiu būdu norėtų
                gauti su Paslaugų teikimu susijusią informaciją.
            </p>
        </li>
        <li>
            <p>
                Informacija apie kandidatus, ir kita informacija, kiek tai susiję su darbdaviu ir asmenų pasidalintais
                darbo skelbimais, atlyginimų ir premijų
                ataskaitomis, tyrimais ir kito turinio informacija, bus renkama CV OPEN sistemoje.
            </p>
        </li>
        <li>
            <p>
                Asmeniui susisiekus su CV OPEN sistema, atsiunčiant elektroninį laišką arba naudojant bet kurią kitą CV
                OPEN sistemos kontaktinių formų, CV OPEN
                sistema gali rinkti bet kokią informaciją, esančią ir (arba) pridedamą prie elektroninio laiško ar prie
                užpildytos kontaktinės formos.
            </p>
        </li>
    </ul>
    <p class="heading3">
        Asmens prisistatymas
    </p>

    <p>
        Asmens prisistatymas <strong>(“Profilis”)</strong> – tai jo vizitinė kortelė ir gyvenimo aprašymas, naudojantis
        CV OPEN sistema. Asmuo gali keisti, atnaujinti savo asmeninę
        informaciją bet kuriuo metu, kai tik prisijungia prie savo Paskyros, vadovaudamasis nustatymais „Profilio
        redagavimas“. Asmuo, norėdamas pašalinti savo
        prisistatymo aprašymą, gali pasirinkti anoniminę Paskyrą.
    </p>

    <p class="heading2">
        Asmeninė informacija surinkta technologijų būdu
    </p>

    <p>
        Naudojantis Paslaugomis, tam tikra informacija, įskaitant ir internetinį adresą (IP) bei naršymo istoriją, taip
        pat gali būti renkama pasyviai. CV OPEN
        sistema kaupia nedidelių tekstų bylas, vadinamas <strong>“Slapukais”</strong> (angl. Cookie), siekiant kaupti
        prisijungimo informaciją (jei tokia yra) bei pageidavimus dėl
        teikiamų Paslaugų. CV OPEN sistema gali naudoti abiejų sesijų slapukus, t. y. vieni baigiasi, kai yra uždaroma
        naršyklė, o kiti yra nuolatiniai, liekantys
        kompiuteryje, kol jie nėra ištrinami. Naudojantis Paslaugomis, tačiau išjungus naršyklėje slapukus, tam tikros
        Paslaugos gali veikti mažiau efektyviai. Po
        to, kai yra užsiregistruojama ir įgalinami slapukai, CV OPEN sistema gali susieti šią informaciją su asmenine
        informacija.
    </p>

    <p class="heading2">
        Asmeninė informacija iš kitų šaltinių
    </p>

    <p>
        CV OPEN sistema gali gauti Asmeninę informaciją taip pat ir už Svetainės ribų, t. y. iš telefono, elektorinio
        pašto, iš kitų trečiųjų šalių, teikiančių
        šiai sistemai paslaugas, susijusias su teikiamomis Paslaugomis. Ši informacija gali būti susieta su jau turima
        informacija, gauta per Svetainę.
    </p>

    <p class="heading2">
        CV OPEN sistemos surinktos informacijos naudojimas
    </p>

    <p>
        Asmenų (įskaitant Asmeninę informaciją) CV OPEN sistemai bei trečiosioms šalims, palaikančioms CV OPEN sistemos
        renkamą informaciją naudoja tik teikdama
        Paslaugas Vartotojams, atsakydama į prašymus, siekiant pagerinti CV OPEN sistemos Paslaugas bei geriau
        pritaikyti Svetainės funkcijas, veiklą ir palaikymą.
    </p>

    <p>
        Asmeninė informacija taip pat gali būti naudojama siunčiant pranešimus, susijusius su Paslaugų naudojimu, taip
        pat kitokio pobūdžio informacija, įskaitant,
        naujenlaiškius, elektroninę reklamą ir panašius pranešimus.
    </p>

    <p>
        CV OPEN sistema Asmeninę informaciją gali pakeisti anonimiška informacija ir naudoti ją vidiniams tikslams
        (analizuojant naudojimosi Svetaine būdus,
        siekiant pagerinti Paslaugų teikimą). Sistema pasilieka teisę naudoti ir atskleisti tretiesiems asmenims tokią
        anonimišką informaciją savo nuožiūra.
    </p>

    <p class="heading2">
        Viešas paskelbimas
    </p>

    <p>
        Vienas iš Paslaugų tikslų yra galimybė Registruotiems vartotojams dalintis su darbu susijusia informacija su
        kitais Vartotojais bei platinti ją viešai.
        Todėl darytina prielaida, jog bet kokia pateikta informacija (įskaitant bet kokia asmeninė informacija, kurią
        galima pasirinkti pateikti, tačiau išskyrus
        registracijai būtiną informaciją) bus prieinama kitiems Paslaugos Vartotojams. CV OPEN sistema suteikia galimybę
        anonimiškai dalintis darbo pasiūlymų
        nuorodomis, tačiau tokiu atveju kontaktinė asmens informacija (vardas bei pavardė, elektroninio pašto adresas,
        socialinio tinklo kontaktai) nebus
        skelbiama, tačiau CV OPEN sistemos vidinė sistema bei gali susieti asmens kontaktinę informaciją su informacija,
        kuria dalijamasi.
    </p>

    <p class="heading2">
        Asmeninės informacijos atskleidimas
    </p>

    <p>
        CV OPEN sistema negali atskleisti ar dalintis Asmenine informacija su trečiosiomis šalimis, be paties asmens
        prašymo ar leidimo, išskyrus kiek tai numatyta
        šioje Privatumo politikoje.
    </p>

    <p>
        CV OPEN sistema gali suteikti Asmeninę informaciją trečiosioms šalims, teikiančioms paslaugas bei dirbančioms CV
        OPEN sistemos vardu ar kartu su CV OPEN
        sistema ir padedančioms palaikyti ryšį su Vartotojais. Vis dėlto šie paslaugų tiekėjai neturi teisės naudotis
        šia informacija, išskyrus kiek tai reikalauja
        CV OPEN sistemos palaikymui bei siekiant tinkamai suteikti Paslaugas.
    </p>

    <p>
        CV OPEN sistema gali suteikti Asmeninę informaciją patronuojančioms, dukterinėmis įmonėmis, kitiems sistemos
        administratorių kontroliuojantiems bei jo
        kontroliuojamiems asmenims (kartu, <strong>“Agentai”</strong>), iš šių įmonių bei jų teisių perėmėjų bus
        reikalaujama laikytis Privatumo politikos ir Naudojimosi CV OPEN
        sistemos paslaugomis sutarties nuostatų.
    </p>

    <p>
        CV OPEN sistema gali (ir registruodamasis asmuo patvirtina tokį savo sutikimą) atskleisti informaciją, kurią
        surinko apie Vartotoją (įskaitant ir Asmeninę
        informaciją), jeigu mano, jog toks atskleidimas yra būtinas: (a) laikantis teisės aktų reikalavimų bei esant
        atitinkamam teismo sprendimui; (b) siekiant
        užtikrinti tinkamą CV OPEN sistemos Paslaugų naudojimą bei naudojimosi sąlygų laikymąsi; (c) saugant bei ginant
        CV OPEN sistemos, Vartotojų bei trečiųjų
        asmenų teises.
    </p>

    <p class="heading2">
        Asmeninės informacijos pasirinkimas
    </p>

    <p>
        Galima keisti Asmeninę informaciją, prisijungiant Svetainėje ir redaguojant Paskyros informaciją „Profilis“ arba
        atsisakyti atskleisti Asmeninę informaciją
        (ar jos dalį), pasirenkant anonimiškumą.
    </p>

    <p>
        CV OPEN sistema Vartotojams gali siųsti naujienlaiškius, reklaminius pranešimus ar kitus pranešimus savo bei
        partnerių vardu, kurių asmuo gali atsisakyti
        naudojantis nurodymais, esančiais kiekviename tokiame pranešime. Taip pat sistema gali pasiūlyti galimybę
        atsisakyti tam tikrų sąsajų (ryšių), jungiantis
        prie Svetainės ir redaguojant Paskyros informaciją per „Profilis“. Bet kuriuo atveju, pranešimų, susijusių su
        Paslaugomis ir šių pranešimų(ne reklaminio
        pobūdžio pranešimų) asmuo neturi galimybės atsisakyti.
    </p>

    <p>
        Paskyra gali būti pašalinta ir Asmeninė informacija gali būti ištrinta išsiunčiant elektroninį laišką <a
            href="mailto:support@cvopen.lt">support@cvopen.lt</a> arba per Paskyrą pasirenkant „Kontaktai“. CV OPEN
        sistema nebus atsakinga už informacijos
        neišsaugojimą po Paskyros pašalinimo.
    </p>

    <p>
        Pašalinus Paskyrą asmuo gali vėl tapti Registruotu vartotoju tik pateikus naują elektroninio pašto adresą.
    </p>

    <p class="heading2">
        Nuorodos į kitus internetinius puslapius
    </p>

    <p>
        Svetainė ir (ar) siunčiami pranešimai gali turėti nuorodų į kitus internetinius puslapius, kurie nepriklauso CV
        OPEN sistemai. Šios nuorodos yra teikiamos
        Paslaugų naudotojų patogumui. CV OPEN sistema nekontroliuoja, neprižiūri ir nėra atsakinga už tokių interneto
        tinklapių turinio privatumo politiką ir CV
        OPEN sistemos Privatumo politika netaikoma šiems tinkalalpiams.
    </p>

    <p class="heading2">
        Reklamos serveris
    </p>

    <p>
        CV OPEN sistema leidžia Svetainėje skelbti reklaminę medžiagą. Paspaudus ant reklaminio pranešimo, asmuo yra
        nukreipiamas į trečiosios šalies internetinę
        svetainę. Trečiosios šalies skelbimų serveris gali naudoti slapukus arba interneto švyturėlius, susijusius su
        reklama. Privatumo politika neapima trečiųjų
        šalių skelbimų serverių, naudojant slapukus, interneto švyturėlius ar kitas priemones, informacijos. Norėdami
        gauti daugiau informacijos apie reklamos
        partnerius, apsilankykite jų interneto svetainėse per nuorodas, pateiktas reklamos pristatymuose.
    </p>

    <p class="heading3">
        Saugumas
    </p>

    <p>
        CV OPEN sistema siekdamas apsaugoti asmens duomenų bei renkamos informacijos saugumą siekė įdiegti visas galimas
        priemones tam, kad būtų apsaugota ir
        užtikrinta renkamos informacijos saugumas, tačiau CV OPEN sistema negali garantuoti ir visiškai pašalinti su
        asmens informacijos saugumu susijusią riziką.
    </p>

    <p class="heading3">
        Nepilnamečių privatumas
    </p>

    <p>
        CV OPEN sistema sąmoningai nerenka ir nesaugo informacijos iš Svetainės lankytojų bei Vartotojų jaunesnių nei 13
        metų.
    </p>

    <p class="heading2">
        Pakeitimai
    </p>

    <p>
        Jei CV OPEN sistema nuspręs keisti esamą Privatumo politiką, Vartotojai (tam, kad žinotų, kokia informacija yra
        renkama, kaip ir kokiomis aplinkybėmis ji
        naudojama ir atskleidžiama), apie tai bus informuoti elektroniniu paštu (darant esminius Privatumo politikos
        pakeitimus) arba paskelbiant pranešimą CV OPEN
        Svetainėje.
    </p>

    <p class="heading2">
        Susisiekite su mumis
    </p>

    <p>
        Esant klausimams ar pasiūlymams dėl Privatumo politikos, prašome susisiekti elektroniniu paštu <a
            href="mailto:support@cvopen.lt">support@cvopen.lt</a> arba per paskyrą „Kontaktai“.
    </p>

    <p class="heading1">Naudojimosi CV OPEN sistemos teikiamomis paslaugomis sutartis</p>

    <p>
        <strong>CV OPEN</strong> sistema teikia paslaugas, susijusias su informacijos keitimusi apie darbdavius,
        darbuotojus, laisvų darbo vietų pasiūlymus, kandidtų testavimą
        (toliau <strong>“Paslaugos”</strong>) naudojantis CV OPEN sistemos tinklalapiu http://www.cvopen.lt (toliau
        <strong>“Svetainė”</strong>).
        <br/>
        <br/>
        Asmuo, lankydamasis Svetainėje ir naudojantis Paslaugomis, įsipareigoja laikytis Sutarties nuostatų būdamas tiek
        <strong>”Svetainės lankytoju”</strong> (neregistruotu
        vartotoju), tiek ir <strong>”Registruotu vartotoju”</strong> (registruotais vartotojais yra laikomi Svetainėje
        užsiregistravę asmenys) (toliau naudojama <strong>”Vartotojo”</strong> sąvoka
        apima tiek <strong>”Svetainės lankytojo”</strong>, tiek <strong>”Registruoto vartotojo”</strong> sąvoką).
        <br/>
        <br/>
        Pieš pradedant naudotis Paslaugomis ar jų dalimi, prašome atidžiai perskaityti Sutartį. Ši Sutartis yra teisinę
        reikšmę turintis susitarimas, sudaromas
        tarp individualiai naudojančio Paslaugas asmens arba naudojančios Paslaugas įmonės ir CV OPEN sistemos
        administratoriaus.
    </p>

    <p class="heading2">
        1. CV OPEN sistemos paskyra.
    </p>

    <p>
        Tam, kad būtų galima naudotis Paslaugomis (taip pat tapti Registruotu vartotoju), privaloma susikurti paskyrą CV
        OPEN sistemoje, (toliau – <strong>“Paskyra”</strong>).
        Darbdavio sukurta Paskyra, nepaisant to, ar darbdavys yra fizinis ar juridinis asmuo toliau vadinama <strong>„Įmonės
            Paskyra“</strong>, o kandidato Paskyra toliau vadinama
        <strong>„Asmenine Paskyra”</strong>.
    </p>

    <p class="heading3">
        Tinkamumas.
    </p>

    <p>
        Asmuo, naudojantis Paslaugas, pareiškia ir garantuoja, kad: (a) visa pateikta informacija, pateikiama Paskyros
        registracijos metu, yra teisinga ir tiksli;
        (b) bus išlaikytas pateiktos informacijos tikslumas; (c) asmuo yra ne jaunesnis kaip 13 metų amžiaus; ir (d)
        naudojimasis Paslaugomis nepažeis asmens
        atžvilgiu taikomų teisės aktų, taisyklių arba bet kokių kitų įsipareigojimų (įskaitant sutartinius
        įsipareigojimus), turinčių ar galinčių turėti įtakos
        tretiesiems asmenims. Norėdamas susikurti Įmonės Paskyra, asmuo (įmonės darbuotojas) privalo pateikti (i)
        duomenis apie save bei įmonę; (ii) nurodyti
        užimamas pareigas; (iii) po Įmonės Paskyros registracijos, asmuo gali papildyti įmonės profilį pateikdamas
        papildomą informaciją bei įkelti darbo skelbimą
        (toliau – <strong>“Darbo skelbimas”</strong>). Esant pagrįstam pagrindui manyti, jog Svetainėje pateikta
        informacija yra netiksli, neatitinka tikrovės ir (ar) jos turinys
        nėra tinkamas, Paskyra, sukurta Svetainėje, galės būti pašalinta CV OPEN sistemos ir (arba) Įmonės Paskyros
        administratoriaus be papildomo Paskyros
        savininko įspėjimo.
    </p>

    <p class="heading3">
        Slaptažodis.
    </p>

    <p>
        Asmuo, kuris sukurs Įmonės Paskyrą ar susikurs Asmeninę Paskyrą bus paprašytas Paskyrai susikurti slaptažodį.
        Registruotas Vartotojas bus visiškai
        atsakingas už slaptažodžio konfidencialumo išsaugojimą ir turės nedelsiant pranešti CV OPEN sistemos
        administratoriui, kilus įtarimams apie neteisėtą
        bandymą prisijungti prie Paskyros ar gauti prisijungimo slaptažodį.
    </p>

    <p class="heading3">
        Sutarties galiojimas ir jos nutraukimas.
    </p>

    <p>
        Sutartis galioja visa apimtimi (a) Svetainės lankytojui, naudojantis Paslaugomis ir (b) esant Registruotam
        vartotojui. Asmuo turi teisę bet kuriuo metu
        ištrinti savo sukurtą Paskyrą nusiųsdamas prašymą CV OPEN sistemos Vartotojų centrui. Pažymėtina, kad net ir tuo
        atveju, kai asmuo ištrina savo Paskyrą,
        tačiau Paslaugomis naudojasi kaip Svetainės lankytojas, jis lieka Sutarties subjektu. Tuo atveju, jeigu CV OPEN
        sistemos administratorius pašalina Paskyrą,
        tuomet asmuo turi teisę susikurti naują Paskyrą, joje nurodydamas naują įmonę, įkeldamas darbo skelbimus. CV
        OPEN sistemos administratorius turi teisę bet
        kuriuo metu pašalinti sukurtą Paskyrą ir nebeteikti Paslaugų, remiantis šia Sutartimi jam už tai atsakomybė
        nekyla.
    </p>

    <p class="heading2">
        2. Nuosavybės teisės. Paslaugų naudojimas.
    </p>

    <p class="heading3">
        Garantas.
    </p>

    <p>
        Iš asmens nėra reikalaujama nuosavybės teisių į bet kokį Turinį, kurį jis įkelia, pateikia, daro prieinamą
        Svetainėje ar kitaip perleidžia naudojantis
        Paslaugomis. Siekiant, kad Paslaugos būtų teikiamos teisėtai, CV OPEN sistema įsipareigoja naudoti tokį Turinį
        išimtinai Paslaugų tinkamo suteikimo
        tikslais ir toliau nurodytomis sąlygomis. Asmeniui taip pat yra suteikiamos tam tikros naudojimo teisės
        (nurodytos 2.2 skyriuje) į Turinį, kuris priklauso
        CV OPEN sistemai (ar jų licencijuotojams) ir kuris naudojamas teikiant Paslaugas asmeniui ir kitiems
        Vartotojams. Asmuo, skelbdamas bet kokį Turinį
        naudojantis Paslaugomis, sutinka ir suteikia CV OPEN sistemai teisę jį naudoti, kopijuoti ir platinti. Siekiant
        išvengti abejonių, CV OPEN sistemos
        naudojamas tik tas Turinys, kurį įkelia ir (arba) sutinka paviešinti pats Vartotojas.
    </p>

    <p class="heading3">
        Trečiosios šalies turinys.
    </p>

    <p>
        CV OPEN sistema turi ir išsaugo visas nuosavybės teises, susijusias su teikiamomis Paslaugomis. Vartotojams
        atsižvelgiant į Sutarties sąlygas, CV OPEN
        sistema suteikia tik ribotą leidimą iš Svetainės atsisiųsti, peržiūrėti, kopijuoti ir spausdinti CV OPEN
        sistemos Turinį ir tik tiek, kiek tai susiję su
        naudojimusi Paslaugomis.
    </p>

    <p class="heading2">
        3. Tretieji asmenys ir kiti Vartotojai
    </p>

    <p class="heading3">
        Nuosavybės teisė ir apribojimai, susiję su Paslaugomis.
    </p>

    <p>
        Asmuo pripažįta, kad visos intelektinės nuosavybės teisės į Paslaugas (<strong>“išskyrus bet kokį Vartotojo
            pateiktą Turinį”</strong>) priklauso CV OPEN sistemai arba CV
        OPEN sistemos licencijuotojams. Naudojantis Paslaugomis asmuo sutinka ir įsipareigoja: (a) neatgaminti,
        nekeisti, nespausdinti, neperduoti, neplatinti,
        neparduoti, nekurti išvestinių darbų, paremtų Paslaugų ar CV OPEN sistemos Turiniu; (b) neperduoti, nenuomoti,
        neskolinti ar kitais būdais nesuteikti
        prieigos prie Paskyros ir Paslaugų tretiesiems asmenims. “Turinys” – tai bet koks autorinis darbas ar
        informacija, įskaitant bet neapsiribojant
        informacija, susijusia su įmonėmis, darbo skelbimais, darbuotojais, komentarais, nuomonėmis, skelbimais,
        pranešimais, tekstais, testais, paveikslėliais,
        nuotraukomis, autoriniais darbais, elektroniniais laiškais bei kita medžiaga.
    </p>

    <p class="heading3">
        CV OPEN sistemoje skelbiamas Turinys.
    </p>

    <p>
        Asmenims, besinaudojantiems Paslaugomis, suteikiama teisė naudotis kitų Vartotojų, reklamuotojų ir trečiųjų
        šalių Turiniu. CV OPEN sistema nekontroliuoja
        ir neatsako už Svetainėje skelbiamą Turinį, todėl (a) asmuo sutinka, kad CV OPEN sistema nėra atsakinga už
        Turinio pobūdį, įskaitant reklamą ir informaciją
        apie trečiosios šalies gaminius ar paslaugas, darbdavius, darbo pokalbius ir su įmone susijusią informaciją,
        kuri yra pateikiama (atskleidžiama) kitų
        Registruotų vartotojų Įmonės Paskyroje, Darbo skelbimuose ir kurią pateikia darbuotojai; (b) CV OPEN sistema
        negarantuoja Turinio informacijos tikslumo,
        tinkamumo ar kokybės. Taip pat neprisiima atsakomybės už nenumatytą, smerktiną, netikslų, klaidinantį ir
        neteisėtą Turinį, kuris yra pateikiamas kitiems
        Vartotojams, reklamuotojams bei trečiosioms šalims.
    </p>

    <p class="heading3">
        Atsakomybė.
    </p>

    <p>
        Asmens bendradarbiavimas su kitais Vartotojais ar reklamuotojais dėl jų paslaugų, įskaitant prekių bei paslaugų
        apmokėjimus ir pristatymus, taip pat kitos
        sąlygos, garantijos ir kiti pareiškimai, susiję su sandoriais, yra tik asmens ir kito vartotojo ar reklamuotojo
        reikalas. Asmuo sutinka, kad CV OPEN
        sistema nėra atsakinga už bet kokius nuostolius ar žalą, atsiradusius dėl tokių veiksmų, taip pat dėl kitų
        Vartotojų Turinio naudojimo ar informacijos apie
        asmenį atskleidimo, kurią asmuo pateikė viešai ir padarė prieinama Svetainėje.
    </p>

    <p class="heading3">
        4. CV OPEN sistemos Registruotų vartotojų sąveika.
    </p>

    <p>
        Asmuo įsipareigoja nenaudoti informacijos, gautos naudojantis Paslaugomis, siekdamas įžeisti, užgauti ar
        pakenkti kitiems asmenims; norėdamas susisiekti su
        kitu Vartotoju, reklamuoti, gauti ar parduoti kitam Vartotojui informaciją, turėtų gauti Vartotojo išankstinį
        aiškų sutikimą. Siekiant apsaugoti
        Registruotus vartotojus nuo reklamavimo ar perteklinio Turinio, CV OPEN sistema pasilieka teisę apriboti
        Registruotų vartotojų galimybę siųsti kitiems
        Registruotiems vartotojams bet kokio pobūdžio Turinį ir juo dalintis.
    </p>

    <p class="heading2">
        4. Naudojimasis Paslaugomis bei Turiniu.
    </p>

    <p>
        Vartotojas yra visiškai atsakingas už Turinį, pateiktą jo Paskyroje, bei jo sąveiką su kitais Vartotojais.
    </p>

    <p class="heading3">
        Draudžiamas Turinys.
    </p>

    <p>
        Asmuo įsipareigoja, naudojantis Paslaugomis, nesiųsti ir nenaudoti Draudžiamo Turinio. Draudžiamas Turinys – tai
        Turinys, kuris (i) yra įžeidžiantis,
        skatinantis rasizmą, fanatizmą, neapykantą bet kuriai žmonių grupei ar asmeniui, taip pat esantis pornografinio
        ar seksualiai atviro pobūdžio; (ii) yra
        patyčių bei priekabiavimo pobūdžio, skatinantis ir palaikantis priekabiavimą; (iii) apimantis „laiškų -
        šiukšlių“, „grandininių laiškų“, „nepageidaujamų
        masinių laiškų“ perdavimą; (iv) yra klaidingas ir klaidinantis, skatinantis bei remiantis neteisėtą veiklą ar
        elgesį, kuris yra užgaulus, grasinantis,
        nepadorus ir šmeižikiškas; (v) skatina, kopijuoja, atlieka ar platina nelegalią, autorių teises pažeidžiančius
        kitų asmenų darbus bei jų kopijas; teikia
        piratines kompiuterių programas ar nuorodas į jas, taip pat informaciją apie tai, kaip apeiti gamintojo įdiegtą
        apsaugą nuo kopijavimo; teikia piratinę
        muziką, vaizdo įrašus ar filmus, ar nuorodas į piratinę muziką, vaizdo įrašus ar filmus; (vi) yra susijusi su
        asmenų iki 18 metų seksualiniu išnaudojimu ir
        žiaurumu, taip pat siūloma informacija yra apie asmenis iki 18 metų; (vii) teikia informaciją apie neteisėtus
        veiksmus, tokius kaip pasigaminti ar kur
        nusipirkti nelegalių ginklų, pažeidžia asmenų privatumą, platina kompiuterinius virusus bei kitus kenksmingus
        kodus; (viii) siūlo kitų Vartotojų
        slaptažodžius ar kitą asmeninę informaciją komerciniams bei neteisėtiems tikslams; išskyrus atvejus, kai gauna
        CV OPEN sistemos pritarimą (patvirtinimą),
        įtraukia komercinę veiklą ir (ar) reklamavimą (turnyrus, loterijas, mainus, reklamą ir t.t.); (ix) talpina
        įvairius virusus, įskaitant, bet neapsiribojant
        Trojos arklį, kirminus, nepatikimas bylas, panašią programinę įrangą; (x) siunčia ir platina informaciją,
        pažeidžiančią konfidencialumo įsipareigojimus,
        neatskleidimo ir kitus įsipareigojimus trečiosioms šalims, įskaitant sutartinius įsipareigojimus esamiems,
        buvusiems bei būsimiems darbdaviams; (xi) kitaip
        pažeidžia Sutarties sąlygas.
    </p>

    <p class="heading3">
        Turinio užtikrinimas.
    </p>

    <p>
        Svetainėje talpinantis Turinį asmuo pareiškia ir garantuoja, kad (a) jam priklauso Turinys arba jis turi visus
        reikalingus leidimus jį naudoti; (b) Turinys
        nepažeidžia asmenų privatumo teisių, viešosios tvarkos, taip pat autorių teisių bei kitų asmenų teisių; (c) bet
        kokia teikiama informacija apie esamą,
        buvusį ar būsimą darbdavio ar darbuotojo statusą yra tiksli ir neklaidinanti. Asmuo, naudodamas Paslaugas ir
        teikdamas informaciją, privalo įsitikinti, kad
        jam yra leidžiama pateikti šią informaciją, kad ji nepažeidžia jo su trečiaisiais asmenimis sudarytų
        įsipareigojimų, įskaitant konfidencialumą ir
        neatskleidimą.
    </p>

    <p class="heading3">
        Įgyvendinimas.
    </p>

    <p>
        Bet koks Paslaugų naudojimas pažeidžiant Sutartį gali būti pagrindu nutraukti ar sustabdyti galimybę naudotis
        Paslaugomis. CV OPEN sistema taip pat turi
        teisę peržiūrėti bet kokį Turinį, jį ištrinti arba keisti, jeigu pastarasis pažeidžia Sutartį, atitinka
        Draudžiamo Turinio sąvoką arba gali kitaip pažeisti
        Vartotojų ar kitų asmenų teises, daryti jiems žalą ar kelti grėsmę jų saugumui. CV OPEN sistema turi teisę
        ištirti ir imtis atitinkamų veiksmų prieš
        asmenį, pažeidusį Sutarties nuostatas, įskaitant, bet neapsiribojant Turinio pašalinimu iš Svetainės (ar jo
        keitimu), Paskyros pašalinimu, pranešimu
        kompetentingoms institucijoms bei kitokius teisinius veiksmus prieš tokį asmenį.
    </p>

    <p class="heading3">
        Teisėtas naudojimas.
    </p>

    <p>
        Asmuo Paslaugomis privalo naudojasi vadovaudamasis teisės aktais ir tik teisėtais tikslais. Paslaugos skirtos
        įmonės žinomumo didinimui ir darbuotojų
        paieškos bei motyvacinės sistemos naudojimui. Komercinių reklamų partnerių nuorodos ir kitokio pobūdžio
        tarpininkavimas be perspėjimo gali būti pašalinti
        ir gali turėti įtakos Paskyros blokavimui ir (ar) pašalinimui.
    </p>

    <p class="heading3">
        Be sutrikimų.
    </p>

    <p>
        Asmeniui neturi teisės: (i) padengti ar paslėpti reklamos, esančios Svetainėje, ar bet kokio CV OPEN sistemos
        puslapio per HTML / CSS, kodą ar kt.
        priemones; (ii) įsiterpti, sutrikdyti ar sukurti per didelę Paslaugos ar jos tinklų naštą; (iii) įdiegti
        Paslaugoms programines įrangas arba automatinius
        agentus, naudotis Paslauga, turinčia kelias Paskyras, kurti automatines žinutes, taip pat atrasti duomenis iš
        Paslaugų; (iv) įsiterpti, sutrikdyti ar
        pakeisti bet kokius Paslaugų duomenis ir funkcionalumą.
    </p>

    <p class="heading3">
        Kiti vartotojai.
    </p>

    <p>
        Asmuo neturi teisės apsimesti kitu Vartotoju ar kitu asmeniu, įskaitant ir CV OPEN sistemos darbuotoju.
    </p>

    <p class="heading2">
        5. Trečiosios šalies internetinis puslapis.
    </p>

    <p>
        Paslaugos gali apimti nuorodas į trečiųjų šalių tinklalapius (toliau – <strong>“Trečiųjų šalių
            tinklalapis”</strong>), įkeltus CV OPEN sistemos kaip paslaugą tiems, kurie
        domisi šia informacija bei įkeltus kitų Registruotų vartotojų. Asmuo Trečiųjų asmenų tinklalapių nuorodomis
        naudojasi savo rizika. CV OPEN sistema nestebi
        ir nekontroliuoja, taip pat nepriima jokių pretenzijų ir pareiškimų dėl Trečiųjų asmenų tinklalapių. Tiek, kiek
        tokios nuorodos yra teikiamos CV OPEN
        sistemos, jos yra pateikiamos tik patogumo tikslais ir tokios nuorodos į Trečiųjų asmenų tinklalapius nereiškia
        CV OPEN sistemos sąsajų su Trečiųjų asmenų
        tinklalapiais.
    </p>

    <p class="heading2">
        6. Autorių teisės.
    </p>

    <p>
        CV OPEN sistema, gavusi iš autoriaus ar jo agento pranešimą apie teisių pažeidimą, sustabdo Registruotam
        vartotojui Paslaugų teikimą, jeigu pastarasis
        pakartotinai pažeidžia autorių teises – jo Paskyra panaikinama. Jeigu asmuo mano, jog jo darbas buvo
        nukopijuotas, patalpintas Svetainėje ir tokiu būdu
        buvo pažeistos jo teisės, asmuo turėtų susisiekti su CV OPEN sistemos Autorių teisių agentu ir pateikti jam šią
        informaciją: (i) įgalioto asmens,
        veikiančio autoriaus vardu, parašą arba elektroninį parašą; (ii) autoriaus darbą, dėl kurio buvo kreiptasi;
        (iii) nurodyti vietą, kur galimai pažeidžianti
        teises medžiaga buvo patalpinta; (iv) autoriaus adresą, telefono numerį ir elektroninio pašto adresą; (v)
        rašytinį pareiškimą, kad asmuo yra įsitikinęs,
        jog šios medžiagos naudojimui nėra gauta autoriaus, jo agento ar įstatyminio leidimo; (vi) patvirtinimą, jog
        aukščiau pateikta informacija yra tiksli ir
        kad asmuo yra panaudotos medžiagos teisėtas autorius ar autoriaus vardu įgaliotas veiki asmuo.
    </p>

    <p>
        Autorių teisių agento, atsakingo už pranešimus apie autoriaus teisių pažeidimus, kontaktai:
        <a href="mailto:support@cvopen.lt.com">support@cvopen.lt</a>
    </p>

    <p class="heading2">
        7. Atsakomybės apribojimas.
    </p>

    <p>
        CV OPEN sistema nėra atsakinga už neteisingą ar netikslų Turinį (taip pat ir Vartotojų profilių informaciją),
        patalpintą Svetainėje, įkeltą Vartotojų ar
        kitų Svetainės naudotojų. CV OPEN sistema nėra atsakinga už Vartotojų elgesį. CV OPEN sistema neprisiima jokios
        atsakomybės už bet kokias klaidas,
        aplaidumą, defektus, veikimo ar perdavimo atidėjimus, komunikacijos priemonių sutrikimus, neleistiną
        prisijungimą ir bet kokį bendravimą su kitais
        Vartotojais. CV OPEN sistema nėra atsakinga už problemas ar techninius nesklandumus, kilusius dėl techninės ir
        programinės įrangos, įskaitant bet kokią
        žalą, sukeltą Vartotojams ar kitų asmenų kompiuteriams, kuriais buvo gaunama informacija reikalinga naudojantis
        Paslaugomis. Jokiais atvejais CV OPEN
        sistema nebus atsakinga už bet kokius nuostolius ar žalą, kilusius dėl naudojimosi Paslaugomis, dėl Turinio
        patalpinto Svetainėje ar perduoto Vartotojams,
        taip pat dėl bet kokios sąveikos tarp Paslaugos Vartotojų. CV OPEN sistema neužtikrina, kad (a) Paslaugos
        patenkins asmens reikalavimus; (b) Paslaugos bus
        prieinamos nepertraukiamai, laiku, saugiai ir be klaidų; (c) rezultatai, kurie bus gauti iš naudojimosi
        Paslaugomis bus tikslūs ir patikimi.
    </p>

    <p class="heading2">
        8. Atsakomybės apribojimas.
    </p>

    <p>
        CV OPEN sistema jokiais atvejais neatsako už asmens ar trečiosios šalies prarastą pelną, netiesioginius,
        numatomus ar kitokius nuostolius bei žalą,
        kylančią naudojantis Paslaugomis, net ir tuo atveju, jeigu žinojo ar galėjo žinoti, jog tokie nuostoliai ar žala
        gali kilti.
    </p>

    <p class="heading2">
        9. Įvairūs.
    </p>

    <p class="heading3">
        Pakeitimai.
    </p>

    <p>
        Ši Sutartis gali būti CV OPEN sistemos administratoriaus iniciatyva pakeista vienašališkai. Jeigu CV OPEN
        sistemos administratorius keis esmines Sutarties
        nuostatas, asmuo apie tai bus informuotas elektroniniu paštu, nurodytu jo Paskyroje. Asmuo sutinka, kad
        Sutarties pakeitimai įsigalios po 30 dienų nuo
        atitinkamo pranešimo išsiuntimo. Asmens tolimesnis naudojimasis Paslaugomis bus laikomas sutikimu su Sutarties
        pakeitimais.
    </p>

    <p class="heading3">
        Force Majeure.
    </p>

    <p>
        Bet koks pareigų ir įsipareigojimų nevykdymas, netinkamas vykdymas ar vilkinimas, nebus laikomas Sutarties
        pažeidimu, jeigu šis bus nulemtas darbo ginčų,
        gaisrų, žemės drebėjimo, potvynio, karo, terorizmo, valstybės valdymo institucijų veiksmų, kitokių force majeure
        aplinkybių ar kitų įvykių, kurių šalis
        negali kontroliuoti, tačiau dės visas pastangas, pranešdama kitai šaliai apie priežastis, lėmusias Sutarties
        vykdymo uždelsimą ir jos planuojamą vykdymo
        atnaujinimą, kiek įmanoma greičiau.
    </p>

    <p class="heading3">
        Atleidimas nuo atsakomybės bei nebaudžiamumas.
    </p>

    <p>
        Tiek, kiek tai yra leidžiama pagal taikytiną teisę, asmuo įsipareigoja CV OPEN sistemai, jos pareigūnams,
        darbuotojams, agentams ir teisių perėmėjams
        neteikti jokių pretenzijų, reikalavimų, neprašyti atlyginti nuostolių bei žalos, kurie tiesiogiai ar
        netiesiogiai kyla iš: (i) visų sąveikų su kitais
        Vartotojais; (ii) asmens dalyvavimo CV OPEN sistemos naudotojų renginiuose. Asmuo sutinka apsaugoti, ginti CV
        OPEN sistemą, jo dukterines įmones,
        partnerius bei darbuotojus nuo bet kokios žalos, atsakomybės, pretenzijų ir reikalavimų, įskaitant reikalavimus
        apmokėti išlaidas advokatui, susijusias su
        asmens naudojimusi Paslaugomis, taip pat kylančius dėl Sutarties nuostatų pažeidimo.
    </p>

    <p class="heading3">
        Sutarties nuostatų galiojimas.
    </p>

    <p>
        Sprendimai, priimti pagal 2, 4, 3, 6, 7, 8, ir 9 skyrių nuostatas, galios pasibaigus Sutarčiai, taip pat ją
        nutraukus bet kuriuo pagrindu.
    </p>

    <p class="heading3">
        Autorių teisės / Informacija apie prekės ženklą.
    </p>

    <p>
        © 2015, CV OPEN. Visos teisės yra saugomos. CV OPEN ® yra registruotas CV OPEN sistemos prekės ženklas. Asmenims
        nėra leidžiama naudoti Svetainėje
        naudojamais prekių (paslaugų) ženklais be išankstinio rašytinio CV OPEN sistemos administratoriaus ar trečiosios
        šalies, turinčio nuosavybės teise į
        atitinkamą prekės (paslaugos) ženklą, sutikimo.
    </p>

    <div>
        <br/>
    </div>
</div>
<!--googleoff: all-->