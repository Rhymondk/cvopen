<?php $this->breadcrumb = Yii::t('web', $quiz->name); ?>
<div class="container text-center">
    <div class="row">
        <div class="col-md-12">
            <div class="description">
                <?= nl2br($quiz->description); ?>
            </div>
        </div>
        <div class="col-md-12">
            <div class="start-button">
                <?=
                CHtml::link(
                    Yii::t('web', 'Spręsti testą'),
                    Yii::app()->createUrl('quiz/solve'),
                    array(
                        'class' => 'btn btn-blue'
                    )
                )
                ?>
            </div>
        </div>
    </div>
</div>