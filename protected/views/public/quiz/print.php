<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        strong {
            color: #000;
        }

        .container {
            border-top: 2px solid #00adef;
        }

        .logo {
            margin: 15px 0;
        }

        .text-center {
            text-align: center;
        }

        .breadcrumb {
            background-image: url(/images/Web/breadcrumb.png);
            background-position: center center;
            height: 48px;
            width: 800px;
            color: #fff;
            font-size: 24px;
            line-height: 48px;
        }

        .content {
            padding: 0 5%;
        }

        .bold {
            font-weight: bold;
        }

        .color-blue {
            color: #00aaff;
        }

        .content .name {
            font-size: 20px;
            margin-top: 35px;
        }

        .content .date_created {
            font-size: 12px;
            color: #525252;
        }

        .content .legend {
            margin-top: 25px;
            text-transform: uppercase;
        }

        .content .legend-lower {
            margin-top: 25px;
        }

        .content .description {
            font-size: 13px;
            margin-top: 25px;
            color: #717171;
        }

        .content .green-block {
            background-color: #26c5a9;
            color: #fff;
            font-size: 14px;
            padding: 5px 25px 10px 15px;
            margin-top: 40px;
            margin-bottom: 15px;
        }

        .content .green-block .bold {
            font-size: 16px;
        }

        .content .green-block .bold span {
            font-size: 20px;
        }

        .content table {
            margin-top: 50px;
            width: 100%;
        }

        .content table tr td {
            border-top: 1px solid #d7d7d7;
            padding: 15px;
            font-size: 14px;
        }

        .content table tr td.percent {
            text-align: right;
            padding-right: 0;
            font-weight: bold;
            color: #26c5a9;
        }

        .content table tr td.type-name {
            padding-left: 0;
            text-transform: uppercase;
            font-weight: bold;
        }

        .content table tr td.type-sub-name {
            color: #717171;
            font-weight: bold;
            font-size: 12px;
        }

        .content .type-score-name {
            color: #26c5a9;
        }
    </style>
</head>
<body>
<?php
function generateProgressBase64($percent, $width = 400)
{
    $progress = imagecreatetruecolor($width, 10);
    $grey = imagecolorallocate($progress, 222, 222, 222);
    imagefill($progress, 0, 0, $grey);

    $green = imagecolorallocate($progress, 38, 197, 169);
    imagefilledrectangle($progress, 0, 0, (int) $width * ($percent / 100), 10, $green);

    ob_start();
    imagejpeg($progress);
    $image_data = ob_get_contents();
    ob_end_clean();

    return 'data:image/jpeg;base64,' . base64_encode($image_data);
}
?>
<div class="container">
    <div class="logo text-center">
        <img src="/images/Web/logo-print.png"/>
    </div>
    <div class="breadcrumb text-center">
        <?= Yii::t('web', 'Testo rezultatai') ?>
    </div>
    <div class="content">
        <div class="name color-blue bold"><?= $worker->user->name ?></div>
        <div class="date_created">
            <span class="bold"><?= Yii::t('web', 'Užpildyta: ') ?></span>
            <?= $worker->quizOrder->order->date_created ?>
        </div>
        <div class="legend bold"><?= Yii::t('web', 'BENDRŲJŲ MĄSTYMO GEBĖJIMŲ (INTELEKTO) TESTAS') ?></div>
        <div class="description">
            <?= Yii::t('web', 'CV OPEN intelekto testas nustato žmogaus bendruosius gebėjimus, kurie nusako, kaip žmogus sugeba mokytis naujų dalykų, kaip tiksliai ir greitai sprendžia užduotis, kaip tiksliai perteikia savo mintis klausytojams. Aukštas bendrųjų gebėjimų lygis taip pat padeda susitvarkyti su netikėtomis situacijomis, priimti sprendimus, daryti išvadas ir detalių pagrindu sukurti bendrą vaizdą. Daugelio dešimtmečių tyrimai patvirtino, kad protiniai gebėjimai geriausiai atspindi žmogaus gebėjimą mokytis, apdoroti naują informaciją, spręsti problemas greitai ir tiksliai, prisitaikyti prie darbo reikalavimų. Šiame testo apraše pateikti rezultatai yra pagrįsti Jūsų duotais atsakymais, kurie lyginami su tuo, kaip sėkmingai atlieka testus daugelis Lietuvos gyventojų, turinčių vidurinį ir aukštesnį išsilavinimą. Vertinant rezultatus būtinai reikia turėti omenyje, kad žemesnis už vidutinį rezultatas nebūtinai rodo, kad Jūsų protiniai gebėjimai yra menkesni nei kitų, o gali atspindėti, pvz., susijaudinimą arba aplinkos sąlygotus trukdžius (pvz., spręsdamas užduotis Jūs neturėjote galimybės ramiai susikaupti arba jas spręsti visą duotą laiką), kas sutrukdė Jums sėkmingai užpildyti testą.') ?>
        </div>
        <div class="iq-result">
            <div class="legend-lower bold">
                <?= $typeInfo[Yii::app()->params['iq_type_id']]['type'] ?>.
                <span class="type-score-name"><?= $iqTable->name ?> (<?= $iqTable->iq ?> <?= Yii::t('web', 'balai') ?>).</span>
            </div>
        </div>
        <div class="description">
            <?= $typeInfo[Yii::app()->params['iq_type_id']]['description'] ?>
        </div>
        <pagebreak />
        <div class="legend bold"><?= Yii::t('web', 'Atsakymų pasiskirstymas') ?></div>
        <div class="description">
            <?= Yii::t('web', 'Šis grafikas parodo Jūsų atsakymus procentine išraiška, t.y., kiek užduočių Jūs atlikote per nustatytą laiką, kiek užduočių buvo atlikta teisingai, ir kiek neteisingai.') ?>
        </div>
        <div class="content">
            <table cellpadding="0" border="0">
                <tr>
                    <td class="type-name"><?= Yii::t('web', 'Iš viso atsakyta') ?></td>
                    <td><?= CHtml::image(generateProgressBase64($iqQuizResults['answered'])) ?></td>
                    <td class="percent"><?= $iqQuizResults['answered'] ?>%</td>
                </tr>
                <tr>
                    <td class="type-sub-name"><?= Yii::t('web', 'Teisingai') ?></td>
                    <td>
                        <?= CHtml::image(generateProgressBase64($iqQuizResults['correct'])) ?>
                    </td>
                    <td class="percent"><?= $iqQuizResults['correct'] ?>%</td>
                </tr>
                <tr>
                    <td class="type-sub-name"><?= Yii::t('web', 'Neteisingai') ?></td>
                    <td>
                        <?= CHtml::image(generateProgressBase64($iqQuizResults['incorrect'])) ?>
                    </td>
                    <td class="percent"><?= $iqQuizResults['incorrect'] ?>%</td>
                </tr>
                <tr>
                    <td class="type-name"><?= Yii::t('web', 'Neatsakyta') ?></td>
                    <td>
                        <?= CHtml::image(generateProgressBase64($iqQuizResults['unanswered'])) ?>
                    </td>
                    <td class="percent"><?= $iqQuizResults['unanswered'] ?>%</td>
                </tr>
            </table>
        </div>
        <pagebreak />
        <div class="legend bold"><?= Yii::t('web', 'Asmeninių savybių testas'); ?></div>
        <div class="description">
            <?= Yii::t('web', 'CV OPEN asmeninių savybių testas yra parengtas pagal Didžiojo penketo asmenybės teoriją ir leidžia įvertinti asmens bruožus pagal penkias asmenybės dimensijas: <strong>ektravertiškumą, neurotizmą, draugiškumą, sąmoningumą bei atvirumą patirčiai</strong>.
Šis testas parodo rezultatus, lyginant su kitais Lietuvos gyventojais, turinčiais vidurinį ir aukštesnį išsilavinimą.');
?>
        </div>
        <div class="content">
            <?php foreach ($typeDisplay as $type_id): ?>
                <div class="legend-lower bold">
                    <?= $typeInfo[$type_id]['type'] ?>.
                    <span class="type-score-name"><?= $typeInfo[$type_id]['name'] ?> (<?= $typeInfo[$type_id]['min'] ?>-<?= $typeInfo[$type_id]['max'] ?> <?= Yii::t('web', 'balai') ?>).</span>
                </div>
                <div class="description">
                    <?= $typeInfo[$type_id]['description'] ?>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
</body>
</html>