<?php $this->breadcrumb = Yii::t('web', 'Testas atliktas'); ?>
<div class="container">
    <div class="row finish-text">
        <div class="col-md-12">
            <div class="text-block">
                <?= Yii::t('web', 'Testą atlikote per <span>{timeSpent}</span>.
Jūsų atsakymus peržiūrėsime artimiausiu metu.
Jeigu testo rezultatai ir pateikta informacija atitiks kliento reikalavimus,
susisieksime su Jumis dviejų savaičių laikotarpyje.', array(
                    '{timeSpent}' => $timeSpent
                )) ?>
            </div>
            <div class="text-block">
                    <?= Yii::t('web', 'Kandidatai, atlikę CV OPEN sistemoje esančius testus ir norintys gauti savo intelekto ir asmeninių savybių<br/>
<span>įvertinimą ir aprašymą el. paštu,</span> gali siųsti sms numeriu <strong>{paysera_sms_number}</strong> su kodu <strong>{paysera_sms_text}</strong>, žinutės kaina {quiz_price} eur. arba atlikti {quiz_price} eur. mokėjimą per el. bankininkystę.', array(
                        '{paysera_sms_number}' => Yii::app()->params['paysera_sms_number'],
                        '{paysera_sms_text}' => Yii::t('web', Yii::app()->params['paysera_sms_text'], array('{sms_key}' => Yii::app()->user->getId())),
                        '{quiz_price}' => number_format(Yii::app()->params['quiz_price'] / 100, 2, ',', ''),
                    )) ?>
            </div>
            <div class="bank-logos">
                <div class="logos">
                    <?php foreach ($paymentMethods as $paymentMethod) : ?>

                        <?=
                        CHtml::link(
                            CHtml::image($paymentMethod->getLogoUrl(), $paymentMethod->getKey(), array(
                                'width' => 150
                            )),
                            Yii::app()->createUrl('quiz/pay', array('via' => $paymentMethod->getKey()))
                        );
                        ?>
                    <?php endforeach; ?>
                </div>
            </div>
            <div class="remodal" data-remodal-id="preview">
                <?php $this->renderPartial('_preview'); ?>
                <div class="text-center">
                    <a class="remodal-cancel preview-close" href="#">
                        <?= Yii::t('web', 'Uždaryti'); ?>
                    </a>
                </div>
            </div>
            <div class="col-md-12 text-center">
                <div class="preview-button">
                    <?=
                    CHtml::link(Yii::t('web', 'Testo rezultatų pavyzdys'), '#preview', array(
                        'class' => 'btn btn-green',
                        'onclick' => '_gaq.push(["_trackEvent", "Quiz example", "Open", "Opened quiz example"])'
                    ));
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
