<?php $this->timeleft = $quiz->time ? $time_left : ''; ?>
<div id="leave-message" data-text="<?= Yii::t('web', 'Jeigu išeisite iš testo sprendimo puslapio - vertinsime Jūsų rezultatus pagal tai kiek testo buvote atlikę. Perspręsti ar kitaip koreguoti testo rezultatų vėliau nebegalėsite. Dalyvaujant kitose atrankose testo rezultatus sistema priskirs automatiškai.') ?>"></div>
<div class="container">
    <?php $this->renderPartial($question->questionType->template, array(
        'question' => $question,
        'quizTakenAnswer' => $quizTakenAnswer
    )); ?>
    <?php if ($quiz->pagination): ?>
    <div class="col-md-8 col-md-offset-2">
        <div class="pager">
        <?php $i = 1; ?>
        <?php foreach($quiz->quizQuestions as $quizQuestion): ?>
            <?php $solved = in_array($quizQuestion->question_id, $quizAnsweredQuestions); ?>
            <?php $current = $quizQuestion->question_id == $question->id; ?>
            <?php $class = 'page ' . ($solved ? 'solved ' : ' ') . ($current ? 'current ' : ' '); ?>
            <?php if ($solved): ?>
                <div class="<?= $class ?>">
                    <?= $i++ ?>
                </div>
            <?php else: ?>
                <?=
                CHtml::link($i++, Yii::app()->createUrl('quiz/solve', array('id' => $quizQuestion->question_id)), array(
                    'class' => $class
                ));
                ?>
            <?php endif; ?>
        <?php endforeach; ?>
        </div>
    </div>
    <?php endif; ?>
</div>