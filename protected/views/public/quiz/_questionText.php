<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'quiz-taken-asnwer-form',
    'enableClientValidation' => true,
    'clientOptions'=>array(
        'validateOnSubmit'=>true
    )
));
?>
    <div class="row question-text">
        <div class="col-md-12">
            <div class="title">
                <?= Yii::t('web', $question->name) ?>
            </div>
            <div class="answers animated">
                <?= $form->hiddenField($question, 'id'); ?>
                <?php $i = 1; ?>
                <?php foreach ($question->answers as $answer): ?>
                    <?=
                    $form->radioButton($quizTakenAnswer, 'answer_id', array(
                        'placeholder' => $answer->name,
                        'value' => $answer->id,
                        'uncheckValue' => null,
                        'id' => 'answer_' . $answer->id
                    ));
                    ?>
                    <?php $i++ ?>
                <?php endforeach; ?>
            </div>
            <div class="text-center submit-button">
                <?= CHtml::submitButton(Yii::t('web', 'Patvirtinti'), array(
                    'class' => 'btn btn-blue'
                )); ?>
            </div>
        </div>
    </div>
<?php
$this->endWidget();
?>