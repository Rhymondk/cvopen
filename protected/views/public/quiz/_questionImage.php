<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'quiz-taken-asnwer-form'
));
?>
    <div class="row question-image text-center">
        <div class="col-md-8 col-md-offset-2">
            <div class="row">
                <div class="col-md-12">
                    <div class="image">
                        <?= CHtml::image(Image::open($question->image)->resize(450, 280)->jpeg(100), $question->name) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="answers animated">
                            <?= $form->hiddenField($question, 'id'); ?>
                            <?php $i = 1; ?>
                            <?php foreach ($question->answers as $answer): ?>
                                <div class="col-md-4">
                                    <div class="number"><?= $i ?></div>
                                    <?=
                                    $form->radioButton($quizTakenAnswer, 'answer_id', array(
                                        'placeholder' => CHtml::image(Image::open($answer->image)->cropResize(80, 80, '#ffffff')->jpeg(100), $answer->name),
                                        'value' => $answer->id,
                                        'uncheckValue' => null,
                                        'id' => 'answer_' . $answer->id
                                    ));
                                    ?>
                                </div>
                                <?php $i++ ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-12 text-center submit-button">
                        <?= CHtml::submitButton(Yii::t('web', 'Patvirtinti'), array(
                            'class' => 'btn btn-blue'
                        )); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
$this->endWidget();
?>