<h1>Tvarkyti testus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('quiz/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'type-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'name',
        array(
            'class' => 'booster.widgets.TbButtonColumn',
            'template' => '{more}{update}{delete}',
            'buttons' => array(
                'more' => array(
                    'label' => 'Testo klausimai',
                    'url' => 'Yii::app()->createUrl("quiz/question", array("id" => $data->id))',
                    'icon' => 'plus',
                ),
            ),
        ),
    ),
)); 
?>
