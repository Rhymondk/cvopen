<div class="col-md-12 col-lg-12">

<h1><?php echo $model->isNewRecord ? 'Kurti testą' : 'Atnaujinti testą'; ?></h1>

<?php
/** @var TbActiveForm $form */
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'type-form',
    'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'name', array('widgetOptions '=> array('htmlOptions' => array('maxlength' => 45)))); ?>

<div class="form-group">
    <?php echo $form->label($model, 'description'); ?>
    <?php $this->widget(
        'booster.widgets.TbCKEditor',
        array(
            'model' => $model,
            'attribute' => 'description',
            'editorOptions' => array(
                // From basic `build-config.js` minus 'undo', 'clipboard' and 'about'
                //'plugins' => 'basicstyles,toolbar,enterkey,entities,floatingspace,wysiwygarea,indentlist,link,list,dialog,dialogui,button,indent,fakeobjects'
            )
        )
    ); ?>
    <?php echo $form->error($model, 'description'); ?>
</div>

<?php echo $form->textFieldGroup($model, 'time'); ?>

<?php echo $form->textFieldGroup($model, 'order'); ?>

<?php echo $form->checkBoxGroup($model, 'required'); ?>

<?php echo $form->checkBoxGroup($model, 'pagination'); ?>

<div class="form-actions">
    <?php 
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'htmlOptions' => array(
            'name' => 'save'
        ),
        'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
    )); 
    ?>

    <?php 
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'htmlOptions' => array(
            'name' => 'add-questions'
        ),
        'label' => 'Išsaugoti ir pridėti klausimus',
    )); 
    ?>
</div>

<?php $this->endWidget(); ?>
</div>
