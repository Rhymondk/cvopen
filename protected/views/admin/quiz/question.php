<div class="col-md-12 col-lg-12">

<h1>tvarkyti testo klausimus</h1>

<?php 
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'User-form',
    'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->checkboxListGroup(
    $model,
    'question_id',
    array(
        'widgetOptions' => array(
            'data' => CHtml::listdata(Question::model()->findAll(), 'id', 'name')
        ),
    )
); 
?>

<?php 
$this->widget('booster.widgets.TbButton', array(
    'buttonType' => 'submit',
    'context' => 'primary',
    'label' => 'Atnaujinti',
)); 
?>

<?php $this->endWidget(); ?>

</div>
