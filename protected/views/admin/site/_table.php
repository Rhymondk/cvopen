<?php
if (!is_null($model)) {
    $columns[] = array(
        'class' => 'booster.widgets.TbButtonColumn',
        'template' => '{update}{delete}',
        'buttons' => array(
            'update' => array(
                'url' => 'Yii::app()->createUrl("' . $model->tableName() . '/update", array("id" => $data->id))',
            ),
            'delete' => array(
                'url' => 'Yii::app()->createUrl("' . $model->tableName() . '/delete", array("id" => $data->id))',
            )
        )
    );

    $this->widget('booster.widgets.TbGridView',array(
        'id' => 'table-grid',
        'dataProvider' => $model->search(),
        'columns' => $columns
    ));
}
?>
