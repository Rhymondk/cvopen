<div class="row">
    <div class="col-md-12 col-lg-12">
        <h1><?php echo $chart['registered']['title']; ?></h1>
        <div id="chart"></div>
        <?php
            Yii::app()->clientScript->registerScript('chart', "
                var chart = c3.generate({
                    bindto: '#chart',
                    data: {
                        columns: [
                            " . $chart['registered']['data']['registered'] . ",
                            " . $chart['registered']['data']['solved'] . ",
                            " . $chart['registered']['data']['buyed'] . "
                        ],
                        type: 'spline'
                    },
                    axis: {
                        x: {
                            type: 'category',
                            categories: " . $chart['registered']['data']['keys'] . "
                        }
                    }
                });");
        ?>
    </div>
</div>
<?php
    //Charts
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/vendor/c3/c3.min.css');
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/vendor/c3/c3.min.js', CClientScript::POS_END);
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/vendor/d3/d3.min.js', CClientScript::POS_END);
?>