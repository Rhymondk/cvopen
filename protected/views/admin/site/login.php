<?php $this->pageTitle = 'Admin login'; ?>
<div class="row">
    <div class="col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <strong>Login</strong>
            </div>
            <div class="panel-body">
                <?php
                $form = $this->beginWidget(
                    'booster.widgets.TbActiveForm',
                    array(
                        'id' => 'login-form',
                    )
                );

                echo $form->errorSummary($model);
                echo $form->textFieldGroup($model, 'username');
                echo $form->passwordFieldGroup($model, 'password');

                $this->widget(
                    'booster.widgets.TbButton',
                    array(
                        'buttonType' => 'submit', 
                        'label' => 'Prisijungti',
                        'context' => 'primary',
                        'htmlOptions' => array(
                            'class' => 'col-md-12 col-lg-12'
                        )
                    )
                );
                 
                $this->endWidget();
                ?>
            </div>
        </div>
    </div>
</div>