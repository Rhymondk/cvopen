<?php 
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'Worker-form',
    'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'address'); ?>

<?php echo $form->textFieldGroup($model, 'code'); ?>

<?php echo $form->textFieldGroup($model, 'vat_code'); ?>

<?php echo $form->textFieldGroup($model, 'contact_person'); ?>

<?php echo $form->textFieldGroup($model, 'number'); ?>

<div class="form-actions">
    <?php 
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
    )); 
    ?>
</div>

<?php $this->endWidget(); ?>