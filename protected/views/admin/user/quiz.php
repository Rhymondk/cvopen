<?php if ($worker->complete): ?>

    <div class="col-md-12">
        <a class="btn btn-default" href="<?= Yii::app()->createUrl('user/requiz', array('id' => $worker->user_id)) ?>">
            Ištrinti rezultatus
        </a>
    </div>
    <div class="col-md-12 col-lg-12">
        <h1>Keisti rezultatus</h1>
        <?php
        $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
            'id' => 'Results-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'clientOptions' => array(
                'validateOnSubmit' => false,
                'validateOnChange' => false,
                'validateOnType' => true,
                'validationDelay' => 1000,
                'beforeValidateAttribute' => 'js:function(form) {
                    $(".error").hide();
                    $(".form-group").removeClass(".has-error");
                    return true;
                }'
            )
        ));
        ?>
        <?php if (!empty($worker->workerScores)): ?>
            <?php foreach ($worker->workerScores as $score): ?>
                <?php if (!isset($score->type->id)): continue; endif; ?>
                <div class="form-group">
                <?= CHtml::label($score->type->name, 'WorkerScore_' . $score->type->id . '_score'); ?>
                <?=
                $form->textField($score, '[' . $score->type->id . ']score', array(
                    'placeholder' => $score->type->name,
                    'class' => 'form-control'
                ));
                ?>
                <?=
                $form->error($score, '[' . $score->type->id . ']score');
                ?>
                </div>
            <?php endforeach; ?>
        <?php endif;?>
        <?php $this->endWidget(); ?>
    </div>
    <div class="col-md-12 col-lg-12">
        <h1>Bendras Rezultatas: <span id="general-score"><?= $worker->getScore(0) ?></span>%</h1>
    </div>
    <div class="col-md-12 col-lg-12">
        <h1>Bendra informacija</h1>
        <table class="table">
            <tr>
                <td>IQ</td>
                <td><?= $iq->iq; ?> (<?= $iq->name ?>)</td>
            </tr>
            <tr>
                <td>Kiek procentų populiacijos aplenkia (%)</td>
                <td><?= $iq->percent; ?></td>
            </tr>
            <tr>
                <td>Surinktų balų skaičius</td>
                <td><?= $iq->score; ?></td>
            </tr>
        </table>
    </div>
    <div class="col-md-12 col-lg-12">
        <?php foreach ($worker->quizTakens as $quiz): ?>
            <h1><?= $quiz->quiz->name; ?></h1>
            <table class="table">
                <tr>
                    <td>Pabaigtas</td>
                    <td colspan="2"><?= $quiz->complete ? 'Taip' : 'Ne'; ?></td>
                </tr>
                <tr>
                    <td>Pradėtas spręsti</td>
                    <td colspan="2"><?= $quiz->date_start ?></td>
                </tr>
                <tr>
                    <td>Pabaigtas spręsti</td>
                    <td colspan="2"><?= $quiz->date_end ?></td>
                </tr>
                <tr>
                    <td>Išspręstas per</td>
                    <td colspan="2"><?= $quiz->getDiff() ?></td>
                </tr>
                <tr class="line">
                    <td colspan="3"></td>
                </tr>
                <?php foreach ($quiz->getScore() as $id => $score): ?>
                    <tr>
                        <td><?= $types[$id] ?></td>
                        <td colspan="2"><?= $score ?></td>
                    </tr>
                <?php endforeach; ?>
                <tr class="line">
                    <td colspan="3"></td>
                </tr>
                <?php foreach ($quiz->quizTakenAnswers as $answer): ?>
                    <tr>
                        <td><?= $answer->question->name ?></td>
                        <td><?= $answer->answer->name ?></td>
                        <td>
                            <?php foreach ($answer->answer->score as $score) : ?>
                                <span><?= $score->type->name[0]; ?>:<?= $score->value; ?></span>
                            <?php endforeach; ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </table>
        <?php endforeach; ?>
    </div>
<?php else: ?>
    Nėra išspręstų testų
<?php endif; ?>