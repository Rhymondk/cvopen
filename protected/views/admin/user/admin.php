<h1>Tvarkyti vartotojus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('user/create'), array('class'=>'btn-primary btn')); ?>

<?php
$form = $this->beginWidget(
    'booster.widgets.TbActiveForm',
    array(
        'id' => 'inlineForm',
        'type' => 'inline',
        'method' => 'get',
        'action' => Yii::app()->createUrl('user/admin')
    )
);

echo $form->dropDownListGroup(
    $model,
    'role_id',
    array(
        'widgetOptions' => array(
            'data' => array(1 => 'Darbuotojas', 2 => 'Darbdaviai'),
            'htmlOptions' => array('prompt' => 'Vartotojo tipas'),
        )
    )
);

echo $form->textFieldGroup($model, 'name');

$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Ieškoti')
);

$this->endWidget();
?>

<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'user-grid',
    'dataProvider' => $model->search(),
    'ajaxUpdate'=>false,
    'columns' => array(
        array(
            'name' => 'image',
            'type' => 'html',
            'value' => '
                CHtml::image(Image::open(empty($data->image) ? \'images/Web/no-image.png\' : $data->image)->zoomCrop(50, 50)->png(), $data->name, array(
                    "class" => "img-circle",
                    "height" => 50,
                    "width" => 50
                ))
            ',
        ),
        'name',
        array(
            'name' => 'Išsprendė testą',
            'value' => '$data->role_id == 1 ? (isset($data->worker) && $data->worker->complete ? "Išsprendė" : "Neišsprendė") : ""',
        ),
        array(
            'name' => 'role_id',
            'value' => '$data->role->name',
        ),
        array(
            'class' => 'booster.widgets.TbButtonColumn',
            'template' => '{quiz}{selections}{more}{update}{delete}',
            'buttons' => array(
                'selections' => array(
                    'label' => 'Darbdavio atrankos',
                    'url' => 'Yii::app()->createUrl("selection/admin", array("Selection[user_id]" => $data->id))',
                    'icon' => 'list-alt',
                    'visible' => '$data->role_id == 2'
                ),
                'quiz' => array(
                    'label' => 'Testo rezultatai',
                    'url' => 'Yii::app()->createUrl("user/quiz", array("id" => $data->id))',
                    'icon' => 'list',
                    'visible' => '$data->role_id == 1'
                ),
                'more' => array(
                    'label' => 'Papildoma informacija',
                    'url' => 'Yii::app()->createUrl("user/additional", array("id" => $data->id))',
                    'icon'=>'plus',
                ),
            ),
        ),
    ),
)); 
?>
