<div class="col-md-12 col-lg-12">

<h1><?php echo $model->isNewRecord ? 'Kurti vartotoją' : 'Atnaujinti vartotoją'; ?></h1>

<?php 
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'User-form',
    'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->errorSummary($model); ?>

<?php
$this->widget('FileUpload', array(
    'url' => $this->createUrl("user/upload"),
    'model' => $model,
    'form' => $form,
    'attribute' => 'imageFile',
    'attributeUrl' => 'image',
    'id' => 'User-form'
));
?>

<?php echo $form->textFieldGroup($model, 'name'); ?>

<?php echo $form->textFieldGroup($model, 'email'); ?>

<?php
echo $form->passwordFieldGroup($model, 'password', array(
    'widgetOptions' =>
        array('htmlOptions' => array('value' => ''))
    )
);
?>

<?php echo $form->passwordFieldGroup($model, 'repeatPassword'); ?>

<?php 
echo $form->dropDownListGroup($model, 'status', array(
    'widgetOptions' => array(
        'data' => array('Išjungtas', 'Įjungtas'),
    )
));
?>

<?php echo $form->hiddenField($model, 'date_created', array('value' => date("Y-m-d H:i:s"))); ?>

<?php 
if ($model->isNewRecord) {
    echo $form->dropDownListGroup($model, 'role_id', array(
        'widgetOptions' => array(
            'htmlOptions' => array(
                'class' => 'role',
            ),
            'data' => CHtml::listdata(Role::model()->findAll(), 'id', 'name'),
        )
    ));
}
?>

<div class="form-actions">
    <?php 
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'htmlOptions' => array(
            'name' => 'save'
        ),
        'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
    )); 
    ?>

    <?php 
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'htmlOptions' => array(
            'name' => 'create-additional'
        ),
        'label' => 'Išsaugoti ir pildyti papildomą informaciją',
    ));
    ?>
</div>

<?php $this->endWidget(); ?>
</div>
