<div class="form-group col-lg-6 col-md-12">
    <?php
    echo CHtml::label($type->name, '', array(
        'class' => 'col-md-8 col-lg-8 control-label'
    ));
    ?>
    <div class="col-md-4 col-lg-4">
    <?php
    echo CHtml::textField(
        "Score[$type->id][value]", 
        isset($model->score[$counter]->value) ? $model->score[$counter]->value : 0, 
        array(
            'class' => 'form-control text-center'
        )
    );
    ?>
    </div>
</div>