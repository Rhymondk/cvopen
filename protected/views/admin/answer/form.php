<div class="col-md-12 col-lg-12">

    <h1><?php echo $model->isNewRecord ? 'Kurti atsakymą' : 'Atnaujinti atsakymą'; ?></h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'answer-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
            if (!hasError) {
                app.redirect(data.redirect);
            }
        }',
        ),
    ));
    ?>
    <?php echo $form->textFieldGroup($model, 'name', array('widgetOptions' => array('htmlOptions' => array('maxlength' => 255)))); ?>

    <?php
    $this->widget('FileUpload', array(
        'url' => $this->createUrl("answer/upload"),
        'model' => $model,
        'form' => $form,
        'attribute' => 'imageFile',
        'attributeUrl' => 'image',
        'id' => 'answer-form'
    ));
    ?>

    <hr/>
    <?php
    $i = 0;
    foreach ($types as $type) {
        $this->renderPartial('_types', array(
            'type' => $type,
            'model' => $model,
            'counter' => $i++,
        ));
    }
    ?>
    <div class="clearfix"></div>
    <hr/>
    <div class="form-actions">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'htmlOptions' => array(
                'name' => 'save'
            ),
            'label' => 'Išsaugoti',
        ));
        ?>

        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'htmlOptions' => array(
                'name' => 'create-answer'
            ),
            'label' => 'Išsaugoti ir kurti naują',
        ));
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>