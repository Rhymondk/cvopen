<?php 
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
    'id' => 'Worker-form',
    'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'firstname'); ?>

<?php echo $form->textFieldGroup($model, 'lastname'); ?>

<?php echo $form->textFieldGroup($model, 'address'); ?>

<?php echo $form->textFieldGroup($model, 'number'); ?>

<?php 
echo $form->datePickerGroup($model, 'birthday', array(
    'widgetOptions' => array(
        'options' => array(
            'format' => 'yyyy-mm-dd',
        ),
    ),
)); 
?>

<?php 
echo $form->dropDownListGroup($model, 'gender', array(
    'widgetOptions' => array(
        'data' => array('Vyras', 'Moteris'),
        'htmlOptions' => array(
            'prompt' =>'Pasirinkite lytį'
        )
    )
));
?>

<?php
echo $form->dropDownListGroup($model, 'education_id', array(
    'widgetOptions' => array(
        'data' => CHtml::listdata(
            Education::model()->findAll(),
            'id',
            'name'
        ),
        'htmlOptions' => array(
            'prompt' =>'Pasirinkite išsilavinimą'
        )
    )
));
?>

<?php echo $form->textAreaGroup($model, 'comment', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'rows' => 6
        )
    )
)); ?>

<?php echo $form->textAreaGroup($model, 'admin_comment', array(
    'widgetOptions' => array(
        'htmlOptions' => array(
            'rows' => 6
        )
    )
)); ?>

<div class="form-actions">
    <?php 
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
    )); 
    ?>
</div>

<?php $this->endWidget(); ?>