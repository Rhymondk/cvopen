<div class="col-md-12 col-lg-12">

    <h1><?php echo $model->isNewRecord ? 'Kurti tipo aprašymą' : 'Atnaujinti tipo aprašymą'; ?></h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'type-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model, 'name'); ?>

    <?php echo $form->textFieldGroup($model, 'min'); ?>

    <?php echo $form->textFieldGroup($model, 'max'); ?>

    <?php echo $form->textAreaGroup($model, 'description', array(
        'widgetOptions' => array(
            'htmlOptions' => array(
                'rows' => 6
            )
        )
    )); ?>

    <?php echo $form->dropDownListGroup($model, 'type_id', array(
        'widgetOptions' => array(
            'data' => CHtml::listData(Type::model()->findAll(), 'id', 'name')
        )
    )); ?>

    <div class="form-actions">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
