<h1>Tvarkyti tipus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('type/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'type-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
		'name',
		'color',
		'max_score',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
