<h1>Tvarkyti klausimus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('question/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'question-grid',
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
		'name',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
