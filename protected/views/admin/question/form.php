<div class="col-md-12 col-lg-12">

    <h1><?php echo $model->isNewRecord ? 'Kurti klausimą' : 'Atnaujinti klausimą'; ?></h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'question-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => false,
            'validateOnType' => false,
            'afterValidate' => 'js:function(form, data, hasError) {
			if (!hasError) {
				app.redirect(data.redirect);
			}
		}',
        ),
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model, 'name', array('widgetOptions' => array('htmlOptions' => array('maxlength' => 255)))); ?>

    <?php echo $form->dropDownListGroup($model, 'question_type_id',
        array('widgetOptions' =>
            array('data' => CHtml::listData(QuestionType::model()->findAll(), 'id', 'name'))
        )); ?>

    <?php
    $this->widget('FileUpload', array(
        'url' => $this->createUrl("question/upload"),
        'model' => $model,
        'form' => $form,
        'attribute' => 'imageFile',
        'attributeUrl' => 'image',
        'id' => 'question-form'
    ));
    ?>

    <hr/>

    <div class="form-actions">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'htmlOptions' => array(
                'name' => 'save'
            ),
            'label' => 'Išsaugoti',
        ));
        ?>

        <?php if ($model->isNewRecord) { ?>

            <?php
            $this->widget('booster.widgets.TbButton', array(
                'buttonType' => 'submit',
                'context' => 'primary',
                'htmlOptions' => array(
                    'name' => 'create-answer'
                ),
                'label' => 'Išsaugoti ir kurti atsakymą',
            ));
            ?>

        <?php } else { ?>

            <?php
            $this->widget('booster.widgets.TbButton', array(
                'buttonType' => 'link',
                'context' => 'primary',
                'url' => Yii::app()->createUrl('answer/create', array('qid' => $model->id)),
                'label' => 'Kurti atsakymą',
            ));
            ?>

        <?php } ?>
    </div>
    <?php $this->endWidget(); ?>
    <?php
    $this->renderPartial('//site/_table', array(
        'model' => $answerModel,
        'columns' => array('name'),
    ));
    ?>
</div>
