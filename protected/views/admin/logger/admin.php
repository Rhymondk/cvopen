<?php
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'logger-grid',
    'filter' => $model,
    'ajaxUpdate' => false,
    'dataProvider' => $model->search(),
    'columns' => array(
        'name',
        'count',
        'date',
        'status',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{view}{delete}',
        ),
    ),
));
?>
