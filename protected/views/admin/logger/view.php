<?php
    $this->widget('booster.widgets.TbButton',array(
        'context' => 'primary',
        'label' => $model->status ? 'Not fixed' : 'Fixed',
        'buttonType' => 'link',
        'url' => Yii::app()->createUrl('logger/delete', array('id' => $model->id))
    ));
?>
<table>
    <?php foreach ($model->attributes as $name => $atribute): ?>
        <?php if ($name == 'context'): continue; endif; ?>
    <tr>
        <td><?= $model->getAttributeLabel($name) ?></td>
        <td><?=  $atribute ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<pre>
    <code>
        <?= $model->context ?>
    </code>
</pre>
<?php
Yii::app()->clientScript->registerCssFile('https://highlightjs.org/static/styles/monokai_sublime.css');
Yii::app()->clientScript->registerScriptFile('https://highlightjs.org/static/highlight.pack.js');
Yii::app()->clientScript->registerScript('highlight.js', 'hljs.initHighlightingOnLoad();');
?>