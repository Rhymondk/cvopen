<h1>Tvarkyti DUK</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('faq/create'), array('class'=>'btn-primary btn')); ?>
<?php
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'faq-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'name',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
));
?>
