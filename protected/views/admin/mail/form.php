<div class="col-md-12 col-lg-12">

    <h1>Siųsti laišką</h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'mail-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model, 'email', array('widgetOptions ' => array('htmlOptions' => array('maxlength' => 255)))); ?>

    <?php
    echo $form->dropDownListGroup($model, 'template', array(
        'widgetOptions' => array(
            'data' => $model->templates,
        )
    ));
    ?>

    <?php echo $form->checkboxGroup($model, 'force') ?>

    <div class="form-actions">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => 'Siųsti',
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
