<h1>Tvarkyti el. paštus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('mail/create'), array('class'=>'btn-primary btn')); ?>
<?php
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'mail-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'email',
        'template'
    ),
));
?>
