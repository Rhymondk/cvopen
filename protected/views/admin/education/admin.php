<h1>Tvarkyti išsilavinimus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('education/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'type-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
		'name',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
