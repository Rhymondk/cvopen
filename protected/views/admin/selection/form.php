<div class="col-md-12 col-lg-12">
    <h1><?php echo $model->isNewRecord ? 'Kurti CV' : 'Atnaujinti CV'; ?></h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'selection-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php
    echo $form->dropDownListGroup($model, 'user_id', array(
        'widgetOptions' => array(
            'data' => CHtml::listdata(User::model()->findAllByRole(2), 'id', 'name'),
            'htmlOptions' => array(
                'prompt' => 'Pasirinkite darbdavį'
            )
        )
    ));
    ?>

    <?php
    echo $form->dropDownListGroup($model, 'city_id', array(
        'widgetOptions' => array(
            'data' => CHtml::listdata(City::model()->findAll(), 'id', 'name'),
            'htmlOptions' => array(
                'prompt' => 'Pasirinkite miestą'
            )
        )
    ));
    ?>

    <?php
    echo $form->dropDownListGroup($model, 'status_id', array(
        'widgetOptions' => array(
            'data' => CHtml::listdata(Status::model()->findAll(), 'id', 'name'),
        )
    ));
    ?>

    <?php echo $form->textFieldGroup($model, 'position'); ?>

    <?php echo $form->textFieldGroup($model, 'salary'); ?>

    <?php echo $form->datePickerGroup($model, 'date_start', array(
        'widgetOptions' => array(
            'options' => array(
                'format' => 'yyyy-mm-dd'
            )
        )
    )); ?>

    <?php echo $form->datePickerGroup($model, 'date_end', array(
        'widgetOptions' => array(
            'options' => array(
                'format' => 'yyyy-mm-dd'
            )
        )
    )); ?>

    <?php echo $form->textAreaGroup($model, 'description', array(
        'widgetOptions' => array(
            'htmlOptions' => array(
                'rows' => 15
            )
        )
    )); ?>

    <?php echo $form->textAreaGroup($model, 'offer', array(
        'widgetOptions' => array(
            'htmlOptions' => array(
                'rows' => 15
            )
        )
    )); ?>

    <div class="form-actions">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'htmlOptions' => array(
                'name' => 'save'
            ),
            'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
        ));
        ?>
    </div>

    <?php $this->endWidget(); ?>
</div>
