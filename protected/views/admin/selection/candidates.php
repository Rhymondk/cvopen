<?php
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'type-grid',
    'dataProvider' => $selectionCandidate->search(),
    'columns' => array(
        'workerUser.firstname',
        'workerUser.lastname',
        'workerUser.address',
        'workerUser.education.name',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{profile}',
            'buttons' => array(
                'profile' => array(
                    'label' => 'Profilis',
                    'url' => 'Yii::app()->createUrl("selection/candidate", array("id" => $data->id))',
                    'icon' => 'list'
                )
            )
        ),
    ),
));
?>