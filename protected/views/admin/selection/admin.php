<h1>Tvarkyti Atrankas</h1>

<?php
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'type-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'id',
		'position',
        'employer.name',
        array(
            'name' => 'Kandidatavo',
            'value' => '$data->CandidatesCount'
        ),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{plus}{candidate}{update}{delete}',
            'buttons' => array(
                'candidate' => array(
                    'label' => 'Kandidatai',
                    'url' => 'Yii::app()->createUrl("selection/candidates", array("id" => $data->id))',
                    'icon' => 'list'
                ),
                'plus' => array(
                    'label' => 'Testai',
                    'url' => 'Yii::app()->createUrl("selection/additional", array("id" => $data->id))',
                    'icon' => 'plus'
                ),
            )
        ),
    ),
)); 
?>
