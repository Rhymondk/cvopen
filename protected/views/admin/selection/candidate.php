<div class="col-md-12 text-center">
    <?=
    $selectionCandidate->workerUser->user->image ?
    CHtml::image(Image::open($selectionCandidate->workerUser->user->image)->resize(200, 200)->png(),
        $selectionCandidate->workerUser->user->name
    ) : '';
    ?>
</div>
<table class="table">
    <tr>
        <td colspan="2"><h1><?= Yii::t('web', 'Reikalavimai') ?></h1></td>
    </tr>
    <?php foreach ($selectionCandidate->selection->competences as $competence): ?>
        <tr>
            <th><?= $competence->name ?></th>
            <td>
            <?php if (
                in_array(
                    $competence->id,
                    CHtml::listData($selectionCandidate->selectionCandidateCompetence, 'competence_id', 'competence_id'))):
            ?>
                Taip
            <?php else: ?>
                Ne
            <?php endif; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    <tr>
        <td colspan="2"><h1>Profilio informacija</h1></td>
    </tr>
    <tr>
        <th>El. paštas</th>
        <td><?= $selectionCandidate->workerUser->user->email; ?></td>
    </tr>
    <tr>
        <th>Vardas</th>
        <td><?= $selectionCandidate->workerUser->firstname; ?></td>
    </tr>
    <tr>
        <th>Pavardė</th>
        <td><?= $selectionCandidate->workerUser->lastname; ?></td>
    </tr>
    <tr>
        <th>Adresas</th>
        <td><?= $selectionCandidate->workerUser->address; ?></td>
    </tr>
    <tr>
        <th>Gimimo data</th>
        <td><?= $selectionCandidate->workerUser->birthday; ?></td>
    </tr>
    <tr>
        <th>Išsilavinimas</th>

        <td>
            <?php if (isset($selectionCandidate->workerUser->education)): ?>
                <?= $selectionCandidate->workerUser->education->name; ?>
            <?php endif; ?>
        </td>

    </tr>
    <tr>
        <th>Lytis</th>
        <td><?= $selectionCandidate->workerUser->gender->name; ?></td>
    </tr>
    <tr>
        <th>Profilio komentaras</th>
        <td><?= $selectionCandidate->workerUser->comment; ?></td>
    </tr>
    <tr>
        <th>Pageidaujamas atlyginimas</th>
        <td><?= $selectionCandidate->workerUser->salary; ?></td>
    </tr>
    <tr>
        <th>IP adresas</th>
        <td><?= $selectionCandidate->workerUser->user->ip; ?></td>
    </tr>
    <tr>
        <th>Paskyra sukurta</th>
        <td><?= $selectionCandidate->workerUser->user->date_created; ?></td>
    </tr>
    <tr>
        <th>CV</th>
        <td>
        <?php if (!empty($selectionCandidate->workerUser->cv)): ?>
            <a href="<?= $selectionCandidate->workerUser->cvLink; ?>" target="_blank">Siųstis</a>
        <?php endif; ?>
        </td>
    </tr>
    <tr>
        <td colspan="2"><h1>Testų rezultatai</h1></td>
    </tr>
    <tr>
        <th>Tipas</th>
        <td>Taškai</td>
    </tr>
    <?php if ($selectionCandidate->workerUser->complete): ?>
        <?php foreach ($selectionCandidate->workerUser->getScore() as $type_id => $score): ?>
            <?php if (!isset($types[$type_id])): ?>
                <?php continue; ?>
            <?php endif; ?>
            <tr>
                <th><?= $types[$type_id]; ?></th>
                <td><?= $score; ?></td>
            </tr>
        <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="2">Neišspręsti testai</td>
        </tr>
    <?php endif; ?>
</table>