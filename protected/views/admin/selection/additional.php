<div class="col-md-12 col-lg-12">

    <h1>Pridėti testus</h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'add-quiz-form',
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->checkboxListGroup(
        $selectionQuiz,
        'quiz_id',
        array(
            'widgetOptions' => array(
                'data' => CHtml::listdata($quizes, 'id', 'name')
            ),
        )
    );
    ?>

<!--    <h1>Pridėti tipus</h1>-->
<!---->
<!--    --><?php //echo $form->checkboxListGroup(
//        $selectionType,
//        'type_id',
//        array(
//            'widgetOptions' => array(
//                'data' => CHtml::listdata($types, 'id', 'name')
//            ),
//        )
//    );
//    ?>

    <?php
    $this->widget('booster.widgets.TbButton', array(
        'buttonType' => 'submit',
        'context' => 'primary',
        'label' => 'Atnaujinti',
    ));
    ?>

    <?php $this->endWidget(); ?>

</div>
