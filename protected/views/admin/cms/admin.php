<h1>Tvarkyti statinius puslapius</h1>

<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'cms-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
		'id',
		'name',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
