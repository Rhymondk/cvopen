<div class="col-md-12 col-lg-12">

<h1><?php echo $model->isNewRecord ? 'Kurti statinį puslapį' : 'Atnaujinti statinį puslapį'; ?></h1>

<?php 
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'cms-form',
	'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'name', array('widgetOptions '=> array('htmlOptions' => array('maxlength' => 255)))); ?>

<div class="form-group">
    <?php echo $form->label($model, 'description'); ?>
    <?php $this->widget(
        'booster.widgets.TbCKEditor',
        array(
            'model' => $model,
            'attribute' => 'description',
            'editorOptions' => array(
                // From basic `build-config.js` minus 'undo', 'clipboard' and 'about'
                //'plugins' => 'basicstyles,toolbar,enterkey,entities,floatingspace,wysiwygarea,indentlist,link,list,dialog,dialogui,button,indent,fakeobjects'
            )
        )
    ); ?>
    <?php echo $form->error($model, 'description'); ?>
</div>

<div class="form-actions">
	<?php 
	$this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
	));
	?>
</div>

<?php $this->endWidget(); ?>
</div>
