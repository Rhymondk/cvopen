<div class="col-md-12 col-lg-12">

<h1><?php echo $model->isNewRecord ? 'Kurti klientą' : 'Atnaujinti klientą'; ?></h1>

<?php
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'Client-form',
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
	'enableAjaxValidation' => false,
));
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

<?php
$this->widget('FileUpload', array(
    'url' => $this->createUrl("client/upload"),
    'model' => $model,
    'form' => $form,
    'attribute' => 'clientLogo',
    'attributeUrl' => 'image',
    'id' => 'Client-form'

));
?>

<?php echo $form->textFieldGroup($model,'link',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5',)))); ?>

<?php
echo $form->dropDownListGroup(
	$model,
	'status',
	array(
		'widgetOptions' => array(
			'data' => array('Ne', 'Taip'),
		)
	)
);
?>

<div class="form-actions">
	<?php
	$this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord ? 'Kurti' : 'Išsaugoti',
	));
	?>
</div>
<?php $this->endWidget(); ?>
</div>
