<h1>Tvarkyti klientus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('client/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'client-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'image' => array(
            'name' => 'logo',
            'type' => 'html',
            'value' => '
            $data->image ?
            CHtml::image(Image::open($data->image)->resize(80, 80)->png(), $data->name, array(
                "class" => "img-circle"
            )) : "";',
        ),
		'name',
		'status' => array(
            'name' => 'Rodyti',
            'value' => '$data->status ? \'Taip\' : \'Ne\'',
        ),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
