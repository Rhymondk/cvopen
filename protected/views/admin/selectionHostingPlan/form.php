<div class="col-md-12 col-lg-12">

    <h1><?php echo $model->isNewRecord ? 'Kurti atrankos talpinimąo planą' : 'Atnaujinti atrankos talpinimo planą'; ?></h1>

    <?php
    $form = $this->beginWidget('booster.widgets.TbActiveForm', array(
        'id' => 'SelectionHosting-form',
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
        'enableAjaxValidation' => false,
    ));
    ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->textFieldGroup($model,'name',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5','maxlength'=>255)))); ?>

    <?php
    $this->widget('FileUpload', array(
        'url' => $this->createUrl("selectionHostingPlan/upload"),
        'model' => $model,
        'form' => $form,
        'attribute' => 'imageFile',
        'attributeUrl' => 'image',
        'id' => 'SelectionHosting-form'

    ));
    ?>

    <?php echo $form->textFieldGroup($model,'price',array('widgetOptions'=>array('htmlOptions'=>array('class'=>'span5')))); ?>

    <div class="form-actions">
        <?php
        $this->widget('booster.widgets.TbButton', array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'label' => $model->isNewRecord ? 'Kurti' : 'Išsaugoti',
        ));
        ?>
    </div>
    <?php $this->endWidget(); ?>
</div>
