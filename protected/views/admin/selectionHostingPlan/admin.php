<h1>Tvarkyti atrankų talpinimo planus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('selectionHostingPlan/create'), array('class'=>'btn-primary btn')); ?>
<?php
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'selectionHosting-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
        'image' => array(
            'name' => 'Atvaizdas',
            'type' => 'html',
            'value' => '
            $data->image ?
            CHtml::image(Image::open($data->image)->resize(80, 80)->png(), $data->name, array(
                "class" => "img-circle"
            )) : "";',
        ),
        'name',
        'price',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
));
?>
