<h1>Tvarkyti miestus</h1>

<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('city/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'type-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
		'name',
        'exclude' => array(
            'name' => 'Išskirti',
            'value' => '$data->exclude ? "Taip" : "Ne"',
        ),
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
