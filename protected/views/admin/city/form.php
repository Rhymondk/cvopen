<div class="col-md-12 col-lg-12">

<h1><?php echo $model->isNewRecord ? 'Kurti miestą' : 'Atnaujinti miestą'; ?></h1>

<?php 
$form = $this->beginWidget('booster.widgets.TbActiveForm', array(
	'id' => 'education-form',
	'enableAjaxValidation' => false,
)); 
?>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldGroup($model, 'name', array('widgetOptions '=> array('htmlOptions' => array('maxlength' => 255)))); ?>

<div class="form-actions">
	<?php 
	$this->widget('booster.widgets.TbButton', array(
		'buttonType' => 'submit',
		'context' => 'primary',
		'label' => $model->isNewRecord ? 'Kurti' : 'Atnaujinti',
	));
	?>
</div>

<?php $this->endWidget(); ?>
</div>
