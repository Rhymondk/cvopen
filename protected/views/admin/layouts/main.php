<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="en"/>
    <?php
    Yii::app()->getClientScript()->registerCoreScript('jquery');

    //CSS
    Yii::app()->clientScript->registerCssFile(Yii::app()->request->baseUrl . '/css/admin/style.min.css');

    //Js
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/js/admin/script.min.js', CClientScript::POS_END);

    ?>
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<body
    onload="app.load()"
    data-controller="<?= ucfirst(strtolower(Yii::app()->controller->id)) ?>"
    data-action="<?= ucfirst(strtolower(Yii::app()->controller->action->id)) ?>">
<?php
$this->widget(
    'booster.widgets.TbNavbar',
    array(
        'brand' => CHtml::image(Image::open('images/Web/logo.png')->scaleResize(150, 45)->png(), 'cvopen'),
        'fixed' => false,
        'fluid' => true,
        'items' => array(
            array(
                'class' => 'booster.widgets.TbMenu',
                'type' => 'navbar',
                'items' => array(
                    array('label' => 'Klausimai', 'url' => array('question/admin')),
                    array('label' => 'Testai', 'url' => array('quiz/admin')),
                    array('label' => 'Vartotojai', 'url' => array('user/admin')),
                    array('label' => 'Atrankos', 'url' => array('selection/admin')),
                    array('label' => 'Nustatymai', 'url' => array('setting/admin')),
                    array('label' => 'Autoreply siuntimas', 'url' => array('mail/admin')),
                    array('label' => 'Kita', 'items' => array(
                        array('label' => 'Tipai', 'url' => array('type/admin')),
                        array('label' => 'Tipų aprašymai', 'url' => array('typeDescription/admin')),
                        array('label' => 'Išsilavinimas', 'url' => array('education/admin')),
                        array('label' => 'Miestai', 'url' => array('city/admin')),
                        array('label' => 'Klientai', 'url' => array('client/admin')),
                        array('label' => 'Statiniai puslapiai', 'url' => array('cms/admin')),
                        array('label' => 'DUK', 'url' => array('faq/admin')),
                        array('label' => 'Atrankų talpinimas', 'url' => array('selectionHostingPlan/admin')),
                        array('label' => 'Klaidos', 'url' => array('logger/admin'))
                    )),
                ),
            ),
            array(
                'class' => 'booster.widgets.TbMenu',
                'htmlOptions' => array('class' => 'pull-right'),
                'type' => 'navbar',
                'items' => array(
                    array('label' => 'Puslapis', 'url' => Yii::app()->request->hostInfo),
                    array('label' => 'Atsijungti', 'url' => array('site/logout')),
                )
            )
        ),
    )
);
?>
<div class="container" id="page">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <?php echo $content; ?>
        </div>
    </div>
</div>
</body>
</html>
