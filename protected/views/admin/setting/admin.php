<h1>Tvarkyti nustatymus</h1>
<?php echo CHtml::link('Kurti naują', Yii::app()->createUrl('setting/create'), array('class'=>'btn-primary btn')); ?>
<?php 
$this->widget('booster.widgets.TbGridView',array(
    'id' => 'setting-grid',
    'dataProvider' => $model->search(),
    'columns' => array(
		'key',
		'value',
        array(
            'class'=>'booster.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
        ),
    ),
)); 
?>
