<?php $this->imageBlock = '/images/Web/cvopen-you-new.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kandidate,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?=
    Yii::t('web', 'Dėkojame, kad domitės CV OPEN darbo pasiūlymais. Norime priminti, kad Jūs vis dar nesate atlikęs mąstymo gebėjimų (intelekto) ir asmeninių savybių testų. Deja, be šios svarbios informacijos mes negalime vertinti Jūsų kandidatūros į pasirinktą darbo poziciją.',
        array(
            'CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)),
            'www.cvopen.lt' => CHtml::link('www.cvopen.lt', Yii::app()->getBaseUrl(true))
        )
    );
    ?>
</div>