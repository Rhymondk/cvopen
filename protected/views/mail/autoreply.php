<?php $this->imageBlock = '/images/Web/cvopen-you.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kandidate,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?=
    Yii::t('web', 'Dėkojame, kad domitės mūsų darbo pasiūlymais. Prašome Jūsų registruotis CV OPEN sistemoje www.cvopen.lt ir įkelti savo CV. Kitu atveju Jūsų kandidatūros įvertinti negalėsime.',
        array(
            'CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)),
            'www.cvopen.lt' => CHtml::link('www.cvopen.lt', Yii::app()->getBaseUrl(true))
        )
    );
    ?>
</div>