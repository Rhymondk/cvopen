<h1><?= Yii::t('web', 'Slaptažodžio keitimas'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?= Yii::t('web', 'Norėdami pakeisti slaptažodį paspauskite nuorodą žemiau'); ?>
    <?=
    CHtml::link(
        Yii::app()->createAbsoluteUrl('user/changePassword', array('token' => $token)),
        Yii::app()->createAbsoluteUrl('user/changePassword', array('token' => $token))
    );
    ?>
</div>