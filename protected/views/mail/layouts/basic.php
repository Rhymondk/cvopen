<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<meta name="viewport" content="width=device-width"/>
<style>
/**********************************************
* Ink v1.0.5 - Copyright 2013 ZURB Inc        *
**********************************************/

/* Client-specific Styles & Reset */
@font-face {
    font-family: 'Open Sans';
    font-style: normal;
    font-weight: 400;
    src: local('Open Sans'), local('OpenSans'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3bO3LdcAZYWl9Si6vvxL-qU.woff) format('woff');
}

#outlook a {
    padding: 0;
}

body {
    width: 100% !important;
    min-width: 100%;
    -webkit-text-size-adjust: 100%;
    -ms-text-size-adjust: 100%;
    margin: 0;
    padding: 0;
    text-align: center;
}

img {
    outline: none;
    text-decoration: none;
    -ms-interpolation-mode: bicubic;
    width: auto;
    max-width: 100%;
    clear: both;
}

center {
    width: 100%;
    min-width: 580px;
}

a img {
    border: none;
}

p {
    margin: 0 0 0 10px;
}

table {
    border-spacing: 0;
    border-collapse: collapse;
}

td {
    word-break: break-word;
    -webkit-hyphens: auto;
    -moz-hyphens: auto;
    hyphens: auto;
    border-collapse: collapse !important;
}

table, tr, td {
    padding: 0;
    vertical-align: top;
}

hr {
    color: #d9d9d9;
    background-color: #d9d9d9;
    height: 1px;
    border: none;
}

/* Alignment & Visibility Classes */

table.center, td.center {
    text-align: center;
}

h1.center,
h2.center,
h3.center,
h4.center,
h5.center,
h6.center {
    text-align: center;
}

span.center {
    display: block;
    width: 100%;
    text-align: center;
}

img.center {
    margin: 0 auto;
    float: none;
}

.show-for-small,
.hide-for-desktop {
    display: none;
}

/* Typography */

body, table.body, h1, h2, h3, h4, h5, h6, p, td {
    color: #222222;
    font-family: "Open sans", "Arial", sans-serif;
    font-weight: normal;
    padding: 0;
    margin: 0;
    line-height: 1.5;
}

h1, h2, h3, h4, h5, h6 {
    word-break: normal;
}

h2 {
    font-size: 36px;
}

h3 {
    font-size: 32px;
}

h4 {
    font-size: 28px;
}

h5 {
    font-size: 24px;
}

h6 {
    font-size: 20px;
}

body, table.body, p, td {
    font-size: 14px;
}

p {
    margin-bottom: 10px;
}

small {
    font-size: 10px;
}

a {
    color: #00aaff !important;
    text-decoration: none;
    white-space: nowrap;
}

a:hover {
    text-decoration: underline;
}

a:active {
    color: #00aaff !important;
}

a:visited {
    color: #00aaff !important;
}

h1 {
    color: #3e3e3e;
    font-size: 18px;
    font-weight: 700;
    margin-top: 20px;
    padding: 20px 0;
}
</style>
</head>
<body style="background-color: #ececec;padding: 20px 0;" bgcolor="#ececec">
<div style="margin: 0 auto;width: 580px;">
    <div style="background-color: #fff;border-bottom: 1px solid #ccc;margin-top: 20px;text-align: center;padding: 20px 0;">
        <div>
            <?= CHtml::image(Yii::app()->getBaseUrl(true) . '/images/Web/logo-print.png', 'cvopen', array(
                'class' => 'center'
            )); ?>
        </div>
        <?php if (!empty($this->imageBlock)): ?>
            <div style="height: 238px;width: 580px;margin-top: 15px;">
                <?= CHtml::image(Yii::app()->getBaseUrl(true) . $this->imageBlock, 'image-block'); ?>
            </div>
        <?php endif; ?>
        <div style="padding: 0 30px; line-height: 1.5">
            <?= $content; ?>
            <div style="margin: 30px 50px 0 50px;padding-top: 30px;padding-bottom: 10px;border-top: 1px solid #d4d4d4;font-size: 12px;line-height: 1.2;">
                <div style="color: #9c9c9c;"><?= Yii::t('web', 'Kilus klausimams su mumis galite susisiekti') ?></div>
                <div>
                    <a href="mailto:<?= Yii::app()->params['mail_email'] ?>">
                        <?= Yii::app()->params['mail_email'] ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div style="color: #a9a9a9;font-size: 12px;margin-top: 20px;text-align: center">
        <?= Yii::t('web', ' © 2015 CV OPEN'); ?>
    </div>
</div>
</body>
</html>