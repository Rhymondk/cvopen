<?php $this->imageBlock = '/images/Web/mail-hands.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kliente,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?=
    Yii::t('web', 'Dėkojame kad užsiregistravote CV OPEN sistemoje. Prisijunkite ir naudokitės CV OPEN kandidato atrankos ir testavimo
        sistema.', array('CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)))
    );
    ?>
</div>