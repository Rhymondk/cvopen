<h1><?= Yii::t('web', 'Naujas kandidatas'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?=
    Yii::t('web', 'Jūsų atrankoje "{selection}", testus atliko naujas kandidatas. Išsamesnį aprašymą rasite savo CV OPEN paskyroje', array(
            '{selection}' => $selection
        )
    );
    ?>
</div>