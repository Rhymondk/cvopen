<?php $this->imageBlock = '/images/Web/write-note.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kandidate,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?=
    Yii::t('web', 'Darbuotojų vertinimo ir darbo paieškos sistemoje CV OPEN atlikote mąstymo gebėjimo (intelekto) ir asmeninių savybių testus. Rezultatus rasite el. laiško priede.',
        array('CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)))
    );
    ?>
</div>