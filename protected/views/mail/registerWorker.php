<?php $this->imageBlock = '/images/Web/mail-hands.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kandidate,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;">
    <?=
    Yii::t('web', 'Dėkojame kad užsiregistravote CV OPEN sistemoje. Prisijunkite ir naudokitės CV OPEN darbo paieškos ir testavimo sistema.',
        array('CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)))
    );
    ?>
</div>