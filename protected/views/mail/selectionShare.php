<?php $this->imageBlock = '/images/Web/cvopen-you-new.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kandidate,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400; text-align: left">
    <?=
    Yii::t('web', 'Dėkojame, kad domitės {selection} pozicija - {position}. Atranką vykdo personalo agentūra CV OPEN.',
        array(
            'CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)),
            '{selection}' => $selection->employer->name,
            '{position}' => $selection->position
        )
    );
    ?>
</div>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400; text-align: left">
    <?=
    Yii::t('web', 'Jūs buvote atrinktas tolimesniam atrankos etapui. Prašome Jūsų iki {date} d. atlikti šiuos žingsnius:',
        array(
            '{date}' => date('Y m d', strtotime('+7 days')),
        )
    );
    ?>
</div>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400; text-align: left">
    <?=
    Yii::t('web', '1. Prisiregistruoti CV OPEN sistemoje. Nuoroda: {selection}',
        array(
            'CV OPEN' => CHtml::link('CV OPEN', Yii::app()->getBaseUrl(true)),
            '{selection}' => CHtml::link($selection->employer->name, Yii::app()->createAbsoluteUrl('worker/view', array(
                'id' => $selection->id
            ))),
        )
    );
    ?>
    <br />
    <?=  Yii::t('web', '2. Įkelti savo gyvenimo aprašymą ir pagrindinę informaciją apie save.') ?>
    <br />
    <?=  Yii::t('web', '3. Pažymėti reikalavimus, kuriuos atitinkate.') ?>
    <br />
    <?=  Yii::t('web', '4. Atlikti protinių gebėjimų ir asmeninių savybių testus.') ?>
</div>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400; text-align: left">
    <?= Yii::t('web', 'Neatlikus šių žingsnių ar nutraukus testų sprendimą, Jūsų kandidatūros vertinti negalėsime. Linkime sėkmės!') ?>
</div>
