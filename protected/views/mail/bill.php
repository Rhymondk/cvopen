<?php $this->imageBlock = '/images/Web/write-note.png'; ?>
<h1><?= Yii::t('web', 'Gerbiamas kliente,'); ?></h1>
<div style="color: #3e3e3e;font-size: 15px;margin-top: 20px;font-weight: 400;line-height: 1.5;">
    <?= Yii::t('web', 'Dėkojame, kad užsakėte CV OPEN teikiamas personalo atrankos paslaugas. Prisegtas dokumentas yra sąskaita išankstiniam apmokėjimui. Paslaugas aktyvuosime gavę apmokėjimą. Mokėdami banko pavedimu būtinai nurodykite užsakymo numerį <b>{n}</b>', $selectionId); ?>
</div>