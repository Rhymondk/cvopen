<?php
return array(
    'sourcePath'=>dirname(__FILE__).'/..',
    'messagePath'=>dirname(__FILE__).'/../messages',
    'languages'=>array('lt'),
    'fileTypes'=>array('php'),
    'overwrite'=>true,
    'exclude'=>array(
        '.svn',
        '.gitignore',
        'yiilite.php',
        'yiit.php',
        '/messages',
        '/vendor',
        '/web/js',
    ),
);
