<?php
return array(
    'registerEmployer' => 'Sveikiname užsiregistravus!',
    'registerWorker' => 'Sveikiname užsiregistravus!',
    'printQuiz' => 'Testo rezultatai',
    'autoreply' => 'Dėl atrankos',
    'reminder' => 'dėl CV OPEN atrankos',
    'bill' => 'Sąskaita už pasirinkitas paslaugas',
    'changePassword' => 'Slaptažodžio keitimas',
    'candidate' => 'Naujas kandidatas',
    'selectionShare' => 'CV OPEN atranka',
);
?>
