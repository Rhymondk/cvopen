<?php

/**
 * This is the model class for table "iq_table".
 *
 * The followings are the available columns in table 'iq_table':
 * @property integer $id
 * @property integer $score
 * @property string $iq
 * @property double $percent
 */
class IqTable extends CActiveRecord
{
    public $name;

    public function afterFind()
    {
        $this->name = $this->getName();
    }

    public function getName()
    {
        $result = '';
        switch (true) {
            case $this->score <= 4:
                $result = Yii::t('web', 'Labai žemas');
                break;
            case $this->score <= 8:
                $result = Yii::t('web', 'Žemas');
                break;
            case $this->score <= 13:
                $result = Yii::t('web', 'Žemesnis nei vidutinis');
                break;
            case $this->score <= 19:
                $result = Yii::t('web', 'Vidutinis');
                break;
            case $this->score == 20:
                $result = Yii::t('web', 'Aukštesnis nei vidutinis');
                break;
            case $this->score == 21:
                $result = Yii::t('web', 'Aukštas');
                break;
            case $this->score >= 22:
                $result = Yii::t('web', 'Labai aukštas');
                break;
        }

        return $result;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'iq_table';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('score, iq, percent', 'required'),
            array('score', 'numerical', 'integerOnly' => true),
            array('percent', 'numerical'),
            array('iq', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, score, iq, percent', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'score' => 'Score',
            'iq' => 'Iq',
            'percent' => 'Percent',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('score', $this->score);
        $criteria->compare('iq', $this->iq, true);
        $criteria->compare('percent', $this->percent);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return IqTable the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
