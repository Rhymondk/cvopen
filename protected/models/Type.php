<?php

/**
 * This is the model class for table "type".
 *
 * The followings are the available columns in table 'type':
 * @property integer $id
 * @property string $name
 * @property string $color
 * @property integer $max_score
 *
 * The followings are the available model relations:
 * @property Score[] $scores
 * @property TypeDescription[] $typeDescriptions
 */
class Type extends CActiveRecord
{

    const TEST_TYPE = 8;
    public $ignore = array(self::TEST_TYPE);

    public function findAll($condition = '', $params = array())
    {
        $types = parent::findAll($condition, $params);
        $admin = isset(Yii::app()->user->isAdmin) && Yii::app()->user->isAdmin;

        if (!empty($this->ignore) && !$admin) {

            if ($types) {

                foreach ($types as $key => $type) {

                    if (in_array($type->id, $this->ignore)) {
                        unset($types[$key]);
                    }
                }
            }
        }

        return $types;
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'type';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('max_score', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 45),
            array('color', 'length', 'max' => 7),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, color, max_score', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'scores' => array(self::HAS_MANY, 'Score', 'type_id'),
            'typeDescriptions' => array(self::HAS_MANY, 'TypeDescription', 'type_id'),
            'workerScores' => array(self::HAS_MANY, 'WorkerScore', 'type_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('web', '#'),
            'name' => Yii::t('web', 'Pavadinimas'),
            'color' => Yii::t('web', 'Spalva'),
            'max_score' => Yii::t('web', 'Didžiausias rezultatas'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('color', $this->color, true);
        $criteria->compare('max_score', $this->max_score);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
        ));
    }

    public function findAllWithIgnored($condition = '', $params = array())
    {
        $tempIgnored = $this->ignore;
        $this->ignore = array();
        $types = $this->findAll($condition, $params);
        $this->ignore = $tempIgnored;

        return $types;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Type the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
