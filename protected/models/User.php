<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $date_created
 * @property string $last_login
 * @property integer $role
 * @property string $ip
 * @property integer $status
 * @property integer $role_id
 *
 */
class User extends CActiveRecord
{
    public $imageFile;
    public $repeatPassword;
    public $currentPassword;
    public $rules1; //Worker
    public $rules2; //Employer

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            //Front-insert
            array(
                'rules1, rules2',
                'compare',
                'compareValue' => true,
                'message' => Yii::t('web', 'Turite sutikti su taisyklėmis'),
                'on' => 'front-insert',
            ),
            array('name, email, password, date_created, repeatPassword', 'required', 'on' => 'front-insert'),
            array('email', 'unique', 'except' => 'recover'),
//            array('ip',
//                'unique',
//                'message' => Yii::t('web', 'Registracija jau buvo atlikta naudojantis šiuo kompiuteriu'),
//                'on' => 'front-insert'
//            ),
            array('name, email, password, date_created, repeatPassword', 'required', 'on' => 'insert'),
            array('email', 'required', 'on' => 'social-insert'),
            array('name, email', 'required', 'on' => 'update'),
            array('role_id, status', 'numerical', 'integerOnly' => true),
            array('email', 'email'),
            array('repeatPassword', 'compare', 'compareAttribute' => 'password'),
            array('name, email', 'length', 'max' => 255, 'min' => 3),
            array('password, repeatPassword', 'length', 'max' => 255, 'min' => 6),
            array('password, repeatPassword, currentPassword', 'required', 'on' => 'change-password'),
            array('password, repeatPassword', 'required', 'on' => 'recover-password'),
            array('ip', 'length', 'max' => 150),
            array(
                'imageFile',
                'file',
                'maxSize' => 1024 * 1024 * (int) Yii::app()->params['max_image_size'],
                'types' => Yii::app()->params['valid_image_format'],
                'allowEmpty' => true,
            ),
            array('email', 'required', 'on' => 'recover'),
            array('email', 'email', 'on' => 'recover'),
            array('imageUrl, repeatPassword, currentPassword, password', 'safe'),
            array('currentPassword', 'checkPassword', 'on' => 'change-password'),

        );
    }

    public function checkPassword($attribute, $params)
    {
        $user = User::model()->findByPk($this->id);
        if (!CPasswordHelper::verifyPassword($this->currentPassword, $user->password)) {
            $this->addError($attribute, Yii::t('web', 'Blogai įvestas dabartinis slaptažodis'));
        }
    }
    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'employer' => array(self::HAS_ONE, 'Employer', 'user_id'),
            'role' => array(self::BELONGS_TO, 'Role', 'role_id'),
            'worker' => array(self::HAS_ONE, 'Worker', 'user_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('web', '#'),
            'name' => Yii::t('web', 'Vartotojo vardas arba įmonės pavadinimas'),
            'email' => Yii::t('web', 'El. paštas'),
            'password' => Yii::t('web', 'Slaptažodis'),
            'repeatPassword' => Yii::t('web', 'Pakartoti slaptažodį'),
            'date_created' => Yii::t('web', 'Sukurimo data'),
            'role_id' => Yii::t('web', 'Vartotojo tipas'),
            'ip' => Yii::t('web', 'Ip adresas'),
            'status' => Yii::t('web', 'Statusas'),
            'imageUrl' => Yii::t('web', 'Nuotrauka'),
            'currentPassword' => Yii::t('web', 'Dabartinis slaptažodis'),
        );
    }

    public function findByEmail($email)
    {
        return self::model()->findByAttributes(array('email' => $email));
    }

    public function findAllByRole($role_id)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "role_id = :role_id";
        $criteria->params = array(':role_id' => $role_id);
        return User::model()->findAll($criteria);
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return User the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('password', $this->password, true);
        $criteria->compare('date_created', $this->date_created, true);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('status', $this->status);
        $criteria->compare('role_id', $this->role_id);

        $criteria->order = 'id DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
        ));
    }
}
