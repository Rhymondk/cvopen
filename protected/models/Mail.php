<?php

/**
 * This is the model class for table "mail".
 *
 * The followings are the available columns in table 'mail':
 * @property integer $id
 * @property string $email
 * @property string $template
 * @property integer $force
 */
class Mail extends CActiveRecord
{
    /**
     * Forcing mail send
     * @var int
     */
    public $force = 1;

    /**
     * Sends mail after save
     */
    public function afterSave()
    {
        Mailer::app()->sendTemplate($this->email, $this->template);
        sleep(1);
    }

    /**
     * Get templates
     */
    public function getTemplates()
    {
        return array(
            'autoreply' => 'FORWARD atsakymas iš cv@cvopen.lt',
            'reminder' => 'Priminimas dėl testo'
        );
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'mail';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('email, template, force', 'required'),
            array('email', 'forcing'),
            array('email, template', 'length', 'max' => 255),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, email, template, force', 'safe', 'on' => 'search'),
        );
    }

    public function forcing($attribute, $params)
    {
        if (!$this->force) {
            $exist = Mail::model()->exists('email = :email AND template = :template', array(
                ':email' => $this->email,
                ':template' => $this->template
            ));

            if ($exist) {
                $this->addError($attribute, 'Šiam el. paštui jau buvo išsiųtas laiškas, naudokite parametrą "Priverstinai"');
            }
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array();
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('web', '#'),
            'email' => Yii::t('web', 'El. paštas'),
            'template' => Yii::t('web', 'Šablonas'),
            'force' => Yii::t('web', 'Priverstinai'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('template', $this->template, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Mail the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
