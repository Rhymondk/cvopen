<?php

/**
 * This is the model class for table "employer".
 *
 * The followings are the available columns in table 'employer':
 * @property integer $user_id
 * @property string $address
 * @property string $code
 * @property string $vat_code
 * @property string $contact_person
 * @property string $number
 *
 * The followings are the available model relations:
 * @property User $user
 * @property Selection[] $selections
 */
class Employer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'employer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id', 'numerical', 'integerOnly'=>true),
			array('address, contact_person', 'length', 'max'=>255),
			array('code, vat_code', 'length', 'max'=>50),
			array('number', 'length', 'max'=>15),
            array('address, contact_person, vat_code, code, number', 'required', 'on' => 'reviewSelection'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('user_id, address, code, vat_code, contact_person, number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'selections' => array(self::HAS_ONE, 'Selection', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => Yii::t('web', '#'),
			'address' => Yii::t('web', 'Adresas'),
			'code' => Yii::t('web', 'Įmonės kodas'),
			'vat_code' => Yii::t('web', 'PVM kodas'),
			'contact_person' => Yii::t('web', 'Kontaktinis asmuo'),
			'number' => Yii::t('web', 'Telefono numeris'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('vat_code',$this->vat_code,true);
		$criteria->compare('contact_person',$this->contact_person,true);
		$criteria->compare('number',$this->number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Employer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
