<?php

/**
 * This is the model class for table "worker_score".
 *
 * The followings are the available columns in table 'worker_score':
 * @property integer $id
 * @property integer $type_id
 * @property integer $worker_user_id
 * @property string $score
 *
 * The followings are the available model relations:
 * @property Type $type
 * @property Worker $workerUser
 */
class WorkerScore extends CActiveRecord
{

    public function beforeSave()
    {
        WorkerScore::model()->deleteAllByAttributes(array(
            'worker_user_id' => $this->worker_user_id,
            'type_id' => $this->type_id
        ));

        return parent::beforeSave();
    }
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'worker_score';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('type_id, worker_user_id, score', 'required'),
			array('type_id, worker_user_id, score', 'numerical', 'integerOnly'=>true),
			array('score', 'length', 'max' => 45),
			array('score', 'maxScore'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, type_id, worker_user_id, score', 'safe', 'on'=>'search'),
		);
	}

    /**
     * Check score if not above type max score
     * @param $attribute
     * @param $params
     */
    public function maxScore($attribute, $params)
    {
        $type = Type::model()->findByPk($this->type_id);

        if ($type->max_score < $this->score) {
            $this->addError($attribute, Yii::t('web', 'Rezultatas negali viršyti: ' . $type->max_score));
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type' => array(self::BELONGS_TO, 'Type', 'type_id'),
			'workerUser' => array(self::BELONGS_TO, 'Worker', 'worker_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => Yii::t('web', '#'),
			'type_id' => Yii::t('web', 'Tipas'),
			'worker_user_id' => Yii::t('web', 'Kandidatas'),
			'score' => Yii::t('web', 'Tipo rezultatas'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('type_id',$this->type_id);
		$criteria->compare('worker_user_id',$this->worker_user_id);
		$criteria->compare('score',$this->score,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkerScore the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Generate score via worker id
     */
    public function generateScore($worker_user_id, $sum = array())
    {
        $worker = Worker::model()->findByPk($worker_user_id);

        if (is_null($worker)) {
            throw new CHttpException('Tokio vartotojo nėra, arba nebaigė testų');
        }

        if (!$worker->complete) {
            throw new CHttpException('Vartotojas nėra išsprendęs visų testų');
        }

        if (empty($sum)) {

            $sum = array();

            $types = CHtml::listData(Type::model()->findAllWithIgnored(), 'id', 'id');

            foreach ($types as $type) {
                $sum[$type] = 0;
            }

            if (!empty($worker->quizTakens)) {

                foreach ($worker->quizTakens as $quizTaken) {

                    $scores = $quizTaken->getScore();

                    if (!$quizTaken->quiz->required && isset($scores[Type::TEST_TYPE])) {
                        $workerQuizScore = new WorkerQuizScore();
                        $workerQuizScore->quiz_id = $quizTaken->quiz->id;
                        $workerQuizScore->worker_user_id = $worker->user_id;
                        $workerQuizScore->score = $scores[Type::TEST_TYPE];
                        $workerQuizScore->save(false);
                    }

                    foreach ($scores as $id => $score) {
                        $sum[$id] += $score;
                    }
                }
            }
        }

        $sum[0] = $worker->getGeneralScore($sum);
        $types[0] = 0;

        foreach ($types as $type) {
            $workerScore = new WorkerScore();
            $workerScore->worker_user_id = $worker->user_id;
            $workerScore->score = $sum[$type];
            $workerScore->type_id = $type;
            $workerScore->save(false);
        }
    }
}
