<?php

/**
 * This is the model class for table "quiz_taken".
 *
 * The followings are the available columns in table 'quiz_taken':
 * @property integer $id
 * @property integer $quiz_id
 * @property integer $user_id
 * @property integer $complete
 * @property string $date_start
 * @property string $date_end
 *
 * The followings are the available model relations:
 * @property Quiz $quiz
 * @property Worker $user
 * @property QuizTakenAnswer[] $quizTakenAnswers
 */
class QuizTaken extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'quiz_taken';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('quiz_id, user_id', 'required'),
			array('quiz_id, user_id, complete', 'numerical', 'integerOnly'=>true),
			array('date_start, date_end', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, quiz_id, user_id, complete, date_start, date_end', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quiz' => array(self::BELONGS_TO, 'Quiz', 'quiz_id'),
			'user' => array(self::BELONGS_TO, 'Worker', 'user_id'),
			'quizTakenAnswers' => array(self::HAS_MANY, 'QuizTakenAnswer', 'quiz_taken_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'quiz_id' => 'Quiz',
			'user_id' => 'User',
			'complete' => 'Complete',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('quiz_id',$this->quiz_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('complete',$this->complete);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return QuizTaken the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	private function getNotCompleteCriteria()
	{
		$criteria = new CDbCriteria();
		$criteria->addCondition("user_id = :user_id");
		$criteria->addCondition("complete = 0");
		$criteria->join = 'LEFT JOIN ' . Quiz::model()->tableName() . ' q ON q.id = t.quiz_id';
		$criteria->order = 'q.order ASC';
		$criteria->params[':user_id'] = Yii::app()->user->getId();

		return $criteria;
	}

    /**
     * Get not completed quizzess by user
     * @return static
     */
    public function getNotCompleteRunning()
    {
		return $this->find($this->getNotCompleteCriteria());
    }

	/**
	 * Get not completed quizzess by user
	 * @return static
	 */
	public function getNotComplete()
	{
		$criteria = $this->getNotCompleteCriteria();
		$criteria->addCondition("date_start IS NULL");

		return $this->find($criteria);
	}

    /**
     * Count how much Worker complete quizzess
     * @return string
     */
    public function countComplete()
    {
        return $this->countByAttributes(array(
            'user_id' => Yii::app()->user->getId(),
            'complete' => 1
        ));
    }

    /*
     * Get different between start date and end
     */
    public function getDiff()
    {
        $date_start = new DateTime($this->date_start);
        $date_end = new DateTime($this->date_end);
        $interval = $date_start->diff($date_end);
        return $interval->format('%H:%I:%S');
    }

    /**
     * Get quiz score points by type
     * @return array
     */
    public function getScore()
    {
        $result = array();

        foreach ($this->quizTakenAnswers as $answer) {

            foreach ($answer->answer->score as $score) {

				if (!isset($result[$score->type->id])) {
					$result[$score->type->id] = 0;
				}

                $result[$score->type->id] += $score->value;
            }
        }

        return $result;
    }
}
