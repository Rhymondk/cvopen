<?php

/**
 * This is the model class for table "worker_history".
 *
 * The followings are the available columns in table 'worker_history':
 * @property integer $id
 * @property integer $worker_user_id
 * @property string $company
 * @property string $position
 * @property string $date_start
 * @property string $date_end
 * @property string $working_now
 *
 * The followings are the available model relations:
 * @property Worker $workerUser
 */
class WorkerHistory extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'worker_history';
	}

    public function afterFind()
    {

        if (!strtotime($this->date_end)) {
            $this->date_end = '';
        }
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('worker_user_id, company, position, date_start', 'required'),
			array('worker_user_id', 'numerical', 'integerOnly'=>true),
			array('company, position', 'length', 'max'=>255),
			array('working_now', 'length', 'max'=>45),
			array('date_end', 'safe'),
            array('date_end', 'required', 'on' => 'insert'),
            array('date_end', 'checkDate', 'on' => 'insert'),
			array('id, worker_user_id, company, position, date_start, date_end, working_now', 'safe', 'on'=>'search'),
		);
	}

    public function checkDate($attribute, $params)
    {
        if (strtotime($this->date_start) > strtotime($this->date_end)) {
            $this->addError('date_start', Yii::t('web', 'Dirbta nuo negali būti vėlesnė'));
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'workerUser' => array(self::BELONGS_TO, 'Worker', 'worker_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' =>  Yii::t('web', '#'),
			'worker_user_id' => Yii::t('web', 'Kandidatas'),
			'company' => Yii::t('web', 'Įmonės pavadinimas'),
			'position' => Yii::t('web', 'Pozicija'),
			'date_start' => Yii::t('web', 'Dirbta nuo'),
			'date_end' => Yii::t('web', 'Dirbta iki'),
			'working_now' => Yii::t('web', 'Dirbu dabar'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('worker_user_id',$this->worker_user_id);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('working_now',$this->working_now,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return WorkerHistory the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getDateRange() {
        $dateStart = date('Y.m.d', strtotime($this->date_start));

        if ($this->working_now) {
            $dateEnd = Yii::t('web', 'Dabar');
        } else {
            $dateEnd = date('Y.m.d', strtotime($this->date_end));
        }

        return $dateStart . ' - ' . $dateEnd;
    }
}
