<?php

class SelectionShareForm extends CFormModel
{
    public $emails;

    public function rules()
    {
        return array(
            array('emails', 'required'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'emails' => Yii::t('web', 'El. paštų sąrašas'),
        );
    }
}