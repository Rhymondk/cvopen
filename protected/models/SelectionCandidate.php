<?php

/**
 * This is the model class for table "selection_candidate".
 *
 * The followings are the available columns in table 'selection_candidate':
 * @property integer $id
 * @property integer $selection_id
 * @property integer $worker_user_id
 *
 * The followings are the available model relations:
 * @property Selection $selection
 * @property Worker $workerUser
 * @property SelectionCandidateCompetence $selectionCandidateCompetence
 */
class SelectionCandidate extends CActiveRecord
{

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'selection_candidate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('selection_id, worker_user_id', 'required'),
			array('selection_id, worker_user_id, hidden, starred', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, selection_id, worker_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'selection' => array(self::BELONGS_TO, 'Selection', 'selection_id'),
			'workerUser' => array(self::BELONGS_TO, 'Worker', 'worker_user_id'),
			'selectionCandidateCompetence' => array(self::HAS_MANY, 'SelectionCandidateCompetence', 'selection_candidate_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'id' => Yii::t('web', '#'),'ID',
            'selection_id' => Yii::t('web', 'Atranka'),
            'worker_user_id' => Yii::t('web', 'Kandidatas'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('selection_id',$this->selection_id);
		$criteria->compare('worker_user_id',$this->worker_user_id);
        $criteria->group = 'worker_user_id';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return SelectionCandidate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
