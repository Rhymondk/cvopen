<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property string $uid
 * @property integer $amount
 * @property string $date_created
 * @property string $date_paid
 * @property integer $paid
 *
 * The followings are the available model relations:
 * @property QuizOrder $quizOrder
 */
class Order extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('uid, amount, date_created', 'required'),
			array('amount, paid', 'numerical', 'integerOnly'=>true),
			array('uid', 'length', 'max'=>255),
			array('date_paid', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, uid, amount, date_created, date_paid, paid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quizOrder' => array(self::HAS_ONE, 'QuizOrder', 'order_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'uid' => 'Uid',
			'amount' => 'Amount',
			'date_created' => 'Date Created',
			'date_paid' => 'Date Paid',
			'paid' => 'Paid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('amount',$this->amount);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_paid',$this->date_paid,true);
		$criteria->compare('paid',$this->paid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public static function pay($uid, $additional_data = array())
    {
        $order = Order::model()->findByAttributes(array(
            'uid' => $uid
        ));

        if (is_null($order)) {
            throw new CHttpException(404, Yii::t('web', 'Tokio apmokėjimo nėra'));
        }

        if ($order->paid) {
            Yii::app()->request->redirect(Yii::app()->createUrl('order/paid'));
        }

        $data = array_merge(array(
                'projectid'     => Yii::app()->params['paysera_project_id'],
                'sign_password' => Yii::app()->params['paysera_password'],
                'orderid'       => $order->id,
                'amount'        => $order->amount,
                'currency'      => 'EUR',
                'country'       => 'LT',
                'lang'          => 'LIT',
                'payment'       => $order->via,
                'accepturl'     => Yii::app()->createAbsoluteUrl('order/accept'),
                'cancelurl'     => Yii::app()->createAbsoluteUrl('order/cancel'),
                'test'          => Yii::app()->params['paysera_test'],
            ),
            $additional_data
        );

        WebToPay::redirectToPayment($data);
    }
}
