<?php

/**
 * This is the model class for table "selection_share".
 *
 * The followings are the available columns in table 'selection_share':
 * @property integer $id
 * @property integer $selection_id
 * @property string $email
 *
 * The followings are the available model relations:
 * @property Selection $selection
 */
class SelectionShare extends CActiveRecord
{
    /**
     * @var User $user
     */
    private $user;
    const CHUNK = 30;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'selection_share';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('selection_id, email', 'required'),
            array('selection_id', 'numerical', 'integerOnly' => true),
            array('email', 'length', 'max' => 150),
            array('email', 'email'),
            array('send', 'safe')
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'selection' => array(self::BELONGS_TO, 'Selection', 'selection_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'selection_id' => 'Atranka',
            'email' => 'El. paštas',
            'send' => 'Išsiųsta'
        );
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return SelectionShare the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return static
     */
    public function getUser()
    {
        if (!isset($this->user)) {
            $this->user = User::model()->findByAttributes(array(
                'email' => $this->email
            ));
        }

        return $this->user;
    }

    public function getRegister()
    {
        return (bool) $this->getUser();
    }

    public function getQuizCompelete()
    {
        if ($this->getRegister()) {
            return $this->getUser()->worker && $this->getUser()->worker->getComplete($this->selection_id);
        }

        return false;
    }

    public function getMailList()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('t.sent IS NULL');
        $criteria->limit = self::CHUNK;

        return $this->findAll($criteria);
    }
}
