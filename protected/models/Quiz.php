<?php

/**
 * This is the model class for table "test".
 *
 * The followings are the available columns in table 'test':
 * @property integer $id
 * @property string $name
 *
 * The followings are the available model relations:
 * @property TestQuestion[] $testQuestions
 * @property TestTaken[] $testTakens
 */
class Quiz extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'quiz';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, time', 'required'),
            array('name', 'length', 'max' => 255),
            array('time, order, pagination', 'numerical', 'integerOnly' => true),
            array('required, order, time, description', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'quizQuestions' => array(self::HAS_MANY, 'QuizQuestion', 'quiz_id'),
            'quizTakens' => array(self::HAS_MANY, 'QuizTaken', 'quiz_id'),
            'selectionQuiz' => array(self::HAS_MANY, 'SelectionQuiz', 'quiz_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('web', '#'),
            'name' => Yii::t('web', 'Testo pavadinimas'),
            'pagination' => Yii::t('web', 'Puslapiavimas'),
            'time' => Yii::t('web', 'Laikas sprendimui minutėmis (0 - laikas nerodomas)'),
            'required' => Yii::t('web', 'Būtinas'),
            'order' => Yii::t('web', 'Eilės numeris (Mažesnis numeris pirmesnis)'),
            'description' => Yii::t('web', 'Testo aprašymas (Rodoma prieš sprendžiant testą)')
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
        ));
    }

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getMaxScore()
    {
        $key = 'quiz_max_score_' . $this->id;
        $max_score = Yii::app()->cache->get($key);

        if ($max_score === false) {

            $max_score = 0;

            foreach ($this->quizQuestions as $question) {

                foreach ($question->question->answers as $answer) {

                    foreach ($answer->score as $score) {

                        if ($score->type_id == Type::TEST_TYPE) {
                            $max_score += $score->value;
                        }
                    }
                }
            }

            Yii::app()->cache->set($key, $max_score);
        }

        return $max_score;
    }
}
