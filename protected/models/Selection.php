<?php

/**
 * This is the model class for table "selection".
 *
 * The followings are the available columns in table 'selection':
 * @property integer $id
 * @property integer $user_id
 * @property integer $city_id
 * @property string $position
 * @property string $salary
 * @property string $description
 *
 * The followings are the available model relations:
 * @property Competence[] $competences
 * @property City $city
 * @property Employer $user
 */
class Selection extends CActiveRecord
{

    const NOT_FILLED = 1;
    const NOT_ACTIVE = 2;
    const WAITING = 3;
    const ACTIVE = 4;
    const ENDED = 5;
    const DEMO = 6;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'selection';
    }

    public function afterFind()
    {
        if (strtotime($this->date_start)) {
            $this->date_start = date("Y-m-d", strtotime($this->date_start));
        }

        if (strtotime($this->date_end)) {
            $this->date_end = date("Y-m-d", strtotime($this->date_end));
        }
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('user_id, position, description, offer', 'required'),
            array('user_id, city_id, notification', 'numerical', 'integerOnly' => true),
            array('position', 'length', 'max' => 255),
            array('salary', 'length', 'max' => 45),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, user_id, city_id, position, salary, description', 'safe', 'on' => 'search'),
            array('status_id, ost_type, bill, date_start, date_end', 'safe'),
            //Selection hosting
            array('date_start, date_end', 'required', 'on' => 'hosting'),
            array('date_start, date_end', 'type', 'type' => 'datetime', 'datetimeFormat' => 'yyyy-mm-dd', 'on' => 'hosting'),
            array('date_start', 'dateRange', 'on' => 'hosting'),
        );
    }

    public function dateRange($attribute, $params)
    {
        if (strtotime($this->date_start) > strtotime($this->date_end)) {
            $this->addError($attribute, Yii::t('web', 'Pradžios data negali būti vėlesnė'));
        }
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'competences' => array(self::HAS_MANY, 'Competence', 'selection_id'),
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'employer' => array(self::BELONGS_TO, 'User', 'user_id'),
            'hostings' => array(self::HAS_MANY, 'SelectionHosting', 'selection_id'),
            'status' => array(self::BELONGS_TO, 'Status', 'status_id'),
            'user' => array(self::BELONGS_TO, 'User', 'user_id'),
            'selectionCandidates' => array(self::HAS_MANY, 'SelectionCandidate', 'selection_id', 'group' => 'worker_user_id'),
            'selectionQuizes' => array(self::HAS_MANY, 'SelectionQuiz', 'selection_id'),
            'selectionQuizesCount' => array(self::STAT, 'SelectionQuiz', 'selection_id'),
            'selectionTypes' => array(self::HAS_MANY, 'SelectionType', 'selection_id'),
            'CandidatesCount' => array(self::STAT, 'SelectionCandidate', 'selection_id'),
            'selectionHostings' => array(self::HAS_MANY, 'SelectionHosting', 'selection_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('web', '#'),
            'user_id' => Yii::t('web', 'Dardavys'),
            'city_id' => Yii::t('web', 'Miestas'),
            'status_id' => Yii::t('web', 'Statusas'),
            'position' => Yii::t('web', 'Pozicija'),
            'salary' => Yii::t('web', 'Atlyginimas (€)'),
            'description' => Yii::t('web', 'Darbo pobūdis'),
            'offer' => Yii::t('web', 'Ką mes siūlome'),
            'date_start' => Yii::t('web', 'Pradžios data'),
            'date_end' => Yii::t('web', 'Pabaigos data'),
            'notification' => Yii::t('web', 'Pranešti el. paštu apie kandidatus'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->order = 'id DESC';

        $criteria->compare('id', $this->id);
        $criteria->compare('user_id', $this->user_id);
        $criteria->compare('city_id', $this->city_id);
        $criteria->compare('position', $this->position, true);
        $criteria->compare('salary', $this->salary, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                'pageSize' => Yii::app()->params['default_page_size']
            ),
        ));
    }


    /**
     *
     * @param int $type_id
     * @param int $starred
     * @param int $hidden
     * @return CDbCriteria
     */
    public function getValidCandidatesCriteria($type_id = 0, $starred = 0, $hidden = 0)
    {
        $criteria = new CDbCriteria;
        $criteria->alias = 'sc';
        $criteria->select = '*,';
        $criteria->addCondition('sc.selection_id = :selection_id');
        $criteria->select .= '(SELECT ws.score FROM worker_score ws WHERE ws.worker_user_id = sc.worker_user_id AND ws.type_id = :type_id)';
        $criteria->params[':type_id'] = $type_id;

        if ($this->hasQuiz && $type_id == 0) {

            foreach ($this->selectionQuizes as $key => $selectionQuiz) {
                $criteria->select .= '+IFNULL((SELECT wqs.score FROM worker_quiz_score wqs
                WHERE
                    wqs.worker_user_id = sc.worker_user_id AND
                    wqs.quiz_id = :quiz_id' . $key . '), 0)';

                $criteria->params[':quiz_id' . $key] = $selectionQuiz->quiz->id;
            }

            $criteria->select .= ' as score';
        } else {
            $criteria->select .= ' as score';
        }

        $criteria->having = 'score IS NOT NULL';

        //If type "Patikimumo skalė" show inverted results
        if ($type_id == 7) {
            $criteria->order = 'score ASC';
        } else {
            $criteria->order = 'score DESC';
        }

        if ($starred != 0) {
            $criteria->addCondition('sc.starred = :starred', 'AND');
            $criteria->params[':starred'] = 1;
        }

        if ($hidden != 0) {
            $criteria->addCondition('sc.hidden = :hidden', 'AND');
            $criteria->params[':hidden'] = $hidden;
        }

        $criteria->params[':selection_id'] = $this->id;

        $criteria->group = 'sc.worker_user_id';

        return $criteria;
    }

    /**
     * Get candidates that completed quizzes
     * @return array
     */
    public function getValidCandidates($type_id = 0, $starred = 0, $hidden = 0)
    {
        $criteria = $this->getValidCandidatesCriteria($type_id, $starred, $hidden);
        $count = count(SelectionCandidate::model()->findAll($criteria));

        $pages = new CPagination($count);

        $pageSize = Yii::app()->request->getParam('limit', Yii::app()->params['selections_in_page']);
        $inPageOptions = explode(',', Yii::app()->params['selections_in_page_option']);
        $pageSize = in_array($pageSize, $inPageOptions) ? $pageSize : Yii::app()->params['selections_in_page'];

        $pages->pageSize = in_array($pageSize, $inPageOptions) ? $pageSize : Yii::app()->params['selections_in_page'];

        $pages->applyLimit($criteria);

        $selectionCandidates = SelectionCandidate::model()->findAll($criteria);

        if (empty($selectionCandidates)) {
            return array(false, false);
        }

        return array($pages, $selectionCandidates);
    }

    /**
     * Get count candidates that completed quizzes
     * @return int
     */
    public function getCountValidCandidates()
    {
        //Get criteria with random type_id
        $criteria = $this->getValidCandidatesCriteria(1);
        return SelectionCandidate::model()->count($criteria);
    }

    /**
     * Get date period if date end is greater than today
     * then date end = today
     * @param null $formatStart
     * @param null $formatEnd
     * @return string
     */
    public function getPeriod($formatStart = 'Y.m.d', $formatEnd = 'm.d', $seperator = '-')
    {
        $periodStart = date($formatStart, strtotime($this->date_start));
        $dateEnd = strtotime($this->date_end) < strtotime(date('now')) ? $this->date_end : date('Y-m-d');
        $periodEnd = date($formatEnd, strtotime($dateEnd));

        return $periodStart . $seperator . $periodEnd;
    }

    /**
     * Check and return if selection is valid
     * @param int $id - Selection id
     * @return object static
     * @throws CHttpException - Selection not found
     */
    public function findValidByPk($id)
    {

        $criteria = self::getSelectionValidCriteria();

        $criteria->addCondition('id = :id', 'AND');

        $criteria->params[':id'] = $id;
        $criteria->limit = 1;

        $selection = $this->find($criteria);

        if (empty($selection)) {
            throw new CHttpException(404, 'Atranka nerasta');
        }

        return $selection;
    }

    public function notify($worker)
    {
        if ($this->notification) {
            Mailer::app()->sendTemplate($this->user->email, 'candidate', array(
                'selection' => $this->position
            ));
        }
    }

    /**
     * Get default criteria to find valid selections
     * @return CDbCriteria
     */
    public static function getSelectionValidCriteria()
    {
        $criteria = new CDbCriteria;
        $criteria->addCondition('date_end >= :date_end', 'AND');
        $criteria->addCondition('date_start <= :date_start', 'AND');

        $criteria->params = array(
            ':date_end' => date('Y-m-d'),
            ':date_start' => date('Y-m-d')
        );

        $criteria->addInCondition('status_id', array(self::DEMO, self::ACTIVE));

        return $criteria;
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Selection the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check if selection is DEMO
     */
    public function isDemo()
    {
        return $this->status_id == self::DEMO || $this->status_id == self::NOT_ACTIVE;
    }

    /**
     * Get default status after creating selection
     */
    public static function getDefaultStatus()
    {
        return self::DEMO;
    }

    /**
     * get valid statuses for showing selections
     */
    public static function validStatus()
    {
        return implode(',', array(
            self::DEMO,
            self::ACTIVE
        ));
    }

    public function isValidStatus()
    {
        return in_array($this->status_id, array(self::DEMO, self::ACTIVE));
    }

    public function getHasQuiz()
    {
        return (bool)$this->selectionQuizesCount;
    }

    public function getCandidateScore(Worker $worker)
    {
        if (!$this->getHasQuiz()) {
            return $worker->getGeneralScore();
        }

        $general = $worker->getGeneralScoreCount();
        $maxScore = $worker->getMaxScoreCount();

        foreach ($this->selectionQuizes as $selectionQuiz) {
            $maxScore += 6;

            $point = 6 / $selectionQuiz->quiz->maxScore;
            $general += $worker->getSelectionQuizScore($selectionQuiz->quiz) * $point;
        }

        return round($general * 100 / $maxScore);
    }
}
