<?php
class LoginForm extends CFormModel
{
    public $username;
    public $password;
    public $userType;
 
    private $_identity;
 
    public function __construct($arg = 'Public')
    {
        $this->userType = $arg;
    }

    public function rules()
    {
        return array(
            // username and password are required
            array('username, password', 'required'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'username' => Yii::t('web', 'El. paštas'),
            'password' => Yii::t('web', 'Slaptažodis'),
        );
    }

    public function login()
    {
        if ($this->_identity === null) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            $this->_identity->userType = $this->userType;
            $this->_identity->authenticate();
        }

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            Yii::app()->user->login($this->_identity);
            return true;
        } else {
            return false;
        }
    }

    public function authenticate($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $this->_identity = new UserIdentity($this->username, $this->password);
            $this->_identity->userType = $this->userType;

            if (!$this->_identity->authenticate()) {
                $this->addError('password', Yii::t('web', 'Duomenys neteisingi.'));
            }
        }
    }
}
