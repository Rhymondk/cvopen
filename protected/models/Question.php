<?php

/**
 * This is the model class for table "question".
 *
 * The followings are the available columns in table 'question':
 * @property integer $id
 * @property string $name
 * @property string $image
 * @property integer $question_type_id
 *
 * The followings are the available model relations:
 * @property Answer[] $answers
 * @property QuestionType $questionType
 * @property Quiz[] $quizs
 * @property QuizTakenAnswer[] $quizTakenAnswers
 */
class Question extends CActiveRecord
{

    public $imageFile;

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'question';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('name', 'length', 'max'=>255),
            array(
                'imageFile',
                'file',
                'maxSize' => 1024 * 1024 * Yii::app()->params['max_image_size'],
                'types' => Yii::app()->params['valid_image_format'],
                'allowEmpty' => true,
            ),
            array('image, question_type_id', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'answers' => array(self::HAS_MANY, 'Answer', 'question_id'),
            'questionType' => array(self::BELONGS_TO, 'QuestionType', 'question_type_id'),
            'quizs' => array(self::MANY_MANY, 'Quiz', 'quiz_question(question_id, quiz_id)'),
            'quizTakenAnswers' => array(self::HAS_MANY, 'QuizTakenAnswer', 'question_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('web', '#'),
            'name' => Yii::t('web', 'Klausimas'),
            'image' => Yii::t('web', 'Klausimo nuotrauka'),
            'question_type_id' => Yii::t('web', 'Klausimo tipas'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Question the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
