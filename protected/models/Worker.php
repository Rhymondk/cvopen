<?php

/**
 * This is the model class for table "worker".
 *
 * The followings are the available columns in table 'worker':
 * @property integer $user_id
 * @property string $firstname
 * @property string $lastname
 * @property string $address
 * @property string $birthday
 * @property integer $education_id
 * @property integer $gender_id
 * @property string $comment
 * @property string $salary
 *
 * The followings are the available model relations:
 * @property QuizTaken[] $quizTakens
 * @property SelectionCandidate[] $selectionCandidates
 * @property User $user
 * @property Education $education
 * @property Gender $gender
 * @property WorkerHistory[] $workerHistories
 */
class Worker extends CActiveRecord
{
    public $cvFile;
    public $scoretmp; //Sorting issue

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'worker';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('user_id', 'required'),
			array('user_id, education_id, gender_id', 'numerical', 'integerOnly'=>true),
			array('firstname, lastname', 'length', 'max'=>100),
			array('address, cv', 'length', 'max'=>255),
            array('number', 'phoneNumber'),
            array('number', 'length', 'max' => 20),
			array('salary', 'length', 'max'=>150),
			array('birthday, comment, admin_comment', 'safe'),
            array('firstname, lastname, education_id, gender_id, address, birthday, number', 'required', 'on' => 'selection-create'),
            array(
                'cvFile',
                'file',
                'maxSize' => 1024 * 1024 * Yii::app()->params['max_cv_size'],
                'types' => Yii::app()->params['valid_cv_format'],
                'allowEmpty' => true,
            ),
			array('user_id, firstname, lastname, address, birthday, education_id, gender_id, comment, salary', 'safe', 'on'=>'search'),
		);
	}

    public function phoneNumber($attribute, $params)
    {
        if (!empty($this->number) && !preg_match('/^\+?\d+$/', $this->number)) {
            $this->addError($attribute, Yii::t('web', 'Blogai įvestas telefono numeris'));
        }
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'quizTakens' => array(self::HAS_MANY, 'QuizTaken', 'user_id'),
			'selectionCandidates' => array(self::HAS_MANY, 'SelectionCandidate', 'worker_user_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
			'education' => array(self::BELONGS_TO, 'Education', 'education_id'),
			'gender' => array(self::BELONGS_TO, 'Gender', 'gender_id'),
			'workerHistories' => array(self::HAS_MANY, 'WorkerHistory', 'worker_user_id'),
            'quizOrder' => array(self::HAS_ONE, 'QuizOrder', 'worker_user_id'),
            'workerScores' => array(self::HAS_MANY, 'WorkerScore', 'worker_user_id'),
            'workerQuizScores' => array(self::HAS_MANY, 'WorkerQuizScore', 'worker_user_id'),

		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
            'user_id' => Yii::t('web', '#'),
            'firstname' => Yii::t('web', 'Vardas'),
            'lastname' => Yii::t('web', 'Pavardė'),
            'address' => Yii::t('web', 'Gyvenamoji vieta'),
            'gender_id' => Yii::t('web', 'Lytis'),
            'birthday' => Yii::t('web', 'Gimimo data'),
            'education_id' => Yii::t('web', 'Išsilavinimas'),
            'comment' => Yii::t('web', 'Papildomi komentarai'),
			'salary' => Yii::t('web', 'Pageidaujamas atlyginimas Eur'),
			'number' => Yii::t('web', 'Telefono numeris'),
			'admin_comment' => Yii::t('web', 'CVOPEN Komentaras'),
			'CvFile' => Yii::t('web', 'CV')
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('education_id',$this->education_id);
		$criteria->compare('gender_id',$this->gender_id);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('salary',$this->salary,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'pagination' => array('pageSize' => Yii::app()->params['default_page_size']),
		));
	}

    public function getAge()
    {
        if (!empty($this->birthday)) {
            $birthdate = new DateTime($this->birthday);
            $today   = new DateTime('today');
            return $birthdate->diff($today)->y;
        }

        return 0;
    }

    public function getFullname()
    {
        return ucfirst($this->firstname) . ' ' . ucfirst($this->lastname);
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Worker the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    /**
     * Check if worker completed quizzes
     * @return bool
     */
    public function getComplete($id = null)
    {
        $criteria = new CDbCriteria();
        $criteria->order = 't.order ASC';
        $criteria->addCondition('required = 1', 'OR');

        if (!is_null($id)) {
            Yii::app()->session['candidateSelectionId'] = $id;

            $criteria->join = 'LEFT JOIN ' . SelectionQuiz::model()->tableName() . ' sq ON sq.quiz_id = t.id';

            $criteria->addCondition('sq.selection_id = :selection_id', 'OR');
            $criteria->params[':selection_id'] = $id;
        }

        $quizzes = Quiz::model()->findAll($criteria);

        foreach ($quizzes as $quiz) {
            $quizTaken = QuizTaken::model()->findByAttributes(array(
                'quiz_id' => $quiz->id,
                'user_id' => $this->user_id,
            ));

            if (is_null($quizTaken)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Get score by type_id
     * if type_id null return all types scores
     * @param null $type_id
     * @return array || int
     */
    public function getScore($type_id = null)
    {

        $types = CHtml::listData(Type::model()->findAll(), 'id', 'max_score');
        $sum = array();

        foreach(array_keys($types) as $type) {
            $sum[$type] = 0;
        }

        $scores = CHtml::listData($this->workerScores, 'type_id', 'score');

        foreach ($scores as $key => $score) {
            $sum[$key] = $score;
        }

        if (!is_null($type_id)) {

            if ($type_id == 0) {
                return $this->getGeneralScore($sum);
            }

            $sum[7] = (int) $types[7] - $sum[7];
            return $sum[$type_id];
        }

        $sum[0] = (int) $this->getGeneralScore($sum);
        $sum[7] = (int) $types[7] - $sum[7];
        return $sum;
    }

    /**
     * Check if worker paid for quizzes
     * @return bool
     */
    public function getPaid()
    {
        return (isset($this->quizOrder->order->paid) && $this->quizOrder->order->paid);
    }

    /**
     * Get type description via score
     * @param $type_id
     * @return array
     */
    public function getTypeAttribute($type_id)
    {
        $score = $this->getScore($type_id);

        $typeDescription = TypeDescription::model()->findByAttributes(array(),
            'type_id = :type_id && min <= :score && max >= :score',
            array(
                ':type_id' => $type_id,
                ':score' => $score
            )
        );

        return $typeDescription;
    }

    /**
     * IQ quiz full results
     * @return array
     */
    public function getIqQuizResults()
    {
        $results = Yii::app()->cache->get('iqResults_' . $this->user_id);

        if ($results === false) {
            $iqType = Type::model()->findByPk(Yii::app()->params['iq_type_id']);
            $quizTaken = QuizTaken::model()->findByAttributes(array(
                'quiz_id' => $iqType->id,
                'user_id' => $this->user_id
            ));

            $results = array(
                'answered' => 0,
                'correct' => 0,
                'incorrect' => 0
            );

            $results['answered'] = 0;
            foreach ($quizTaken->quizTakenAnswers as $takenAnswer) {
                $answer = Score::model()->findByAttributes(array(
                    'answer_id' => $takenAnswer->answer_id,
                    'type_id' => $iqType->id
                ));

                if ($answer->value) {
                    $results['correct']++;
                } else {
                    $results['incorrect']++;
                }

                $results['answered']++;
            }

            $results['unanswered'] = (int) $iqType->max_score - $results['answered'];

            foreach ($results as $key => $result) {
                $results[$key] = round(($result / $iqType->max_score) * 100);
            }

            Yii::app()->cache->set('iqResults_' . $this->user_id, $results, 60*60*60*24*10);
        }

        return $results;
    }

    /**
     * General quizzes score percentage
     * @return int
     */
    public function getGeneralScore($score = null)
    {
        return round(($this->getGeneralScoreCount($score) * 100) / $this->getMaxScoreCount());
    }

    public function getGeneralScoreCount($score = null)
    {
        if (is_null($score)) {
            $score = CHtml::listData($this->workerScores, 'type_id', 'score');
        }

        if (empty($score)) {
            throw new CHttpException(404, 'Klaida general score. worker_id: ' . $this->user_id);
        }

        $types = CHtml::listData(Type::model()->findAll(), 'id', 'id');

        foreach($types as $type) {
            $score[$type] = isset($score[$type]) ? $score[$type] : 0;
        }

        //General score
        $general = (int) $score[Yii::app()->params['iq_type_id']] + (
                (int) $score[5] + (int) $score[6] -
                ((int) $score[3] * 2) + (int) $score[4]
            ) - (int) $score[7];

        $general = $general < 0 ? 0 : $general;

        return $general;
    }

    public function getMaxScoreCount()
    {
        $types = Type::model()->findAll();
        $typeslist = CHtml::listData($types, 'id', 'max_score');

        $maxScore = 0;
        $maxScore += (int) $typeslist[Yii::app()->params['iq_type_id']];
        $maxScore += (int) $typeslist[5];
        $maxScore += (int) $typeslist[6];
        $maxScore += (int) $typeslist[4];

        return $maxScore;
    }

    /**
     * Results for employer selection candidates list
     * @return object
     */
    public function getTypeResultsShow()
    {
        $types = Type::model()->findAll();
        $results = array();

        foreach ($types as $type) {
            $results[$type->id]['id'] = $type->id;
            $results[$type->id]['name'] = $type->name;
            $results[$type->id]['percent'] = round(($this->getScore($type->id) * 100) / $type->max_score);
        }

        return $results;
    }

    /**
     * Check if CV exist
     */
    public function cvExist($file)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = 'cv = :cv';
        $criteria->params = array(':cv' => $file);
        return $this->exists($criteria);
    }

    public function getQuizScore($quiz)
    {
        return round($this->getSelectionQuizScore($quiz) * 100 / $quiz->maxScore);
    }

    public function getSelectionQuizScore($quiz)
    {
        $workerQuizScore = WorkerQuizScore::model()->findByAttributes(array(
            'worker_user_id' => $this->user_id,
            'quiz_id' => $quiz->id
        ));

        if (!$workerQuizScore) {
            return 0;
        }

        return (int) $workerQuizScore->score;
    }

    public function getCvLink()
    {
        return Yii::app()->createUrl('site/download', array('file' => urlencode(base64_encode($this->cv))));
    }
}
