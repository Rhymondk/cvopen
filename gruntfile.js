module.exports = function (grunt) {
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-ftp-diff-deployer');

    grunt.registerTask('default', ['watch']);
    grunt.registerTask('dist', ['uglify', 'cssmin']);

    grunt.initConfig({
        uglify: {
            my_target: {
                files: {
                    'js/admin/script.min.js': [
                        'vendor/js/admin/global.js'
                    ],
                    'js/public/script.min.js': [
                        'vendor/remodal/dist/jquery.remodal.min.js',
                        'vendor/pickadate/picker.js',
                        'vendor/pickadate/picker.date.js',
                        'vendor/pickadate/legacy.js',
                        'vendor/select2/select2.min.js',
                        'vendor/autosize/dest/autosize.min.js',
                        'vendor/owl/owl.carousel.min.js',
                        'vendor/sweetalert/dist/sweetalert.min.js'
                    ]
                }
            }
        },
        cssmin: {
            target: {
                files: {
                    'css/admin/style.min.css': [
                        'vendor/normalize/normalize.css',
                        'vendor/animate/animate.min.css',
                        'vendor/css/admin/main.css'
                    ],
                    'css/public/slides.min.css': [
                        'vendor/glidejs/dist/css/style.css',
                        'vendor/css/public/slides.css'
                    ],
                    'css/public/login.min.css': [
                        'vendor/normalize/normalize.css',
                        'vendor/grid/grid.css',
                        'vendor/animate/animate.min.css'
                    ],
                    'css/public/style.min.css': [
                        'vendor/normalize/normalize.css',
                        'vendor/grid/grid.css',
                        'vendor/popover/popover.css',
                        'vendor/remodal/dist/jquery.remodal.css',
                        'vendor/animate/animate.min.css',
                        'vendor/select2/select2.min.css',
                        'vendor/pickadate/themes/classic.css',
                        'vendor/pickadate/themes/classic.date.css',
                        'vendor/owl/assets/owl.carousel.css',
                        'vendor/sweetalert/dist/sweetalert.css',
                        'vendor/hint/hint.min.css'
                    ]
                }
            }
        },
        watch: {
            js: {
                files: [
                    'vendor/**/*.js'
                ],
                tasks: ['uglify']
            },
            css: {
                files: [
                    'vendor/**/*.js',
                    'vendor/**/*.css'
                ],
                tasks: ['cssmin']
            }
        }
    });
};
