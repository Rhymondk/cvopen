app = (function () {

    var private = {};
    this.global = {};

    private.Init = function () {
        return {
            selectors: {
                checkBox: 'input[type="checkbox"]:not(.default), input[type="radio"]',
                popover: '.popover-trigger',
                scrollTo: '.scroll-to',
                select2: 'select',
                textareaAutoSize: 'textarea',
                pickadateSelector: 'input.pickadate',
                timeLeft: '.time-left',
                timeleftMinutes: '.time-left .minutes',
                timeleftSeconds: '.time-left .seconds',
                pickadateinput: 'input.pickadate',
                sweetalert: '*[data-sweetalert]'
            },
            actionInit: function () {
                var obj = this;
                obj.checkboxStyle();
                obj.popover();
                obj.scrollTo();
                obj.select2();
                obj.textareaAutoSize();
                obj.pickadateInit();
                obj.sweetAlert();

                if ($(this.selectors.timeLeft).length) {
                    obj.timeLeft();
                }
            },
            timeLeft: function () {
                if ($(this.selectors.timeLeft).length) {
                    var self = this;
                    var leftTimeInterval = setInterval(function () {
                        var minutes = parseInt($(self.selectors.timeleftMinutes).text());
                        var seconds = parseInt($(self.selectors.timeleftSeconds).text());
                        var totalSeconds = seconds + (minutes * 60);
                        --totalSeconds;
                        if (totalSeconds >= 0) {
                            function pad(val) {
                                return val > 9 ? val : "0" + val;
                            }

                            $(self.selectors.timeleftSeconds).text(pad(totalSeconds % 60));
                            $(self.selectors.timeleftMinutes).text(pad(parseInt(totalSeconds / 60, 10)));
                        } else {
                            window.onbeforeunload = null;
                            location.reload();
                        }
                    }, 1000);
                }
            },
            sweetAlert: function () {
                $(this.selectors.sweetalert).on('click', function (e) {
                    e.preventDefault();

                    swal(
                        $(this).data('title'),
                        $(this).data('description'),
                        $(this).data('type')
                    );
                });
            },
            pickadateInit: function () {
                $(this.selectors.pickadateinput).pickadate({
                    labelMonthNext: 'Sekantis mėnuo',
                    labelMonthPrev: 'Ankstesnis mėnuo',
                    labelMonthSelect: 'Pasirinkite mėnesį',
                    labelYearSelect: 'Pasirinkite metus',
                    monthsFull: ['Sausis', 'Vasaris', 'Kovas', 'Balandis', 'Gegužė', 'Birželis', 'Liepa', 'Rugpjūtis', 'Rugsėjis', 'Spalis', 'Lapkritis', 'Gruodis'],
                    monthsShort: ['Sau', 'Vas', 'Kov', 'Bal', 'Geg', 'Bir', 'Lie', 'Rgp', 'Rgs', 'Spa', 'Lap', 'Grd'],
                    weekdaysFull: ['Sekmadienis', 'Pirmadienis', 'Antradienis', 'Trečiadienis', 'Ketvirtadienis', 'Penktadienis', 'Šeštadienis'],
                    weekdaysShort: ['Sk', 'Pr', 'An', 'Tr', 'Kt', 'Pn', 'Št'],
                    today: '',
                    clear: '',
                    close: 'Uždaryti',
                    firstDay: 1,
                    format: 'yyyy-mm-dd',
                    formatSubmit: 'yyyy-mm-dd',
                    selectYears: 50,
                    selectMonths: true
                });
            },
            textareaAutoSize: function () {
                autosize($(this.selectors.textareaAutoSize));
            },
            select2: function () {

                var options = {};

                if (typeof $(this.selectors.select2).data('withoutsearch') !== 'undefined') {
                    options.minimumResultsForSearch = Infinity;
                }

                if (typeof $(this.selectors.select2).data('prompt') !== 'undefined') {
                    options.placeholder = $(this.selectors.select2).data('prompt');
                }

                $(this.selectors.select2).select2(options);
            },
            scrollTo: function () {

                if (window.location.hash) {
                    var $trigger = $('a[href$=' + window.location.hash + ']');

                    if ($trigger.length && $trigger.data('scroll')) {
                        $('html, body').animate({
                            scrollTop: $($trigger.data('scroll')).offset().top - 50
                        }, 500);
                    }
                }

                $(this.selectors.scrollTo).click(function (e) {
                    //e.preventDefault();
                    var $this = $(this);
                    $('html, body').animate({
                        scrollTop: $($this.data('scroll')).offset().top - 50
                    }, 500);
                });
            },
            checkboxStyle: function () {
                $(this.selectors.checkBox).each(function () {
                    var $this = $(this);
                    var name = $this.attr('name') || '';
                    var type = $this.attr('type') || '';
                    var id = $this.attr('id') || '';
                    var c = $this.attr('class') || '';
                    var val = $this.val() || '';
                    var placeholder = $this.attr('placeholder') || '';
                    var checked = $this.prop('checked') || '';


                    var html = '';
                    html += '<div class="' + type + '-block">';
                    html += '<input ' +
                        'name="' + name + '" ' +
                        'type="' + type + '" ' +
                        'id="' + id + '" ' +
                        'class="' + c + '" ' +
                        'value="' + val + '" ' + (checked ? 'checked' : '') + '/>';
                    html += '<label for="' + id + '"><span></span>' + placeholder + '</label>';
                    html += '</div>';

                    $this.replaceWith(html);

                });
            },
            popover: function () {
                $(this.selectors.popover).click(function () {
                    var $this = $(this);
                    var popoverId = $this.data('popover');
                    var $popover = $('#' + popoverId);

                    if ($popover.hasClass('open')) {
                        $popover.removeClass('open');
                        return;
                    }

                    var offset = $this.offset();

                    $popover.css({
                        'top': offset.top + $this.outerHeight() + 5,
                        'left': Math.round(offset.left - ($popover.width() / 2)) + Math.round($this.outerWidth() / 2)
                    });

                    $popover.addClass('open');
                });
            }
        }
    };

    private.Quiz = function () {
        return {
            selectors: {
                answers: '.answers',
                quizTakenForm: '#quiz-taken-asnwer-form',
                answer: 'input[name="QuizTakenAnswer[answer_id]"]:checked',
                page: '.pager > .page',
                quit: '.navigation .icon-exit'
            },
            quitMessage: function () {
                window.onbeforeunload = function (e) {
                    var message = $('#leave-message').data('text'),
                        e = e || window.event;

                    if (e) {
                        e.returnValue = message;
                    }

                    return message;
                };
            },
            actionSolve: function () {
                this.quitMessage();

                $(app.getSelector('quit')).click(function (e) {
                    e.preventDefault();
                    window.onbeforeunload = null;
                    var $this = $(this);

                    swal({
                        title: "Ar tikrai norite išeiti?",
                        text: $('#leave-message').data('text'),
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Taip",
                        cancelButtonText: "Ne",
                        closeOnConfirm: true,
                        closeOnCancel: true
                    }, function (isConfirm) {
                        if (isConfirm) {
                            window.location = $this.attr('href');
                        }
                    });
                });

                $(app.getSelector('page')).click(function () {
                    window.onbeforeunload = null;
                });

                $(app.getSelector('quizTakenForm')).submit(function (e) {
                    var self = this;
                    e.preventDefault();
                    if ($(app.getSelector('answer')).length) {
                        window.onbeforeunload = null;
                        self.submit();
                    } else {
                        $(app.getSelector('answers')).addClass('shake');
                        $(app.getSelector('answers')).one('webkitAnimationEnd ' +
                            'mozAnimationEnd ' +
                            'MSAnimationEnd ' +
                            'oanimationend ' +
                            'animationend',
                            function () {
                                $(this).removeClass('shake');
                            });
                    }
                });
            }
        }
    };

    private.Faq = function () {
        return {
            selectors: {
                moreButton: '.more',
                accordionParent: '.faq-block'
            },
            actionIndex: function () {
                $(app.getSelector('moreButton')).click(function () {
                    var $parent = $(this).parents(app.getSelector('accordionParent'));

                    if ($parent.hasClass('active')) {
                        $parent.removeClass('active');
                    } else {
                        $parent.addClass('active');
                    }
                });
            }
        }
    };

    private.Worker = function () {
        return {
            selectors: {
                limitSelect: 'select[name="limit"]',
                workerImage: '#User_imageFile',
                workerCv: '#Worker_cvFile',
                uploadImageButton: '.upload-image .btn',
                uploadCvButton: '.upload-selection .btn',
                previewImage: '.image .img-preview',
                birthdayPicker: '#Worker_birthday',
                workingNow: '#WorkerHistory_working_now',
                dateEnd: '#WorkerHistory_date_end',
                dateStart: '#WorkerHistory_date_start',
                dateEndError: '#WorkerHistory_date_end_em_',
                chooseCompetence: '.choose-competence .competence',
                workerForm: '#worker-form',
                workerFileForm: '#worker-file-form',
                submitForm: '#submit-form',
                submitWorkerForm: '#submit-worker-form'
            },
            actionSelections: function () {
                $(app.getSelector('limitSelect')).change(function () {
                    window.location.href = Yii.app.createUrl('worker/selections', {limit: $(this).find(':selected').text()});
                })
            },
            actionEdit: function () {
                $(app.getSelector('uploadCvButton')).click(function () {
                    $(app.getSelector('workerCv')).click();
                });

                $(app.getSelector('uploadImageButton')).click(function () {
                    $(app.getSelector('workerImage')).click();
                });

                $(app.getSelector('workerImage')).change(function () {
                    $(app.getSelector('workerFileForm')).submit();
                });

                $(app.getSelector('workerCv')).change(function () {
                    $(app.getSelector('workerFileForm')).submit();
                });

                var $birthdayPicker = $(app.getSelector('birthdayPicker')).pickadate();
                var pickerBirthday = $birthdayPicker.pickadate('picker');

                pickerBirthday.set('max', new Date(2008, 1, 1));
                pickerBirthday.set('selectYears', true);

                var $dateStartPicker = $(app.getSelector('dateStart')).pickadate();
                var pickerDateStart = $dateStartPicker.pickadate('picker');
                pickerDateStart.set('max', true);

                var $dateEndPicker = $(app.getSelector('dateEnd')).pickadate();
                var pickerDateEnd = $dateEndPicker.pickadate('picker');
                pickerDateEnd.set('max', true);

                $(app.getSelector('workingNow')).change(function () {
                    $(app.getSelector('dateEnd')).toggle();
                    $(app.getSelector('dateEndError')).toggle();
                });

                $(app.getSelector('chooseCompetence')).click(function () {
                    $(this).toggleClass('active');
                    $(this).next().trigger('click');
                });
            },
            actionSelection: function () {
                var $birthdayPicker = $(app.getSelector('birthdayPicker')).pickadate();
                var pickerBirthday = $birthdayPicker.pickadate('picker');

                pickerBirthday.set('max', new Date(2008, 1, 1));
                pickerBirthday.set('selectYears', true);

                $(app.getSelector('uploadCvButton')).click(function () {
                    $(app.getSelector('workerCv')).click();
                });

                $(app.getSelector('uploadImageButton')).click(function () {
                    $(app.getSelector('workerImage')).click();
                });

                $(app.getSelector('workerImage')).change(function () {
                    $(app.getSelector('workerFileForm')).submit();
                });

                $(app.getSelector('workerCv')).change(function () {
                    $(app.getSelector('workerFileForm')).submit();
                });

                $(app.getSelector('submitWorkerForm')).click(function (e) {
                    $(app.getSelector('workerForm')).submit();
                });

                var $dateStartPicker = $(app.getSelector('dateStart')).pickadate();
                var pickerDateStart = $dateStartPicker.pickadate('picker');
                pickerDateStart.set('max', true);

                var $dateEndPicker = $(app.getSelector('dateEnd')).pickadate();
                var pickerDateEnd = $dateEndPicker.pickadate('picker');
                pickerDateEnd.set('max', true);

                $(app.getSelector('workingNow')).change(function () {
                    $(app.getSelector('dateEnd')).toggle();
                    $(app.getSelector('dateEndError')).toggle();
                });

                $(app.getSelector('chooseCompetence')).click(function () {
                    $(this).toggleClass('active');
                    $(this).next().trigger('click');
                });

            }
        }
    };

    private.Selection = function () {
        return {
            selectors: {
                typeIdSelect: 'select[name="type_id"]',
                limitSelect: 'select[name="limit"]',
                actionsInput: 'input[name="hidden"], input[name="starred"]',
                filterSelect: 'select[name="filter"]',
                moreButton: '.more',
                accordionParent: '.types-block',
                shareInput: '#table-search',
                shareTable: '.selection-share-table',
                shareTableRows: '.selection-share-table tbody tr'
            },
            actionShare: function () {
                var $rows = $(app.getSelector('shareTableRows'));
                var obj = this;

                if ($rows.length) {
                    $(app.getSelector('shareInput')).keyup(function () {
                        var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();

                        $rows.show().filter(function () {
                            var text = $(this).find('.filter').text().replace(/\s+/g, ' ').toLowerCase();
                            return !~text.indexOf(val);
                        }).hide();
                    });

                    //var $table = $(app.getSelector('shareTable'));
                    //
                    //$table.find('.sort').on('click', function () {
                    //    obj.sortTable(1, $(this).index(), $table.find('tbody tr'));
                    //});
                }
            },
            actionCandidate: function () {
                var obj = this;
                obj.changeStatus();

                $(app.getSelector('moreButton')).click(function () {
                    var $parent = $(this).parents(app.getSelector('accordionParent'));

                    if ($parent.hasClass('active')) {
                        $parent.removeClass('active');
                    } else {
                        $parent.addClass('active');
                    }
                });
            },
            actionView: function () {
                var obj = this;

                $(
                    app.getSelector('typeIdSelect') + ',' +
                    app.getSelector('limitSelect') + ',' +
                    app.getSelector('filterSelect')
                ).change(function () {
                        obj.refreshSort();
                    });

                obj.changeStatus();

            },
            changeStatus: function () {
                $(app.getSelector('actionsInput')).change(function () {
                    var workerStatus = {};
                    var $this = $(this);

                    workerStatus.action = $this.attr('name');
                    workerStatus.candidateId = $this.val();

                    $.ajax({
                        url: Yii.app.createUrl('ajax/changeWorkerStatus'),
                        type: 'post',
                        data: workerStatus
                    });
                });
            },
            sortTable: function (f, n, $table) {
                //FUTURE
            },
            refreshSort: function () {

                var sortUrl = {};
                var type_id = $(app.getSelector('typeIdSelect')).val();
                var limit = $(app.getSelector('limitSelect') + ' option:selected').text();
                var filter = $(app.getSelector('filterSelect')).val();

                sortUrl.id = selectionId;

                if (type_id != '') {
                    sortUrl.type_id = type_id;
                }

                if (limit != '') {
                    sortUrl.limit = limit;
                }

                switch (parseInt(filter)) {
                    case 0:
                        break;
                    case 1:
                        sortUrl.starred = 1;
                        break;
                    case 2:
                        sortUrl.hidden = 1;
                        break;
                }

                window.location.href = Yii.app.createUrl('selection/view', sortUrl);
            }
        }
    };

    private.Employer = function () {
        return {
            selectors: {
                addField: '#selection-form .add-field',
                removeField: '#selection-form .competence-field-group > span',
                competenceFieldGroup: '#selection-form .competence-field-group',
                dateStartPicker: '#Selection_date_start',
                dateEndPicker: '#Selection_date_end',
                radioInput: '.choose-hosting-type input[type="radio"]',
                cvopenHost: '#cvopen-host > .row',
                priceTableContainer: '#price-table',
                actionButtons: '.actions',
                hostPlansInfo: 'input[name="HostPlan[]"]:checked, ' +
                '#Selection_date_start, ' +
                '#Selection_date_end, ' +
                '.choose-hosting-type input[type="radio"]',
                hostPlans: 'input[name="HostPlan[]"], #Selection_date_start, #Selection_date_end',
                hostPlansCheckbox: 'input[name="HostPlan[]"]',
                submitEmployer: '#submit-employer',
                selectionForm: '#selection-form',
                selectionListTr: '#employer-selections tbody tr',
                trLink: '.tr-link'
            },
            actionHostselection: function () {
                var obj = this;

                //Date start/end
                var $dateStart = $(app.getSelector('dateStartPicker')).pickadate();
                var pickerDateStart = $dateStart.pickadate('picker');
                var $dateEnd = $(app.getSelector('dateEndPicker')).pickadate();
                var pickerDateEnd = $dateEnd.pickadate('picker');
                pickerDateStart.set('min', true);
                pickerDateStart.on('set', function (e) {
                    pickerDateEnd.set('min', new Date(e.select));
                });

                obj.loadPriceTable();

                $(app.getSelector('hostPlans')).change(function () {
                    obj.loadPriceTable();
                });
            },
            loadPriceTable: function () {
                $.ajax({
                    url: Yii.app.createUrl('ajax/GetPriceTable'),
                    type: 'post',
                    dataType: 'JSON',
                    data: $(app.getSelector('hostPlansInfo')).serialize(),
                    success: function (data) {
                        if (data.success) {
                            $(app.getSelector('actionButtons')).slideDown();
                        } else {
                            $(app.getSelector('actionButtons')).slideUp();
                        }

                        $(app.getSelector('priceTableContainer')).html(data.html);
                    }
                });
            },
            actionSelections: function () {
                $(app.getSelector('selectionListTr')).click(function () {
                    window.location.href = $(this).find('.tr-link').attr('href');
                })
            },
            actionReviewselection: function () {
                $(app.getSelector('submitEmployer')).click(function () {
                    $(app.getSelector('selectionForm')).submit();
                })
            },
            actionSelection: function () {
                //Add competence field
                $(app.getSelector('addField')).click(function () {
                    var $lastCompetenceFieldGroup = $(app.getSelector('competenceFieldGroup')).last();
                    var nextIndex = parseInt($lastCompetenceFieldGroup.find('input').data('index')) + 1;
                    var $newCompetenceField = $lastCompetenceFieldGroup.clone();

                    $newCompetenceField.find('input')
                        .attr('value', '')
                        .data('index', nextIndex)
                        .attr('data-index', nextIndex)
                        .attr('name', 'Competence[' + nextIndex + '][name]')
                        .attr('id', 'Competence_' + nextIndex + '_name');

                    $newCompetenceField.find('.errorMessage').hide();


                    $lastCompetenceFieldGroup.after($newCompetenceField[0].outerHTML);
                });//End

                //Remove comptence field
                $(document).on('click', app.getSelector('removeField'), function () {
                    $(this).parents(app.getSelector('competenceFieldGroup')).remove();
                });
            }
        }
    };

    //Private functions
    private.Site = function () {
        return {
            selectors: {
                glideJs: '.slider',
                glideJsItem: '.slider__item',
                owlContainer: '.client-logos',
                quates: '.quates'
            },
            actionIndex: function () {

                $(app.getSelector('owlContainer')).owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: false,
                    autoplay: true,
                    autoplayTimeout: 1000,
                    autoplaySpeed: 1000,
                    responsive: {
                        0: {
                            items: 1
                        },
                        600: {
                            items: 2
                        },
                        1000: {
                            items: 4
                        }
                    }
                });

                $(app.getSelector('quates')).owlCarousel({
                    loop: true,
                    margin: 10,
                    nav: false,
                    autoplay: true,
                    autoplayTimeout: 5000,
                    autoplaySpeed: 1000,
                    autoplayHoverPause: true,
                    items: 1
                });


                setTimeout(function () {
                    $(app.getSelector('glideJs')).find('.slide__1').addClass('open');
                }, 200);

                //$(app.getSelector('glideJs')).glide({
                //    arrows: false,
                //    afterTransition: function () {
                //        var glide = $(app.getSelector('glideJs')).glide().data('api_glide');
                //        var current = glide.current();
                //        $(app.getSelector('glideJsItem')).removeClass('open');
                //        $('.slide__' + current).addClass('open');
                //        console.log(current);
                //    }
                //});
            }
        }
    };

    //Global functions
    global.load = function () {
        var $body = $('body');
        this.controller = $body.data('controller');
        this.controllerAction = $body.data('action');

        private['Init']()['actionInit']();

        if (typeof private[this.controller] !== 'undefined') {
            if (typeof private[this.controller]()['actionInit'] !== 'undefined') {
                private[this.controller]()['actionInit']();
            }

            if (typeof private[this.controller]()['action' + this.controllerAction] !== 'undefined') {
                private[this.controller]()['action' + this.controllerAction]();
            }
        }
    };

    global.redirect = function (url) {
        window.location.href = url;
    };

    global.getConfig = function (config) {
        return private[this.controller]()['config'][config];
    };

    global.setConfig = function (config, value) {
        private[this.controller]()['config'][config] = value;
    };

    global.getSelector = function (selector) {
        return private[this.controller]()['selectors'][selector];
    };

    return global;
})();