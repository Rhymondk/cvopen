<?php
$yii = dirname(__FILE__).'/protected/vendor/autoload.php';

if (isset($_SERVER['ENV'])) {

    if ($_SERVER['ENV'] == 'dev') {
        $config = dirname(__FILE__).'/protected/config/main-dev.php';
    }

    if ($_SERVER['ENV'] == 'stage') {
        $config = dirname(__FILE__).'/protected/config/main-stage.php';
    }

    defined('YII_DEBUG') or define('YII_DEBUG', true);

} else {
    $config = dirname(__FILE__).'/protected/config/main.php';
}

defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL', 3);

require_once($yii);
Yii::createWebApplication($config)->runEnd('public');
return array(
    'class' => 'CWebApplication',
    'config' => $config,
);
